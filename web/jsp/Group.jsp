<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>

<c:forEach items="${Groups}" var="group" varStatus="status">
	<c:if test="${status.index != 0}">
		<jsp:include page="SmallGeneral.jsp" />
	</c:if>
	<div class="group">
		<h2>${status.index + 1}. skupina</h2>
		<table class="groupGenerals">
			<tr><td>Vážil</td><td>${group.bodyWeightReferee}</td></tr>
			<tr><td>Technický rozhodčí</td><td>${group.technicalReferee}</td></tr>
			<tr><td>Zápis</td><td>${group.resultsReferee}</td></tr>
			<tr><td>Časomíra</td><td>${group.timeReferee}</td></tr>
			<tr><td>Hlavní rozhodčí</td><td>${group.mainReferee}</td></tr>
			<c:if test="${not empty group.sideReferee}">
				<c:forEach items="${group.sideReferee}" var="referee">
					<tr><td>Postranní rozhodčí</td><td>${referee}</td></tr>
				</c:forEach>
			</c:if>
		</table>

		<table class="group">
			<thead>
				<tr>
					<th class="name">Jméno</th>
					<th class="name">Oddíl</th>
					<th>Ročník</th>
					<th>Hmotnost</th>
					<th>Trh</th>
					<th>Nadhoz</th>
					<th>Kategorie</th>
				</tr>
			</thead>
			<c:forEach items="${Contestants}" var="contestant">
				<c:if test="${contestant.groupId == group.id}">
					<tr>
						<td>${contestant.name}</td>
						<td>${contestant.club}</td>
						<td class="number">${contestant.yearBirth}</td>
						<td class="number bodyWeight"><fmt:formatNumber type="number" maxFractionDigits="1" minFractionDigits="1" value="${contestant.bodyWeight}" /></td>
						<td class="number base">${contestant.snatchBase}</td>
						<td class="number base">${contestant.cleanAndJerkBase}</td>
						<td class="category">${contestant.category}
							<c:if test="${contestant.teamMembership == 'TEAM' && not empty contestant.team}">(${contestant.team})</c:if>
						</td>
					</tr>
				</c:if>
			</c:forEach>
		</table>
	</div>
</c:forEach>