<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix='fn' uri='http://java.sun.com/jsp/jstl/functions' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
    
<div class="results">
	<c:forEach items="${Groups}" var="group" varStatus="groupNumber">

		<h2<c:if test="${groupNumber.index != 0}"> class="breakAlways"</c:if>>${groupNumber.index + 1}. skupina</h2>
		<table class="results fixed">
			<thead>
				<tr>
					<th class="weld name thin">Jméno</th>
					<th rowspan="2" class="thin">Ročník</th>
					<th rowspan="2" class="thin">Hmot.</th>
					<th colspan="3" class="weld">Trh</th>
					<th colspan="3" class="weld">Nadhoz</th>
				</tr>
				<tr>
					<th>Oddíl</th>
					<th>1.</th>
					<th>2.</th>
					<th>3.</th>
					<th>1.</th>
					<th>2.</th>
					<th>3.</th>
				</tr>
			</thead>
		<c:forEach items="${Contestants}" var="contestant" varStatus="line">
			<c:if test="${contestant.groupId == group.id}">
				<tr>
					<td class="name">${contestant.name}<br><span class="smallClub">${contestant.club}</span>&nbsp;</td>
					<td>${contestant.yearBirth}</td>
					<td class="number"><fmt:formatNumber type="number" maxFractionDigits="1" minFractionDigits="1" value="${contestant.bodyWeight}" /></td>
	
	
					<!-- Snatch attempts -->				
					<td class="number framed">&nbsp;</td>
					<td class="number framed">&nbsp;</td>
					<td class="number framed" style="border-right: 2pt solid black;">&nbsp;</td>
						
					<!-- Clean and jerk attempts -->				
					<td class="number bottomFramed">&nbsp;</td>
					<td class="number framed">&nbsp;</td>
					<td class="number framed rightFramed">&nbsp;</td>
					
				</tr>
			</c:if>
		</c:forEach>
		</table>
	</c:forEach>
</div>