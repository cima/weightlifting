<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix='fn' uri='http://java.sun.com/jsp/jstl/functions' %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
    
<div class="results">

	<H2>Výsledky</H2>

		<c:forEach items="${Results}" var="contestant" varStatus="line">
		<c:if test="${contestant.restartHere}">
			<c:if test="${line.index != 0}"></table></c:if>
		
			<c:set var="category" scope="request" value="${contestant.category}"/>
			<jsp:include page="ResultsHeader.jsp"/>
		</c:if>
		
			<tr <c:if test="${contestant.teamSize > 1 && not contestant.restartHere}">class="topBorder"</c:if>>
				<td class="name">${contestant.name}<br><span class="smallClub">${contestant.club}
				<c:if test="${contestant.teamMembership == 'TEAM' && not empty contestant.team}">(${contestant.team})</c:if>
				</span></td>
				<td>${contestant.yearBirth}</td>
				<td class="number"><fmt:formatNumber type="number" maxFractionDigits="1" minFractionDigits="1" value="${contestant.bodyWeight}" /></td>
				<%--
				<td class="number"><fmt:formatNumber type="number" maxFractionDigits="4" minFractionDigits="4" value="${contestant.sinclairCoefficient}" /></td>
				 --%> 

				<!-- Snatch attempts -->				
				<c:forEach var="i" begin="0" end="2">
					<td class="number">
						<c:if test="${i < fn:length(contestant.results.snatch)}">
							<c:if test="${contestant.results.snatch[i] < 0}">
								<span class="strike">${-contestant.results.snatch[i]}</span>
							</c:if>
							<c:if test="${contestant.results.snatch[i] >= 0}">
								${contestant.results.snatch[i]}
							</c:if>
						</c:if>
						<c:if test="${i >= fn:length(contestant.results.snatch)}">
							&nbsp;
						</c:if>
					</td>
				</c:forEach>
				
				<!-- Clean and jerk attempts -->				
				<c:forEach var="i" begin="0" end="2">
					<td class="number">
						<c:if test="${i < fn:length(contestant.results.cleanAndJerk)}">
							<c:if test="${contestant.results.cleanAndJerk[i] < 0}">
								<span class="strike">${-contestant.results.cleanAndJerk[i]}</span>
							</c:if>
							<c:if test="${contestant.results.cleanAndJerk[i] >= 0}">
								${contestant.results.cleanAndJerk[i]}
							</c:if>
						</c:if>
						<c:if test="${i >= fn:length(contestant.results.cleanAndJerk)}">
							&nbsp;
						</c:if>
					</td>
				</c:forEach>
				
				<td class="number resultValue">${contestant.total}</td>
				<td class="number resultValue">
					<c:if test="${contestant.sinclairScore >= 0}">
						<fmt:formatNumber type="number" maxFractionDigits="4" minFractionDigits="4" value="${contestant.sinclairScore}" />
					</c:if>	
				</td>
				
				<c:if test="${contestant.teamSize == 1}">
					<td class="singleScore resultValue">${contestant.order}</td>
				</c:if>
				<c:if test="${contestant.teamSize > 1}">
					<td class="teamScore resultValue" rowspan="${contestant.teamSize}">${contestant.order}
					<span class="smallClub"><fmt:formatNumber type="number" maxFractionDigits="4" minFractionDigits="4" value="${contestant.teamScore}" /><br/> Sc. bodů</span>
					</td>
				</c:if>
			</tr>
		</c:forEach>
	</table>
</div>