<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%> 
<jsp:include page="Header.jsp">
	<jsp:param value="GroupsAndResults.css" name="specificStyle"/>
</jsp:include>

	<jsp:include page="BigGeneral.jsp" />
	<jsp:include page="Group.jsp" />
	<jsp:include page="Results.jsp" />

<jsp:include page="Footer.jsp" />