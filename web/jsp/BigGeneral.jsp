<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>

<h1>${General.title}</h1>
<div class="location">
	<i>${General.location}</i><br>
	${General.date.day}. ${General.date.month}. ${General.date.year}<br>
	Ředitel soutěže: ${General.director}
</div>