<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%><!DOCTYPE HTML PUBLIC
"-//W3C//DTD HTML 4.01//EN"
"http://www.w3.org/TR/html4/strict.dtd"> 
<html>
	<head>
		<title>${General.title}</title>
		<c:if test="${not empty param.specificStyle}">
			<link rel="stylesheet" href="${applicationScope.contextPath}/${param.specificStyle}" type="text/css">
		</c:if>
		<link rel="stylesheet" href="${applicationScope.contextPath}/common_styles.css" type="text/css">
		<link rel="shorcut icon" href="/img/weightlifting.ico" type="image/x-icon">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
		<meta http-equiv="Pragma" content="no-cache" />
		<meta http-equiv="Expires" content="0" />
		<c:if test="${not empty requestScope.redirect}">
			<meta http-equiv="refresh" content="2;url=${requestScope.redirect}">
		</c:if>
		<c:if test="${not empty param.specificJavascript}">
			<script type="text/javascript" language="javascript" src="${applicationScope.contextPath}/${param.specificJavascript}"></script>
		</c:if>
	</head>

	<c:if test="${not empty param.specificOnloadFunction}"><body onload="${param.specificOnloadFunction}"></c:if>
	<c:if test="${empty param.specificOnloadFunction}"><body></c:if>