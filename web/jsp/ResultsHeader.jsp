<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
    
    <h3>${category}</h3>
    
<table class="results">
	<thead>
		<tr>
			<th class="weld name">Jméno</th>
			<th rowspan="2">Ročník</th>
			<th rowspan="2">Hmot.</th>
			<%-- <th rowspan="2">S. koef.</th> --%>
			<th colspan="3" class="weld">Trh</th>
			<th colspan="3" class="weld">Nadhoz</th>
			<th rowspan="2">Celkem</th>
			<th rowspan="2">Sincl.<br>bodů</th>
			<th rowspan="2">Pořadí</th>
		</tr>
		<tr>
			<th>Oddíl</th>
			<th class="attempt">1.</th>
			<th class="attempt">2.</th>
			<th class="attempt">3.</th>
			<th class="attempt">1.</th>
			<th class="attempt">2.</th>
			<th class="attempt">3.</th>
		</tr>
	</thead>