<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%> 
<jsp:include page="Header.jsp">
	<jsp:param value="Technical.css" name="specificStyle"/>
</jsp:include>

<c:set var="amount" value="23"/>
<c:set var="freeAmount" value="35"/>
<c:set var="pages" value="3"/>

<c:forEach items="${TechnicalSheets}" var="sheet" varStatus="status">

	<c:forEach items="${sheet.disciplines}" var="discipline">
		<c:set var="first" value="${discipline.base}"/>
		<c:set var="last" value="${discipline.base + amount}"/>	
		<c:forEach begin="1" end="${pages}" var="page">
		<div class="technical">
			<h2>${status.index + 1}. skupina – ${discipline.kind.name}</h2>
	
			<table class="technical">
			
				<thead>
					<tr>
						<c:if test="${page == 1}">
						<th class="name">Jméno</th>
						<th class="name">Oddíl</th>
						</c:if>
						<c:forEach begin="${first}" end="${last}" var="i">
							<th class="fixWidth">${i}</th>
						</c:forEach>
						
					</tr>
				</thead>
				<c:forEach items="${Contestants}" var="contestant">
					<c:if test="${contestant.groupId == sheet.group.id}">
						<tr>
							<c:if test="${page == 1}">
								<td class="technical name">${contestant.name}</td>
								<td class="technical name">${contestant.club}</td>
							</c:if>
							<c:forEach begin="${first}" end="${last}" var="i">
								<td class="technical vertical <c:if test="${i == first}"> verticalLeft</c:if>">&nbsp;
								<c:if test="${discipline.kind == 'SNATCH' && contestant.snatchBase == i}">1</c:if>
								<c:if test="${discipline.kind == 'CLEAN_AND_JERK' && contestant.cleanAndJerkBase == i}">1</c:if>
								</td>
							</c:forEach>
						</tr>
					</c:if>
				</c:forEach>
			</table>
		</div>
		<c:set var="first" value="${last + 1}"/>
		<c:set var="last" value="${first + freeAmount}"/>
		</c:forEach>
	</c:forEach>
</c:forEach>

<jsp:include page="Footer.jsp" />