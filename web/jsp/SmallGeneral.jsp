<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
    
<div class="smallGeneral">
	<table class="smallGeneral">
		<tr><td>${General.title}</td><td>${General.location}, ${General.date.day}. ${General.date.month}. ${General.date.year}</td></tr>
	</table>
</div>