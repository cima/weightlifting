<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%> 
<jsp:include page="Header.jsp">
	<jsp:param value="Technical.css" name="specificStyle"/>
	<jsp:param value="anouncement.js" name="specificJavascript"/>
	<jsp:param value="preparePage()" name="specificOnloadFunction"/>
</jsp:include>

<c:set var="maxOffset" value="60"/>
<c:set var="minOffset" value="20"/>

<c:set var="minBase" value="${sheet.disciplines[disciplineKind.ordinal].base - minOffset}"/>
<c:if test="${minBase < 0}"><c:set var="minBase" value="0"/></c:if>

<c:set var="maxBase" value="${sheet.disciplines[disciplineKind.ordinal].maxBase + maxOffset}"/>

<span id="groupSignature" style="display:none;">${groupSignature}</span>
<!-- Menu -->
<div class="AnnouncementsMenu">
	<c:forEach items="${groups}" var="group" varStatus="groupOrder">
		<table>
		<thead><tr><th colspan="2" class="<c:if test="${param.groupId == group.id}">Active</c:if>">${groupOrder.index + 1}. skupina</th></tr></thead>
		<tbody>
			<tr>
				<td class="<c:if test="${param.groupId == group.id && param.discipline == 'SNATCH'}">Active</c:if>"><a href="?discipline=SNATCH&amp;groupId=${group.id}">Trh</a></td>
				<td class="<c:if test="${param.groupId == group.id && param.discipline == 'CLEAN_AND_JERK'}">Active</c:if>"><a href="?discipline=CLEAN_AND_JERK&amp;groupId=${group.id}">Nadhoz</a></td>
			</tr>
		<tbody>
		</table>
	</c:forEach>
</div>
<!-- /Menu -->

<div class="MainContainer">
<!-- Contestants -->
<div class="AnouncementNames">
	<table class="anouncement">
		<thead><tr><th>Jméno</th></tr></thead>
		<tbody>
		<c:forEach items="${contestants}" var="contestant">
			<c:if test="${contestant.groupId == param.groupId}">
			<tr><td class="anouncement">${contestant.name}
			<span class="smallClub lightColor">${contestant.club}</span>
			</td></tr>
			</c:if>
		</c:forEach>
		</tbody>
	</table>
</div>
<!-- /Contestants -->

<!-- Anounnced Attempts -->
<div id="RollingHorizontal">
	<table class="anouncement" id="announcementTable">
		<thead><tr>
		<c:forEach begin="${minBase}" end="${maxBase}" var="weight">
			<th><div>${weight}</div></th>
		</c:forEach>
		</tr></thead>
		<tbody>
		<c:forEach items="${contestants}" var="contestant">
		<c:if test="${contestant.groupId == param.groupId}">
			<tr>
			<c:forEach begin="${minBase}" end="${maxBase}" var="weight">
			<td class="anouncement vertical" id="${contestant.id}_${weight}"></td>
			</c:forEach>
			</tr>
		</c:if>
		</c:forEach>
		</tbody>
	</table>
</div>
<!-- /Anounnced Attempts -->
</div>