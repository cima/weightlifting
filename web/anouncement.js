var rollFunction = function(e) {
	if ("detail" in e) {
		this.scrollLeft += 7 * e.detail;
	}else{
		this.scrollLeft += e.wheelDelta;
	}
	e.preventDefault();
}
 	
var bullet = "<img src=\"img/bulletBlue.png\" alt=\"O\" style=\"height: 25px;\">";
var discipline;
var groupId;
var groupSignature;
var contestants = null;

function anounceAttempt(idWeight, discipline, target, cotent, command){
	var xmlhttp = prepareAjax();
	xmlhttp.onreadystatechange = function(){announcingFinished(xmlhttp, idWeight, target, cotent);};
	xmlhttp.open("GET", "?command=" + command + "&discipline=" + discipline + "&id=" + idWeight.id + "&weight=" + idWeight.weight, true);
	xmlhttp.send();
}

var announcingFinished = function (xmlhttp, idWeight, target, cotent){
	if(xmlhttp.readyState == 4 && xmlhttp.status == 200){
		var okPattern = new RegExp("OK .*","ig");
		if(okPattern.test(xmlhttp.responseText)) {
			updateCell(target, idWeight, contestants[idWeight.id].results, cotent);
			return;
		}
		
		window.alert(xmlhttp.responseText);
		return;
    }
}

function toggleFunction(cell, event) {
	var idWeight = getIdWeightObject(cell);
	if (cell.getElementsByTagName("img").length == 0) {		
		anounceAttempt(idWeight, discipline, cell, bullet, "ADD")
		
	}else{
		anounceAttempt(idWeight, discipline, cell, "", "REMOVE")
	}
}

function addRolling() {
	var rollingHorizontal = document.getElementById('RollingHorizontal');

	if ("onmousewheel" in rollingHorizontal) {
		rollingHorizontal.onmousewheel = rollFunction;
	} else {
		rollingHorizontal.addEventListener("DOMMouseScroll", rollFunction);
	}

	//TODO explorer
}

function addOnclickEvents() {
	var anTable = document.getElementById("announcementTable");
	for (var i = 1, row; row = anTable.rows[i]; i++) {
		for (var j = 0, cell; cell = row.cells[j]; j++) {
			cell.onclick = function(cellSrc){ return function(event){ toggleFunction(cellSrc, event); }; }(cell);
		}
	}
}

function prepareAjax() {
	var xmlhttp;
	if (window.XMLHttpRequest) {
		// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp = new XMLHttpRequest();
	}else{
		// code for IE6, IE5
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	return xmlhttp;
}

function getStaticData(){
	discipline = RegExp("[?&]discipline=([^&]*)").exec(window.location.search)[1];
	groupId = RegExp("[?&]groupId=([^&]*)").exec(window.location.search)[1];
	groupSignature = document.getElementById("groupSignature").innerHTML;
}

function requestUpdate(){
	var xmlhttp = prepareAjax();
	xmlhttp.onreadystatechange = function(){processUpdate(xmlhttp);};
	xmlhttp.open("GET", "?command=UPDATE&groupId=" + groupId + "&discipline=" + discipline, true);
	xmlhttp.send();
}

function getItemsObject(valueString){
	var items = {};
	var weights = valueString.split(",");
	items.orig = weights;
	for (var j = 0, item; item = weights[j]; j++) {
		items[parseInt(item)] = 1;
	}
	
	items.length = weights.length;
	
	return items;
}

function processUpdate(xmlhttp){
	if(xmlhttp.readyState == 4 && xmlhttp.status == 200){
		var lines = xmlhttp.responseText.split(/\r?\n/);
		var newGroupSignature = lines[0];
		if(groupSignature != newGroupSignature){
			forceReload();
			return;
		}
		
		contestants = {};
		
		for (var i = 1, line; line = lines[i]; i++) {
			var idPart = line.split(":");
			var contestantId = parseInt(idPart[0]);
			contestants[contestantId] = {};
			contestants[contestantId].anouncements = getItemsObject(idPart[1]);
			contestants[contestantId].results = getItemsObject(idPart[2]);
		}
		
		updateSheet(contestants);
    }
}

function getIdWeightObject(cell) {
	var idWeight = new Object();
	
	var idPattern = new RegExp("(\\d+)_(\\d+)","ig");
	var idPart = idPattern.exec(cell.id);
	
	if(idPart == null){
		return null;
	}
	
	idWeight.id = parseInt(idPart[1]);
	idWeight.weight = parseInt(idPart[2]);
	
	return idWeight;
}

function updateCell(cell, idWeight, results, content){
		
	var weight = idWeight.weight;
	if(weight == 0){
		return
	}
	
	var activeElement = cell;
	cell.innerHTML = "";
	
	
	// Writeback
	if (results[weight] == 1 || results[-weight] == 1) {
		cell.title = "";
		for (var k = 0; k < 3; k++) {
			if(Math.abs(results.orig[k]) != weight) {
				continue;
			}
			var newElement = document.createElement("div");
			activeElement.appendChild(newElement);
			activeElement = newElement;
			activeElement.className = "writeback";
			activeElement.style.backgroundImage = "url(img/writeback_" +  (1 + k) * (results.orig[k] / Math.abs(results.orig[k])) + ".png)";
		}
	}
	
	// Announcements
	activeElement.innerHTML = content;
	
	if(results.orig.length >= 3){
		cell.style.backgroundColor = "rgb(80, 80, 80)";
	}
}

function updateSheet(contestants){
	
	var anTable = document.getElementById("announcementTable");
	for (var i = 1, row; row = anTable.rows[i]; i++) {
		var results = null;
		for (var j = 0, cell; cell = row.cells[j]; j++) {
			
			var idWeight = getIdWeightObject(cell);
			if (results == null) {
				results = contestants[idWeight.id].results;
			}
			
			updateCell(cell, idWeight, results, contestants[idWeight.id].anouncements[idWeight.weight] == 1 ? bullet : "");
		}
	}
}

function forceReload() {
	location.reload(true);
}

function preparePage() {
	getStaticData();
	addRolling();
	addOnclickEvents();
	requestUpdate();
	
	window.setInterval(requestUpdate, 10 * 1000);
}
