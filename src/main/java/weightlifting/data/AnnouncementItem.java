package weightlifting.data;

import java.math.BigInteger;

public class AnnouncementItem {
	public BigInteger contestantId;
	public DisciplineKind discipline;
	public int weight;
	
	public AnnouncementItem(BigInteger contestantId, DisciplineKind discipline, int weight) {
		this.contestantId = contestantId;
		this.discipline = discipline;
		this.weight = weight;
	}
}
