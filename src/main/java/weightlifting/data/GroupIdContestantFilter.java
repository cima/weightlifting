package weightlifting.data;

import java.math.BigInteger;

import weightlifting.data.structures.ContestantType;

public class GroupIdContestantFilter implements ContestantFilter {

	private BigInteger groupId;
	
	public GroupIdContestantFilter(BigInteger groupId){
		this.groupId = groupId;
	}
	
	@Override
	public boolean fit(ContestantType contestant) {
		return contestant.getGroupId().compareTo(this.groupId) == 0;
	}
}
