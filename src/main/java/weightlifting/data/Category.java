package weightlifting.data;

import weightlifting.data.structures.AgeCategoryType;
import weightlifting.data.structures.ContestantType;
import weightlifting.data.structures.GenderType;
import weightlifting.data.structures.TeamMembershipType;

public class Category implements Comparable<Category> {

	GenderType gender;
	AgeCategoryType ageCategory;
	TeamMembershipType teamMembership;
	
	/** For further use with customizable categories */
	private String subCategory = null;
	
	public Category(ContestantType contestant){
		this.gender = contestant.getGender();
		this.ageCategory = contestant.getAgeCategory();
		this.teamMembership = contestant.getTeamMembership();
	}
	
	public Category(GenderType gender, AgeCategoryType ageCategory, 
			TeamMembershipType teamMembership){
		this.gender = gender;
		this.ageCategory = ageCategory;
		this.teamMembership = teamMembership;
	}
	
	public Category(GenderType gender, AgeCategoryType ageCategory, 
			TeamMembershipType teamMembership, String subCategory){
		this.gender = gender;
		this.ageCategory = ageCategory;
		this.teamMembership = teamMembership;
		if(subCategory != null && ! subCategory.isEmpty()) {
			this.subCategory = new String(subCategory); //deep copy
		}
	}
	
	@Override
	public int compareTo(Category right) {
		/* SINGLE < TEAM < OFF_CONTEST */
		if(this.teamMembership != right.teamMembership){
			return this.teamMembership.ordinal() - right.teamMembership.ordinal();
		}
		
		//ladies first
		if(right.gender != this.gender){
			return right.gender.ordinal() - this.gender.ordinal();
		}
		
		/* SCHOOL < JUNIOR < ADULT < MASTER */
		if(this.ageCategory != right.ageCategory){
			return this.ageCategory.ordinal() - right.ageCategory.ordinal();
		}
		
		/* no category < any category */
		if(this.subCategory != null && right.subCategory != null){
			return this.subCategory.compareTo(right.subCategory);
		}else if(this.subCategory == null && right.subCategory != null){
			return -1;
		}else if(this.subCategory != null && right.subCategory == null){
			return 1;
		}
		
		return 0;
	}

	public boolean equals(Category right){
		return this.compareTo(right) == 0;
	}
	
	public boolean equals(ContestantType right){
		return this.gender == right.getGender() && this.ageCategory == right.getAgeCategory() 
				&& this.teamMembership == right.getTeamMembership();
	}
	
	public String toString(){
		String product = "";

		switch (ageCategory) {
		case SCHOOL:
			if(teamMembership == TeamMembershipType.TEAM){
				product += (gender == GenderType.MALE) ? "Dru�stva ��k�" : "Dru�stva ��ky�";
			}else{
				product += (gender == GenderType.MALE) ? "��ci" : "��kyn�";
			}
			break;
		case JUNIOR:
			if(teamMembership == TeamMembershipType.TEAM){
				product += (gender == GenderType.MALE) ? "Dru�stva junior�" : "Dru�stva juniorek";
			}else{
				product += (gender == GenderType.MALE) ? "Junio�i" : "Juniorky";
			}
			break;
		case ADULT:
			if(teamMembership == TeamMembershipType.TEAM){
				product += (gender == GenderType.MALE) ? "Dru�stva mu��" : "Dru�stva �en";
			}else{
				product += (gender == GenderType.MALE) ? "Mu�i" : "�eny";
			}
			break;
		case MASTER:
			if(teamMembership == TeamMembershipType.TEAM){
				product += (gender == GenderType.MALE) ? "Dru�stva masters mu��" : "Dru�stva masters �en";
			}else{
				product += (gender == GenderType.MALE) ? "Masters mu�i" : "Masters �eny";
			}
			break;
		}
		
		if(teamMembership == TeamMembershipType.SINGLE){
			product += ", jednotlivci";
		} else if(teamMembership == TeamMembershipType.OFF_CONTEST){
			product += ", mimo sout�";
		}
		
		if(this.subCategory != null && ! this.subCategory.isEmpty()){
			product += ", " + this.subCategory;
		}
		
		return product;
	}
	
	//----- getters & setters ------

	public GenderType getGender() {
		return gender;
	}

	public void setGender(GenderType gender) {
		this.gender = gender;
	}

	public AgeCategoryType getAgeCategory() {
		return ageCategory;
	}

	public void setAgeCategory(AgeCategoryType ageCategory) {
		this.ageCategory = ageCategory;
	}

	public TeamMembershipType getTeamMembership() {
		return teamMembership;
	}

	public void setTeamMembership(TeamMembershipType teamMembership) {
		this.teamMembership = teamMembership;
	}

	public String getSubCategory() {
		return subCategory;
	}

	public void setSubCategory(String subCategory) {
		if(subCategory == null || subCategory.isEmpty()){
			this.subCategory = null;
		}else{
			this.subCategory = new String(subCategory);
		}
	}

}
