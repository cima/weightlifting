package weightlifting.data;

import weightlifting.data.structures.ContestantType;

public class GroupNameContestantComparator implements ContestantComparator {

	@Override
	public int compare(ContestantType first, ContestantType second) {
		int res = first.getGroupId().compareTo(second.getGroupId());
		if(res == 0){
			return first.getName().compareTo(second.getName());
		}
		return res;
	}

}
