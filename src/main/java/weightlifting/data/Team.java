package weightlifting.data;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

import org.apache.log4j.Logger;

import weightlifting.data.structures.AgeCategoryType;
import weightlifting.data.structures.ContestantType;
import weightlifting.data.structures.GenderType;
import weightlifting.data.structures.ResultType;
import weightlifting.data.structures.TeamMembershipType;

public class Team extends ContestantType implements ContestantFilter, Comparable<Team> {
	private static Logger logger = Logger.getLogger(ContestantType.class);
	
	private CompetitionDataHandler data;
	
	private TeamContestantFilter filter = null;
	private MembershipContestantFilter membershipFilter = null;
	private Set<ContestantType> members = null;
	private double score = 0;
	private boolean scoreValid = false;
	private ResultType results = null;
	
	/**
	 * Creates new EMPTY team according to given masterContestant and with reference
	 * to given data.
	 * @param data Complete competition data with values needed for Sinclair score calculation
	 * @param masterContestant It's values are used to create new team, but contestant itself is not added to the team. Use addMember().
	 * @throws RuntimeException if masterContestant is not team membership set. 
	 * */
	public Team(CompetitionDataHandler data, ContestantType masterContestant) {
		
		if(masterContestant.getTeamMembership() != TeamMembershipType.TEAM){
			throw new RuntimeException(new WeightliftingDataException("Master contestant must have team membership."));
		}
		
		this.data = data;
		
		this.setGender(masterContestant.getGender());
		this.setAgeCategory(masterContestant.getAgeCategory());
		this.setClub(masterContestant.getClub());
		this.setTeam(masterContestant.getTeam());
		this.setTeamMembership(TeamMembershipType.TEAM);
		
		this.filter = new TeamContestantFilter(this);
		this.membershipFilter = new MembershipContestantFilter(this);
		this.members = new HashSet<ContestantType>(13, 1.0f);		
		this.results = new ResultType();
	}

	public int getTeamMaxSize() {
		
		if(this.getAgeCategory() == AgeCategoryType.MASTER){
			return 4;
		}
		
		return (this.getGender() == GenderType.MALE) ? 6 : 4;
	}
	
	public void addMember(ContestantType newMember) {
		
		//TODO Must be displayed to USER more friendly than by loging.
		if(this.members.size() == this.getTeamMaxSize()){
			logger.error("Team capacity exhausted.");
		}
		
		if(this.filter.fit(newMember)) {
			this.members.add(newMember);
			this.invalidateScore();
		}
	}
	
	public void invalidateScore() {
		this.scoreValid = false;
	}
	
	@Override
	public ResultType getResults() {
		if( ! this.scoreValid) {
			double minimum = Double.MAX_VALUE;
			double totalScore = 0;
			for (ContestantType member : this.members) {
				double score = this.data.getSafeScore(member);
				totalScore += score;
				if(score < minimum){
					minimum = score;
				}
			}
			
			if(this.members.size() == this.getTeamMaxSize()) {
				totalScore -= minimum;
			}
			this.score = totalScore;
			this.scoreValid = true;
		}
		
		this.results.setSinclair(new BigDecimal(this.score));
		return this.results;
	}

	@Override
	public boolean fit(ContestantType contestant) {
		return this.filter.fit(contestant);
	}

	/**
	 * This filter is to be used to separate different categories across teams.
	 * Values of Gender, Age category and Team membership are compared. See MembershipContestantFilter class for details.
	 * @return Filter to separate different categories across all teams. 
	 * */
	public MembershipContestantFilter getMembershipFilter() {
		return membershipFilter;
	}

	public Set<ContestantType> getMembers() {
		return members;
	}

	public Category getCategory() {
		return this.membershipFilter.getCategory();
	}
	
	@Override
	public int compareTo(Team right) {
		int result = 0;
		
		result = this.getGender().ordinal() - right.getGender().ordinal();
		if(result != 0){
			return result;
		}
		
		result = this.getAgeCategory().ordinal() - right.getAgeCategory().ordinal();
		if(result != 0){
			return result;
		}
		
		result = this.getTeamMembership().ordinal() - right.getTeamMembership().ordinal();
		if(result != 0){
			return result;
		}
			
		result = this.getClub().compareTo(right.getClub());
		if(result != 0){
			return result;
		}
		
		if(this.getTeamMembership() == TeamMembershipType.TEAM){
			if(this.getTeam() != null && ! this.getTeam().isEmpty()){
				if(right.getTeam() != null && ! right.getTeam().isEmpty()) {
					result = this.getTeam().compareTo(right.getTeam());
				}else{
					result = -1;
				}
			}else if(right.getTeam() != null && ! right.getTeam().isEmpty()){
				result = 1;
			}
		}
		
		/*//BIBESEBSE for teams
		if(result != 0){
			return result;
		}
		
		result = this.getName().compareTo(right.getName());
		if(result != 0){
			return result;
		}
		
		result = this.getYearBirth().getYear() - right.getYearBirth().getYear();
			
			*/
		
		return result;
	}
	
}
