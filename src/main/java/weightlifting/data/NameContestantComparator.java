package weightlifting.data;

import weightlifting.data.structures.ContestantType;

public class NameContestantComparator implements ContestantComparator {

	@Override
	public int compare(ContestantType first, ContestantType second) {
		return first.getName().compareTo(second.getName());
	}

}
