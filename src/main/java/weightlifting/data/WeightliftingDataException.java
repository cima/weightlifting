package weightlifting.data;

public class WeightliftingDataException extends Exception {
	private static final long serialVersionUID = -1722385508469536278L;

	public WeightliftingDataException() {
		super();
	}

	public WeightliftingDataException(String message, Throwable cause) {
		super(message, cause);
	}

	public WeightliftingDataException(String message) {
		super(message);
	}

	public WeightliftingDataException(Throwable cause) {
		super(cause);
	}
	
}
