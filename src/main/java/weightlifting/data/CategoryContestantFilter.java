package weightlifting.data;

import weightlifting.data.structures.ContestantType;

public class CategoryContestantFilter implements ContestantFilter {

	private Category referenceCategory = null;
	
	public CategoryContestantFilter(Category category){
		this.referenceCategory = category;
	}
	
	public CategoryContestantFilter(ContestantType contestant){
		this.referenceCategory = new Category(contestant);
	}
	
	@Override
	public boolean fit(ContestantType contestant) {
		return this.referenceCategory.equals(contestant);
	}

}
