package weightlifting.data;

import java.util.Comparator;

import weightlifting.data.structures.ContestantType;

public interface ContestantComparator extends Comparator<ContestantType> {
	
	/**
	 * @param first first (left) item to be compared
	 * @param second second (right) item to be compared to first
	 * @return 	zero if first is equal to the second under implemented criterion
	 * 			negative value if first is less than the second
	 * 			positive value if first is more than the second 
	 * */
	public int compare(ContestantType first, ContestantType second);
	
}
