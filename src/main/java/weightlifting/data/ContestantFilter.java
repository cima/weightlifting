package weightlifting.data;

import weightlifting.data.structures.ContestantType;

public interface ContestantFilter {
	public boolean fit(ContestantType contestant);
}
