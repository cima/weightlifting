package weightlifting.data;

import java.math.BigDecimal;
import java.math.BigInteger;

import javax.xml.datatype.XMLGregorianCalendar;

import weightlifting.data.structures.AgeCategoryType;
import weightlifting.data.structures.ContestantType;
import weightlifting.data.structures.GenderType;
import weightlifting.data.structures.ResultType;
import weightlifting.data.structures.TeamMembershipType;

public class ContestantTypeDecorator extends ContestantType {
	
	protected ContestantType contestant = null;
	
	public ContestantTypeDecorator(ContestantType contestant) {
		this.contestant = contestant;
	}
	
	//Decorator stuff
	@Override public String getName() { return contestant.getName(); }
	@Override public void setName(String value) {	contestant.setName(value); }
	@Override public String getClub() { return contestant.getClub(); }
	@Override public void setClub(String value) {	contestant.setClub(value); }
	@Override public XMLGregorianCalendar getYearBirth() { return contestant.getYearBirth(); }
	@Override public void setYearBirth(XMLGregorianCalendar value) { contestant.setYearBirth(value); }
	@Override public BigDecimal getBodyWeight() {	return contestant.getBodyWeight(); }
	@Override public void setBodyWeight(BigDecimal value) { contestant.setBodyWeight(value); }
	@Override public int getSnatchBase() { return contestant.getSnatchBase(); }
	@Override public void setSnatchBase(int value) { contestant.setSnatchBase(value); }
	@Override public int getCleanAndJerkBase() { return contestant.getCleanAndJerkBase(); }
	@Override public void setCleanAndJerkBase(int value) { contestant.setCleanAndJerkBase(value); }
	@Override public GenderType getGender() { return contestant.getGender(); }
	@Override public void setGender(GenderType value) { contestant.setGender(value); }
	@Override public AgeCategoryType getAgeCategory() { return contestant.getAgeCategory(); }
	@Override public void setAgeCategory(AgeCategoryType value) { contestant.setAgeCategory(value); }
	@Override public TeamMembershipType getTeamMembership() {	return contestant.getTeamMembership(); }
	@Override public void setTeamMembership(TeamMembershipType value) { contestant.setTeamMembership(value); }
	@Override public String getTeam() { return contestant.getTeam(); }
	@Override public void setTeam(String value) { contestant.setTeam(value); }
	@Override public ResultType getResults() { return contestant.getResults(); }
	@Override public void setResults(ResultType value) { contestant.setResults(value); }
	@Override public BigInteger getId() { return contestant.getId(); }
	@Override public void setId(BigInteger value) { contestant.setId(value); }
	@Override public BigInteger getGroupId() { return contestant.getGroupId(); }
	@Override public void setGroupId(BigInteger value) { contestant.setGroupId(value); }
}
