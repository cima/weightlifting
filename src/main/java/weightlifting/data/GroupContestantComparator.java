package weightlifting.data;

import weightlifting.data.structures.ContestantType;

public class GroupContestantComparator implements ContestantComparator {

	@Override
	public int compare(ContestantType first, ContestantType second) {
		return first.getGroupId().compareTo(second.getGroupId());
	}

}
