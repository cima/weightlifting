package weightlifting.data;

import weightlifting.data.structures.GenderType;

public class SinclairUtils {

	private double recordManWeight;
	private double recordManCoefficient; 
	private double recordWomanWeight;
	private double recordWomanCoefficient;
	
	private static final double[] maloneMeltzerTable = {
		1.000, 
		1.014, 1.028, 1.043, 1.058, 1.072, 
		1.087, 1.100, 1.113, 1.125, 1.136,
		1.147, 1.158, 1.170, 1.183, 1.195,
		1.207, 1.217, 1.226, 1.234, 1.243,
		1.255, 1.271, 1.293, 1.319, 1.350,
		1.384, 1.417, 1.449, 1.480, 1.509,
		1.536, 1.561, 1.584, 1.608, 1.636,
		1.671, 1.719, 1.782, 1.856, 1.933,
		2.002, 2.053, 2.087, 2.113, 2.142,
		2.184, 2.251, 2.358, 2.500, 2.669,
		2.849, 3.018, 3.166, 3.288, 3.386,
		3.458, 3.508, 3.540, 3.559, 3.571
	};
	
	public SinclairUtils(double recordManWeight, double recordmanCoefficient, 
			double recordWomanWeight, double recordWomanCoefficient){
		this.recordManWeight = recordManWeight;
		this.recordManCoefficient = recordmanCoefficient;
		this.recordWomanWeight = recordWomanWeight;
		this.recordWomanCoefficient = recordWomanCoefficient;
	}
	
	public double getSinclairCoefficient(GenderType gender, double contestantWeight) {
		
		double recordCoefficient = (gender == GenderType.MALE) ? this.recordManCoefficient : this.recordWomanCoefficient;
		double recordBodyWeight = (gender == GenderType.MALE) ? this.recordManWeight : this.recordWomanWeight;
		
		double sinclairCoefficient = 1;
		if( contestantWeight < recordBodyWeight){
			sinclairCoefficient =  Math.pow(10, 
					recordCoefficient * Math.pow(Math.log10(contestantWeight/recordBodyWeight), 2)
				);
		}
		
		return sinclairCoefficient;
	}
	
	public double getMaloneMeltzerCoefficient(GenderType gender, int age) {

		double maloneMeltzerCoefficient = maloneMeltzerTable[0];
		if((gender == GenderType.MALE && age >= 35) ||
		   (gender == GenderType.FEMALE && age >= 30)){
			if(age > 90){
				maloneMeltzerCoefficient = maloneMeltzerTable[60];
			}else{
				maloneMeltzerCoefficient = maloneMeltzerTable[age - 30];
			}
		}
		return maloneMeltzerCoefficient;
	}
	
	public double getSinclairScore(GenderType gender, int age, boolean master, double contestantWeight, int weight) {

		double sinclairCoefficient = getSinclairCoefficient(gender, contestantWeight);
		double maloneMeltzerCoefficient = master ? getMaloneMeltzerCoefficient(gender, age) : 1;
		
		return sinclairCoefficient * maloneMeltzerCoefficient * weight;
	}
	
	public double getJuniorBonus(int age) {
		
		if(age >= 15 && age <= 17){
			return 30;
			
		}else if(age >= 18 && age <= 20){
			return 20;
			
		}
		
		return 0;
	}
	
}
