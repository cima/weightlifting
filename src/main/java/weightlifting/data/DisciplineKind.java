package weightlifting.data;

public enum DisciplineKind {
	
	SNATCH("Trh"), CLEAN_AND_JERK("Nadhoz");
	
	private String name;
	
	private DisciplineKind(String name){ this.name = name; }
	public String getName(){ return name; }
	public int getOrdinal(){ return this.ordinal(); }
}