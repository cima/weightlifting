package weightlifting.data;

import weightlifting.data.structures.ContestantType;

public interface ContestantFactory {
	public ContestantType newContestant();
	public void setMasterContestant(ContestantType master);
}
