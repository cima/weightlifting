package weightlifting.data;

import weightlifting.data.structures.ContestantType;
import weightlifting.data.structures.GenderType;
import weightlifting.data.structures.TeamMembershipType;

public class CategoryContestantComparator implements ContestantComparator {

	@Override
	public int compare(ContestantType first, ContestantType second) {
		/* SINGLE < TEAM < OFF_CONTEST */
		if(first.getTeamMembership() != second.getTeamMembership()){
			return first.getTeamMembership().ordinal() - second.getTeamMembership().ordinal();
		}

		/* FEMALE < MALE */
		if(first.getGender() != second.getGender()){
			return (first.getGender() == GenderType.FEMALE) ? -1 : 1;
		}
		
		/* SCHOOL < JUNIOR < ADULT < MASTER */
		if(first.getAgeCategory() != second.getAgeCategory()){
			return first.getAgeCategory().ordinal() - second.getAgeCategory().ordinal();
		}
		
		/* Club | Team */
		if(first.getTeamMembership() == TeamMembershipType.TEAM &&
			second.getTeamMembership() == TeamMembershipType.TEAM){
			
			String firstCmp = first.getClub();
			if(first.getTeam() != null){
				firstCmp += first.getTeam(); 
			}
			
			String secondCmp = second.getClub();
			if(second.getTeam() != null){
				secondCmp += second.getTeam(); 
			}
			
			int result = firstCmp.compareTo(secondCmp);
			if(result != 0){
				return result;
			}
		}
		
		/* name */
		return first.getName().compareTo(second.getName());
	}

}
