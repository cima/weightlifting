package weightlifting.data;

import java.math.BigDecimal;
import java.math.BigInteger;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import weightlifting.data.structures.AgeCategoryType;
import weightlifting.data.structures.ContestantType;
import weightlifting.data.structures.GenderType;
import weightlifting.data.structures.ObjectFactory;
import weightlifting.data.structures.TeamMembershipType;

public class GroupIdContestantFactory implements ContestantFactory {

	private BigInteger groupId;
	private ObjectFactory objFactory;
	private ContestantType master;
	private DatatypeFactory dateFactory;
	
	public GroupIdContestantFactory(BigInteger groupId){
		this.groupId = groupId;
		this.objFactory = new ObjectFactory();
		try {
			dateFactory = DatatypeFactory.newInstance();
		} catch (DatatypeConfigurationException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public ContestantType newContestant() {
		ContestantType contestant = this.objFactory.createContestantType();
		contestant.setGroupId(this.groupId);
		
		contestant.setName("");
		contestant.setClub("");
		contestant.setBodyWeight(BigDecimal.valueOf(0.0));
		if(dateFactory != null){
			XMLGregorianCalendar year = dateFactory.newXMLGregorianCalendar();
			year.setYear(1989);
			contestant.setYearBirth(year);
		}
		
		contestant.setResults(this.objFactory.createResultType());
		
		if(master != null){
			contestant.setGender(master.getGender());
			contestant.setAgeCategory(master.getAgeCategory());
			contestant.setTeamMembership(master.getTeamMembership());
		}
		
		if(contestant.getGender() == null){
			contestant.setGender(GenderType.MALE);
		}
		
		if(contestant.getAgeCategory() == null){
			contestant.setAgeCategory(AgeCategoryType.ADULT);
		}
		
		if(contestant.getTeamMembership() == null) {
			contestant.setTeamMembership(TeamMembershipType.SINGLE);
		}
		
		return contestant;
	}

	@Override
	public void setMasterContestant(ContestantType master) {
		this.master = master;
	}

	public BigInteger getGroupId() {
		return groupId;
	}

}
