package weightlifting.data;

import weightlifting.data.structures.ContestantType;

public class MembershipContestantFilter implements ContestantFilter {

	private ContestantType masterContestant = null;
	private Category category;
	
	public MembershipContestantFilter(ContestantType masterContestant){
		this.masterContestant = masterContestant;
		this.category = new Category(masterContestant);
	}
	
	@Override
	public boolean fit(ContestantType contestant) {
		if(contestant.getGender() == this.masterContestant.getGender()
				&& contestant.getAgeCategory() == this.masterContestant.getAgeCategory()
				&& contestant.getTeamMembership() == this.masterContestant.getTeamMembership()
		) {
			return true;
		}
		
		return false;
	}
	
	public Category getCategory() {
		return this.category;
	}

}
