package weightlifting.data;

import weightlifting.data.structures.ContestantType;
import weightlifting.data.structures.TeamMembershipType;

public class TeamContestantFilter implements ContestantFilter {

	private ContestantType masterContestant = null;
	
	public TeamContestantFilter(ContestantType masterContestant){
		if(masterContestant.getTeamMembership() != TeamMembershipType.TEAM) {
			throw new RuntimeException(new WeightliftingDataException("Master contestant is not a team member."));
		}
		this.masterContestant = masterContestant;
	}
	
	@Override
	public boolean fit(ContestantType contestant) {
		
		if(this.masterContestant == null
				|| contestant.getGender() != this.masterContestant.getGender()
				|| contestant.getAgeCategory() != this.masterContestant.getAgeCategory()
				|| contestant.getTeamMembership() != TeamMembershipType.TEAM
		) {
			return false;
		}
		
		//According to explicitly defined team name
		String explicitTeam = masterContestant.getTeam();
		String incommingTeam = contestant.getTeam();
		
		if(explicitTeam != null && ! explicitTeam.isEmpty()){
			if(incommingTeam != null && explicitTeam.equals(incommingTeam)){
				return true;
			} else {
				return false;
			}
		}else if(incommingTeam != null && ! incommingTeam.isEmpty()){
			return false;
		}

		//According to club (default)
		String masterClub = masterContestant.getClub();
		String incommingClub = contestant.getClub();
		if(masterClub != null && ! masterClub.isEmpty()){
			if(incommingClub != null && masterClub.equals(incommingClub)){
				return true;
			}
			
		//NULL team
		}else if((masterClub == null || masterClub.isEmpty()) && (incommingClub == null || incommingClub.isEmpty())){
			return true;
		}

		return false;
	}

}
