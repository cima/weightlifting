package weightlifting.data;

import java.io.File;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Observable;
import java.util.Set;
import java.util.TreeMap;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import weightlifting.data.structures.Competition;
import weightlifting.data.structures.Competition.Contestants;
import weightlifting.data.structures.Competition.Groups;
import weightlifting.data.structures.AgeCategoryType;
import weightlifting.data.structures.ContestantType;
import weightlifting.data.structures.GenderType;
import weightlifting.data.structures.GeneralType;
import weightlifting.data.structures.GroupType;
import weightlifting.data.structures.ObjectFactory;
import weightlifting.data.structures.RecordType;
import weightlifting.data.structures.ResultType;
import weightlifting.data.structures.TeamMembershipType;

public class CompetitionDataHandler extends Observable {
	 
	public static final double MALE_RECORD_WEIGHT = 174.393;
	public static final double MALE_RECORD_COEFFICIENT = 0.794358141;
	
	public static final double FEMALE_RECORD_WEIGHT = 148.026;
	public static final double FEMALE_RECORD_COEFFICIENT = 0.897260740;
	
	public static final String SAVE = "CompetitionDataHandler.Save";
	public static final String ANOUNCEMENT = "CompetitionDataHandler.Anouncement";
	
	private static Logger logger = Logger.getLogger(CompetitionDataHandler.class);
	
	private Competition data = null;
	private File xmlFile = null;
	private boolean saved = true;
	
	private SinclairUtils sinclairUtils = null;
	private TeamOrderCache teamCache = null;
	private Map<Category, Set<Double>> orderMap = null;
	
	private boolean basesAnouncementsSynced = false;
	
	public void load(String fileName) throws WeightliftingDataException{
		File tmpFile = new File(fileName);
		load(tmpFile);
	}
	
	public void load(File file) throws WeightliftingDataException {
		if(file == null || ! file.exists()){
			throw new WeightliftingDataException("Given file does not exists.");
		}
		
		try{
			JAXBContext jc = getJaxbContext();
			Unmarshaller unm = jc.createUnmarshaller();
			this.data = (Competition)unm.unmarshal(file);
			this.xmlFile = file;
			this.setSaved(true);
			this.setChanged();
			this.notifyObservers();
		}catch(JAXBException je){
			throw new WeightliftingDataException(je.getCause());
		}
	}
	
	public void save() throws WeightliftingDataException {
		save(this.xmlFile);
	}
	
	public void save(String filename) throws WeightliftingDataException {
		save(new File(filename));
	}
	
	public void save(File file) throws WeightliftingDataException {
	
		try{
			JAXBContext jc = getJaxbContext();	
			//TODO Validate data before serializing to the file (long term task)
			
			Marshaller marshaller = jc.createMarshaller();
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, new Boolean(true));
			marshaller.setProperty(Marshaller.JAXB_NO_NAMESPACE_SCHEMA_LOCATION, "WeightliftingCompetition.xsd");
			marshaller.marshal(this.data, file);
			this.setSaved(true);
			
		}catch(JAXBException je){
			throw new WeightliftingDataException(je.getCause());
		}
		
	}
	
	public void newData() {
		ObjectFactory factory = new ObjectFactory();
		this.data = factory.createCompetition();
		
		this.data.setGeneral(newGeneral());
		
		this.data.setGroups(new Groups());
		List<GroupType> groups = this.data.getGroups().getGroup();
		groups.add(newGroup(1));
		
		this.data.setContestants(new Contestants());
		
		setSaved(false);
	}
	
	public GroupType addGroup(){
		int groupId = getMaxGroupId() + 1;
		GroupType group = newGroup(groupId);
		this.data.getGroups().getGroup().add(group);
		this.setDirty();
		return group;
	}
	
	public void removeGroup(BigInteger groupId){
		GroupType toRemove = null;
		List<GroupType> groups = this.data.getGroups().getGroup();
		for (GroupType group : groups) {
			if(group.getId().equals(groupId)){
				toRemove = group;
				break;
			}
		}
		
		if(toRemove != null){
			groups.remove(toRemove);
			this.setDirty();
		}
	}
	
	public static List<Integer> getAnnouncedAttempts(ContestantType contestant, DisciplineKind discipline) throws WeightliftingDataException {
		if(contestant == null){
			throw new WeightliftingDataException("Provided contestant is null");
		}
		
		ResultType anouncements = contestant.getAnouncements();
		if(anouncements == null){
			anouncements = newResults();
			contestant.setAnouncements(anouncements);
		}
		
		List<Integer> attempts;
		if(discipline == DisciplineKind.SNATCH){
			attempts = anouncements.getSnatch();
		}else{
			attempts = anouncements.getCleanAndJerk();
		}
		
		return attempts;
	}
	
	/** Bit of duplicity - but what do you want for free */
	public static List<Integer> getResults(ContestantType contestant, DisciplineKind discipline) throws WeightliftingDataException {
		if(contestant == null){
			throw new WeightliftingDataException("Provided contestant is null");
		}
		
		ResultType results = contestant.getResults();
		if(results == null){
			results = newResults();
			contestant.setResults(results);
		}
		
		List<Integer> attempts;
		if(discipline == DisciplineKind.SNATCH){
			attempts = results.getSnatch();
		}else{
			attempts = results.getCleanAndJerk();
		}
		
		return attempts;
	}
	
	public void addAnnouncement(BigInteger contestantId, DisciplineKind discipline, Integer weight) throws WeightliftingDataException {
		ContestantType contestant = getContestantById(contestantId);
		List<Integer> attempts = getAnnouncedAttempts(contestant, discipline);
		
		attempts.add(weight);
		Collections.sort(attempts);
		announceAttempt(new AnnouncementItem(contestantId, discipline, weight));
	}
	
	public void removeAnnouncement(BigInteger contestantId, DisciplineKind discipline, Integer weight) throws WeightliftingDataException {
		ContestantType contestant = getContestantById(contestantId);
		List<Integer> attempts = getAnnouncedAttempts(contestant, discipline);
		
		while(attempts.remove(weight)){};
		announceAttempt(new AnnouncementItem(contestantId, discipline, 0));
	}
	
	public void announceAttempt(AnnouncementItem announcement){
		setChanged();
		notifyObservers(announcement);
	}
	
	public void copyBasesToAnouncements(){
		List<ContestantType> contestants = this.data.getContestants().getConstestant();
		for (ContestantType contestant : contestants) {
			copyBasesToAnouncements(contestant);
		}
	}
	
	private static void copyBasesToAnouncements(ContestantType contestant) {
		ResultType anouncements = contestant.getAnouncements();
		if(anouncements == null) {
			contestant.setAnouncements(newResults());
			anouncements = contestant.getAnouncements();
		}
		
		int base = contestant.getSnatchBase();
		if(base > 0){
			anouncements.getSnatch().add(base);
			Collections.sort(anouncements.getSnatch());
		}
	
		base = contestant.getCleanAndJerkBase();
		if(base > 0){
			anouncements.getCleanAndJerk().add(base);
			Collections.sort(anouncements.getCleanAndJerk());
		}
	}
	
	
	public boolean isBasesAnouncementsSynced() {
		return basesAnouncementsSynced;
	}

	public void setBasesAnouncementsSynced(boolean basesAnouncementsSynced) {
		this.basesAnouncementsSynced = basesAnouncementsSynced;
	}

	public synchronized ContestantType addContestant(ContestantFactory factory){
		ContestantType contestant = factory.newContestant();
		int contestantId = getMaxContestantId() + 1;
		contestant.setId(BigInteger.valueOf(contestantId));
		this.data.getContestants().getConstestant().add(contestant);
		return contestant;
	}
	
	public static GeneralType newGeneral(){
		ObjectFactory factory = new ObjectFactory();
		GeneralType general = factory.createGeneralType();
		general.setDirector("");
		general.setLocation("");
		general.setTitle("");
		
		try {
			GregorianCalendar date = new GregorianCalendar();
			XMLGregorianCalendar xmlDate = DatatypeFactory.newInstance().newXMLGregorianCalendar(date);
			general.setDate(xmlDate);
		} catch (DatatypeConfigurationException e) {
			logger.error("Problem while creating date in the new file: " + e.getMessage());
		}
		
		RecordType maleRecord = factory.createRecordType();
		maleRecord.setBodyWeight(BigDecimal.valueOf(MALE_RECORD_WEIGHT));
		maleRecord.setCoefficient(BigDecimal.valueOf(MALE_RECORD_COEFFICIENT));
		general.setMaleRecord(maleRecord);
		
		RecordType femaleRecord = factory.createRecordType();
		femaleRecord.setBodyWeight(BigDecimal.valueOf(FEMALE_RECORD_WEIGHT));
		femaleRecord.setCoefficient(BigDecimal.valueOf(FEMALE_RECORD_COEFFICIENT));
		general.setFemaleRecord(femaleRecord);
		
		return general;
	}
	
	public static GroupType newGroup(int id) {
		ObjectFactory factory = new ObjectFactory();
		GroupType group = factory.createGroupType();
		
		BigInteger value = BigInteger.valueOf(id);
		group.setId(value);
		group.setBodyWeightReferee("");
		group.setMainReferee("");
		group.setResultsReferee("");
		group.setTechnicalReferee("");
		group.setTimeReferee("");
		group.getSideReferee();
		
		return group;
	}
	
	public static ResultType newResults() {
		ObjectFactory factory = new ObjectFactory();
		ResultType results = factory.createResultType();
		//lists are allocated when needed
		return results;
	}
	
	public int getMaxGroupId(){
		int max = -1;
		if(this.data == null){
			return max;
		}
		
		List<GroupType> groups = this.data.getGroups().getGroup();
		for (GroupType group : groups) {
			max = Math.max(group.getId().intValue(), max);
		}
		return max;
	}
	
	public int getMaxContestantId(){
		int max = -1;
		if(this.data == null){
			return max;
		}
		
		List<ContestantType> contestants = this.data.getContestants().getConstestant();
		for (ContestantType contestant : contestants) {
			max = Math.max(contestant.getId().intValue(), max);
		}
		return max;
	}
	
	public static ContestantType newContestant(int groupId){
		ObjectFactory factory = new ObjectFactory();
		ContestantType contestant = factory.createContestantType();
		contestant.setGroupId(BigInteger.valueOf(groupId));
		return contestant;
	}
	
	/**
	 * Search for last non-deleted group
	 * @param group The group to be deleted. We have to skip it during searching. 
	 * @return Last group that remains in competition.
	 * */
	public GroupType getLastRemainingGroup(BigInteger groupId) {
		List<GroupType> groups = data.getGroups().getGroup();
		GroupType targetGroup = null;
		for (GroupType tmpGroup : groups) {
			if(tmpGroup.getId().equals(groupId)){
				continue;
			}
			targetGroup = tmpGroup;
		}
		
		return targetGroup;
	}
	
	public static int getContestantTotal(ContestantType contestant) {
		if(contestant.getResults() != null) {
			int snatch = 0;
			if(contestant.getResults().getSnatch() != null) {
				for (int result : contestant.getResults().getSnatch()) {
					snatch = Math.max(snatch, result);
				}
			}
			
			int cleanAndJerk = 0;
			if(contestant.getResults().getCleanAndJerk() != null) {
				for (int result : contestant.getResults().getCleanAndJerk()) {
					cleanAndJerk = Math.max(cleanAndJerk, result);
				}
			}
			
			return snatch + cleanAndJerk;
		}
		logger.log(Level.ERROR, "Contestant has no results object.");
		return 0;
	}
	
	public SinclairUtils getSinclairUtils() {
		if(this.sinclairUtils == null) {
			this.sinclairUtils = new SinclairUtils(
				this.data.getGeneral().getMaleRecord().getBodyWeight().doubleValue(), 
				this.data.getGeneral().getMaleRecord().getCoefficient().doubleValue(),
				this.data.getGeneral().getFemaleRecord().getBodyWeight().doubleValue(),
				this.data.getGeneral().getFemaleRecord().getCoefficient().doubleValue()
			);
		}
		
		return this.sinclairUtils;
	}
	
	public TeamOrderCache getTeamCache() {
		if(this.teamCache == null){
			this.teamCache = new TeamOrderCache(this);
		}
		
		return this.teamCache;
	}
	
	public double getContestantSinclairScore(ContestantType contestant) {
		SinclairUtils scUtils = this.getSinclairUtils();
		int year = this.getData().getGeneral().getDate().getYear();
		int total = getContestantTotal(contestant);
		if(contestant.getYearBirth() != null 
				&& (contestant.getBodyWeight() != null && contestant.getBodyWeight().doubleValue() > 0)) {
			
			int age = year - contestant.getYearBirth().getYear();
			double score = scUtils.getSinclairScore(
					contestant.getGender(),
					age,
					contestant.getAgeCategory() == AgeCategoryType.MASTER,
					contestant.getBodyWeight().doubleValue(),
					total
				);
			
			//Since the begining of 2012 there are bonus points for joung men participating in adult's teams. 
			if(year >= 2012 && contestant.getTeamMembership() == TeamMembershipType.TEAM 
					&& contestant.getAgeCategory() == AgeCategoryType.ADULT && score != 0 && contestant.getGender() == GenderType.MALE){
				score += scUtils.getJuniorBonus(age);
			}
			
			return score;
		}
		return -1;
	}
	
	public double getSafeScore(ContestantType contestant){
		double contestantScore = getContestantSinclairScore(contestant);
		if (0 > contestantScore){
			logger.error("Constestant " + contestant.getName() + "(" + contestant.getClub() + ") has invalid age or body weight.");
			contestantScore = 0;
		}
		return contestantScore;
	}
	
	private void initOrderMap(){
		this.orderMap = new TreeMap<Category, Set<Double>>();
		for (ContestantType contestant : this.data.getContestants().getConstestant()) {
			if(contestant.getTeamMembership() == TeamMembershipType.TEAM){
				continue; //Teams has their own cache
			}
			Category category = new Category(contestant);
			Set<Double> scores = this.orderMap.get(category);
			if(scores == null){
				scores = new HashSet<Double>(20);
				this.orderMap.put(category, scores);
			}
			scores.add(getSafeScore(contestant));
		}
	}
	
	public int getContestantOrder(ContestantType contestant){
		
		if(contestant.getTeamMembership() == TeamMembershipType.TEAM){
			throw new RuntimeException(new WeightliftingDataException("This method is not intended for use with team members. Call getTeamCache().getTeamOrder(contestant)")); 
		}
		
		int order = 1;
		double contestantScore = getSafeScore(contestant);
		
		if(orderMap == null){
			initOrderMap();
		}
		
		Category category = new Category(contestant);
		Set<Double> scores = this.orderMap.get(category);
		if(scores != null) {
			for (Double opponentScore : scores) {
				if(opponentScore > contestantScore){
					order += 1;
				}
			}
		}
		return order;
	}
	
	public ContestantType getContestantById(BigInteger id){
		List<ContestantType> contestants = this.data.getContestants().getConstestant();
		for (ContestantType contestant : contestants) {
			if(contestant.getId().equals(id)){
				return contestant;
			}
		}
		return null;
	}
	
	private static JAXBContext getJaxbContext() throws JAXBException {
		return JAXBContext.newInstance("weightlifting.data.structures");
	}

	public Competition getData() {
		return data;
	}

	public void setData(Competition data) {
		if(this.data != data){
			setSaved(false);
		}
		this.data = data;
	}

	public File getXmlFile() {
		return xmlFile;
	}

	public void setXmlFile(File xmlFile) {
		if(this.xmlFile != null && this.xmlFile.compareTo(xmlFile) != 0){
			setSaved(false);
		}
		this.xmlFile = xmlFile;
	}

	public boolean isSaved() {
		return saved;
	}

	public void setSaved(boolean saved) {
		if(this.saved != saved){
			this.saved = saved;
			setChanged();
			notifyObservers(SAVE);
		}
	}
	
	public boolean isDirty(){
		return ! isSaved();
	}
	public void setDirty(){
		this.setSaved(false);
		this.getTeamCache().invalidateScore();
		this.orderMap = null; //team ordermap is nulled in teamCache
		this.getSinclairUtils();
		this.setChanged();
		this.notifyObservers();
	}
	
	public boolean hasData(){
		return this.data != null;
	}
}
