package weightlifting.data;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import weightlifting.data.structures.ContestantType;
import weightlifting.data.structures.TeamMembershipType;

public class TeamOrderCache {

	private CompetitionDataHandler data = null;
	private Set<Team> teams;
	private boolean cacheValid = false;
	private Map<Category, Set<Double>> orderMap = null;

	public TeamOrderCache(CompetitionDataHandler data) {
		this.data = data;
		this.teams = new HashSet<Team>(32);
	}
	
	public void rebuildCache(){
		this.teams.clear();
		this.orderMap = null;
		
		Contestants: for (ContestantType contestant : this.data.getData().getContestants().getConstestant()) {
			
			if(contestant.getTeamMembership() != TeamMembershipType.TEAM){
				continue;
			}
			
			for (Team team : this.teams) {
				if(team.fit(contestant)){
					team.addMember(contestant);
					continue Contestants;
				}
			}
			
			Team newTeam = new Team(this.data, contestant);
			newTeam.addMember(contestant);
			this.teams.add(newTeam);
		}
	
		this.cacheValid = true;
	}
	
	/**
	 * @param contestant specifies team that's score we want to know. 
	 * @return sinclair score points for team specified by contestant
	 * */
	public double getContestantTeamScore(ContestantType contestant) {
		for (Team team : this.teams) {
			if(team.fit(contestant)){
				return team.getResults().getSinclair().doubleValue();
			}
		}
		return 0;
	}
	
	/**
	 * Returns Team object that fits given contestant
	 * @param contestant sample contestant or even team to specify team we are looking for.
	 * @return team specified by contestant vlaues across teams in same category.
	 * */
	public Team getTeam(ContestantType contestant) {
		if( ! cacheValid){
			this.rebuildCache();
		}
		
		for (Team team : this.teams) {
			if( ! team.getMembershipFilter().fit(contestant)){
				continue; //Different category
			}
			
			if(team.fit(contestant)){
				return team;
			}
		}
		return null;
	}
	
	/**
	 * Calculates team's order in it's category. Team is specified by given contestant.
	 * @param contestant sample contestant or even team to specify team we are looking for.
	 * @return order of team specified by contestant vlaues across teams in same category.
	 * */
	public int getTeamOrder(ContestantType contestant) {
		int order = 1;

		if( ! cacheValid){
			this.rebuildCache();
		}
		
		double contestantTeamScore = this.getContestantTeamScore(contestant);
		
		if(this.orderMap == null){
			buildOrderMap();
		}
		
		Category category = new Category(contestant);
		Set<Double> scores = this.orderMap.get(category);
		if(scores != null) {
			for (Double opponentScore : scores) {
				if(opponentScore > contestantTeamScore){
					order += 1;
				}
			}
		}
		return order;
	}
	
	public boolean isValid() {
		return cacheValid;
	}

	protected void setValid(boolean valid) {
		this.cacheValid = valid;
	}
	
	/**
	 * Invalidates whole cache. All teams have to be rebuilt. Call this method when any contestant data realted to
	 * team membership has changed (gender, age category, team membership, team name, club name).
	 * 
	 * In case of results change call just invalidateScore(), it doesn't rebuild all team, just recalculates score.
	 * */
	public void invalidate() {
		this.cacheValid = false;
	}
	
	/**
	 * Invalidates score in all teams and forces each team item to recalculate it's sinclair score value.
	 * This should be called whenever contestant's results change.
	 * */
	public void invalidateScore() {
		this.orderMap = null;
		for (Team team : this.teams) {
			team.invalidateScore();
		}
	}
	
	private void buildOrderMap() {
		this.orderMap = new TreeMap<Category, Set<Double>>();
		
		if( ! cacheValid){
			this.rebuildCache();
		}
		
		for (Team team : this.teams) {

			Category category = team.getCategory();
			Set<Double> scores = this.orderMap.get(category);
			if(scores == null){
				scores = new HashSet<Double>(20);
				this.orderMap.put(category, scores);
			}
			scores.add(team.getResults().getSinclair().doubleValue());
		}
	}	
}
