package weightlifting.settings;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

import org.apache.log4j.Logger;

import weightlifting.gui.CompetitionWindow;

public class Settings {
	
	private static Logger logger = Logger.getLogger(CompetitionWindow.class);
	
	public static final String AUTOSAVE_PROPERTY = "autosave";
	public static final String LAST_FOLDER_PROPERTY = "lastFolder";
	public static final String WEBSERVER_PORT_PROPERTY = "webserverPort";
	public static final String USE_JUNIOR_BONUS_PROPERTY = "useJuniorBonus";
	public static final String SHOW_ADVANCED_OPTIONS = "showAdvancedOptions";
	
	public static final int DEFAULT_WEBSERVER_PORT = 80; 
	
	private Properties properties = null;
	private File settingsFile = null;

	public Settings(File settingsFile){
		this.settingsFile = settingsFile;
		try {
			this.properties = new Properties();
			FileInputStream input = new FileInputStream(settingsFile);
			this.properties.loadFromXML(input);
			input.close();
		}catch(IOException ioe){
			logger.error("Problem while loading persistent settings.", ioe);
		}
	}

	public void save() {
		try {
			FileOutputStream output = new FileOutputStream(settingsFile);
			this.properties.storeToXML(output, "Persistent settings for programme");
			output.close();
		}catch(IOException ioe){
			logger.error("Problem while saving persistent settings.", ioe);
		}
		
	}
	
	Properties getProperties() {
		return properties;
	}
	
	public boolean isAutosave() {
		return Boolean.parseBoolean(this.properties.getProperty(AUTOSAVE_PROPERTY, Boolean.FALSE.toString()));
	}
	public void setAutosave(boolean autosave) {
		this.properties.setProperty(AUTOSAVE_PROPERTY, autosave ? Boolean.TRUE.toString() : Boolean.FALSE.toString());
	}
	
	public String getLastFolder() {
		return this.properties.getProperty(LAST_FOLDER_PROPERTY, System.getProperty("user.home"));
	}

	public void setLastFolder(String lastFolder) {
		File test = new File(lastFolder);
		String finalFolder = lastFolder;
		if( ! test.isDirectory() ){
			finalFolder = test.getPath();
		}
		this.properties.setProperty(LAST_FOLDER_PROPERTY, finalFolder);
	}
	
	public int getWebserverPort(){
		return Integer.parseInt(this.properties.getProperty(WEBSERVER_PORT_PROPERTY, "" + DEFAULT_WEBSERVER_PORT), 10);
	}
	
	public void setWebserverPort(int port){
		this.properties.setProperty(WEBSERVER_PORT_PROPERTY, "" + port);
	}
	
	public boolean isShowAdvancedOptions(){
		return Boolean.parseBoolean(this.properties.getProperty(SHOW_ADVANCED_OPTIONS, Boolean.FALSE.toString()));
	}
}
