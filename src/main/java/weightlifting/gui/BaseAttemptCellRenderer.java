package weightlifting.gui;

import weightlifting.data.DisciplineKind;
import weightlifting.data.structures.ContestantType;

public class BaseAttemptCellRenderer extends AnouncedAttemptCellRenderer {
	private static final long serialVersionUID = 3046711772101532283L;

	public BaseAttemptCellRenderer(DisciplineKind baseKind, int order) {
		super(baseKind, order);
	}

	@Override
	protected String getAnouncementText(ContestantType contestant) {
		String anouncementText = super.getAnouncementText(contestant);
		
		if(this.order == 1 && (anouncementText == null || anouncementText.isEmpty())){
			if(this.discipline == DisciplineKind.SNATCH) {
				anouncementText = Integer.toString(contestant.getSnatchBase());
			}else{
				anouncementText = Integer.toString(contestant.getCleanAndJerkBase());
			}
		}
		
		return anouncementText;
	}

	
}
