package weightlifting.gui;

import java.awt.Component;
import java.util.Map;

import javax.swing.DefaultCellEditor;
import javax.swing.JComboBox;
import javax.swing.JTable;


public class EnumCellEditor<E extends Enum<E>> extends DefaultCellEditor {
	
	private class TranslatableItem{
		E value;
		String translation;
		public TranslatableItem(E value, String translation) {
			this.value = value;
			this.translation = translation;
		}
		
		public String toString(){
			return this.translation;
		}
	}
	
	private static final long serialVersionUID = 7949848488522192804L;
	
	private E base;
	private JComboBox<TranslatableItem> myCombo;
	public EnumCellEditor(final E base){
		this(base, null);
	}
	
	public EnumCellEditor(final E base, Map<E, String> translations){
		super(new JComboBox<E>());
		this.base = base;
		myCombo = new JComboBox<TranslatableItem>();
		E[] items = base.getDeclaringClass().getEnumConstants();
		for (E e : items) {
			String translation = translations != null ? translations.get(e) : null;
			myCombo.addItem(new TranslatableItem(e, translation != null ? translation : e.toString()));	
		}
	}
	
	@Override
	public Component getTableCellEditorComponent(JTable table, Object cellValue,
			boolean isSelected, int row, int col) {
		
		if(cellValue != null && cellValue.getClass() == this.base.getDeclaringClass()){
			for(int i = 0; i < this.myCombo.getItemCount(); i++){
				Object currentItem = myCombo.getItemAt(i);   
				if(currentItem instanceof EnumCellEditor<?>.TranslatableItem){
					if(((EnumCellEditor<?>.TranslatableItem) currentItem).value == cellValue){
						myCombo.setSelectedItem(currentItem);
						break;
					}
				} else if(currentItem == cellValue){
					myCombo.setSelectedItem(currentItem);
					break;
				}
			}
		}
		return myCombo;
	}

	@Override
	public Object getCellEditorValue() {
		Object selectedItem = myCombo.getSelectedItem();   
		if(selectedItem instanceof EnumCellEditor<?>.TranslatableItem){
			return ((EnumCellEditor<?>.TranslatableItem) selectedItem).value;
		}
		return selectedItem;
	}

	@Override
	public Component getComponent() {
		return myCombo;
	}	

}
