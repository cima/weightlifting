package weightlifting.gui;

public interface ObservableChangeSource {
	public void addObservableChange(ObservableChange change);
	public void removeObservableChange(ObservableChange change);
}
