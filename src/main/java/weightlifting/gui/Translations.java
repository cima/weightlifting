package weightlifting.gui;

import java.util.HashMap;
import java.util.Map;

import weightlifting.data.structures.AgeCategoryType;
import weightlifting.data.structures.GenderType;
import weightlifting.data.structures.TeamMembershipType;

public class Translations {

	private static Map<GenderType, String> genderEnumMap;
	
	public static Map<GenderType, String> getGenderTranslations(){
		if(genderEnumMap == null){
			genderEnumMap = new HashMap<GenderType, String>(2);
			
			genderEnumMap.put(GenderType.MALE, "Mu�");
			genderEnumMap.put(GenderType.FEMALE, "�ena");
		}
		return genderEnumMap;
	}
	
	//---------------------------------------------------------------//
	
	private static Map<AgeCategoryType, String> ageCategoryEnumMap;
	
	public static Map<AgeCategoryType, String> getAgeCategoryTranslations(){
		if(ageCategoryEnumMap == null){
			ageCategoryEnumMap = new HashMap<AgeCategoryType, String>(2);
			
			ageCategoryEnumMap.put(AgeCategoryType.ADULT, "Dosp�l�");
			ageCategoryEnumMap.put(AgeCategoryType.JUNIOR, "Junio�i");
			ageCategoryEnumMap.put(AgeCategoryType.MASTER, "Veter�ni");
			ageCategoryEnumMap.put(AgeCategoryType.SCHOOL, "��ci");
		}
		return ageCategoryEnumMap;
	}
	
	//---------------------------------------------------------------//
	
	private static Map<TeamMembershipType, String> teamMembershipEnumMap;
	
	public static Map<TeamMembershipType, String> getTeamMembershipTranslations(){
		if(teamMembershipEnumMap == null){
			teamMembershipEnumMap = new HashMap<TeamMembershipType, String>(2);
			
			teamMembershipEnumMap.put(TeamMembershipType.SINGLE, "Jednotlivci");
			teamMembershipEnumMap.put(TeamMembershipType.TEAM, "Dru�stva");
			teamMembershipEnumMap.put(TeamMembershipType.OFF_CONTEST, "Mimo sout�");
		}
		return teamMembershipEnumMap;
	}
}
