package weightlifting.gui;

import java.awt.Component;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.TableCellEditor;

public class SelectAllCellDecorator extends TableCellEditorDecorator implements FocusListener {

	private JTextField text = null;
	
	public SelectAllCellDecorator(TableCellEditor editor) {
		super(editor);
	}

	@Override
	public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {

		Component field = super.getTableCellEditorComponent(table, value, isSelected, row, column);
		
		if(this.text == null && field instanceof JTextField){
			this.text = (JTextField)field;
			this.text.addFocusListener(this);
		}
		
		if(this.text != null){
			this.text.selectAll();
		}
		return field;
	}
	
	@Override
	public void focusGained(FocusEvent e) {
		this.text.selectAll();
	}

	@Override
	public void focusLost(FocusEvent e) {
		// not needed here
	}
}
