package weightlifting.gui;

import java.awt.Component;
import java.util.LinkedList;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.event.ChangeListener;

import weightlifting.data.CompetitionDataHandler;
import weightlifting.data.structures.Competition;
import weightlifting.data.structures.GroupType;
import weightlifting.settings.Settings;

public class CompetitionPanel extends JPanel {
	private static final long serialVersionUID = 3116203428493595777L;
	
	// --- data
	private CompetitionDataHandler competitionDataHandler;
	private static final String GROUP_TEXT = "V�n� listina ";
	
	private Settings settings;
	
	// --- gui
	private JTabbedPane tabs;
	
	private GeneralView generalView;
	private List<GroupView> groupViews;
	private ResultsView resultsView;
	
	private ObservableChange groupChange;
	
	public CompetitionPanel(CompetitionDataHandler data, Settings settings){
		this.competitionDataHandler = data;
		this.settings = settings;
		this.tabs = new JTabbedPane();
		
		this.setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
		
		Competition cmp = this.competitionDataHandler.getData();
		if(cmp.getGeneral() != null) {
			this.generalView = new GeneralView(this.competitionDataHandler);
			this.tabs.addTab("Obecn�", this.generalView);
		}
		
		this.groupChange = new ObservableChange();
		if(cmp.getGroups() != null) {
			this.groupViews = new LinkedList<GroupView>();
			List<GroupType> groups = cmp.getGroups().getGroup();
			int i = 1;
			for (GroupType group : groups) {
				GroupView gw = new GroupView(this.competitionDataHandler, group.getId());
				gw.addObservableChange(this.groupChange);
				this.groupViews.add(gw);
				this.tabs.addTab(GROUP_TEXT + i++, gw);	
			}
		}
		
		//We always want to see results tab, because of possibility to fill it after presence
		this.resultsView = new ResultsView(this.competitionDataHandler, this.settings);
		this.groupChange.addObserver(this.resultsView);
		this.tabs.addTab("V�sledky", this.resultsView);
		
		this.add(this.tabs);
	}
	
	public void addTabsChangeListener(ChangeListener listener){
		this.tabs.addChangeListener(listener);
	}
	
	public void removeTabsChangeListener(ChangeListener listener){
		this.tabs.removeChangeListener(listener);
	}
	
	public boolean isGroupSelected(){
		return this.tabs.getSelectedComponent() instanceof GroupView;
	}
	
	public GroupView getSelectedGroup(){
		Component selectedComponent = this.tabs.getSelectedComponent();
		if(this.tabs.getSelectedComponent() instanceof GroupView){
			return (GroupView)selectedComponent;
		}
		return null;
	}
	
	public void addGroup(GroupType group){
		int lastGroup = getLastGroup();
		GroupView gw = new GroupView(this.competitionDataHandler, group.getId());
		gw.addObservableChange(this.groupChange);
		this.groupViews.add(gw);
		this.tabs.insertTab(GROUP_TEXT + this.groupViews.size(), null, gw, "", lastGroup + 1);
		this.groupChange.changeOccured(gw);
	}

	public void RemoveGroup(GroupView groupView){
		groupView.removeObservableChange(this.groupChange);
		this.groupViews.remove(groupView);
		this.tabs.remove(groupView);
		this.groupChange.changeOccured(groupView);
	}
	
	private int getLastGroup(){
		int count = this.tabs.getTabCount();
		int retval = -1; 
		for(int i = 0; i < count; i++){
			Component component = this.tabs.getComponentAt(i);
			if(component instanceof GroupView){
				retval = i;
			}
		}
		return retval;
	}
	
	public void RenameGroupTabs(){
		int count = this.tabs.getTabCount();
		int num = 1; 
		for(int i = 0; i < count; i++){
			Component component = this.tabs.getComponentAt(i);
			if(component instanceof GroupView){
				this.tabs.setTitleAt(i, GROUP_TEXT + num++);
			}
		}
	}
	
	public void setCoefficientEditEnabled(boolean enabled) {
		this.generalView.setCoefficientEditEnabled(enabled);
		this.tabs.setSelectedIndex(0);
	}
}
