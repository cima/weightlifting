package weightlifting.gui;

import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.util.LinkedList;
import java.util.List;

import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JSeparator;
import javax.swing.KeyStroke;

public class FileMenu extends JMenu {
	
	private static final long serialVersionUID = -1406133472940079806L;
	
	static String NEW_ACTION_COMMAND = "new";
	static String OPEN_ACTION_COMMAND = "open";
	static String SAVE_ACTION_COMMAND = "save";
	static String SAVEAS_ACTION_COMMAND = "saveAs";
	static String EXPORT_ACTION_COMMAND = "export";
	static String PRINT_GROUPS_ACTION_COMMAND = "print groups";
	static String PRINT_RESULTS_ACTION_COMMAND = "print results";
	static String PRINT_EMPTY_RESULTS_ACTION_COMMAND = "print empty results";
	static String PRINT_TECHNICAL_ACTION_COMMAND = "print technical";
	static String EXIT_ACTION_COMMAND = "exit";
	
	private JMenuItem newItem;
	private JMenuItem openItem;
	private JMenuItem saveItem;
	private JMenuItem saveAsItem;
	
	private JMenuItem printGroupsItem;
	private JMenuItem printTechnicalItem;
	private JMenuItem printEmptyResultsItem;
	private JMenuItem printResultsItem;
	
	private JMenuItem exitItem;
	
	private List<JMenuItem> items;
	
	public FileMenu(){
		super("Soubor");
		this.setMnemonic(KeyEvent.VK_F);
		
		this.items = new LinkedList<JMenuItem>();
		
		newItem = new JMenuItem("Nov�");
		newItem.setMnemonic(KeyEvent.VK_N);
		newItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, KeyEvent.CTRL_DOWN_MASK));
		newItem.setActionCommand(NEW_ACTION_COMMAND);
		this.items.add(newItem);
		this.add(newItem);
		
		openItem = new JMenuItem("Otev��t ...");
		openItem.setMnemonic(KeyEvent.VK_O);
		openItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, KeyEvent.CTRL_DOWN_MASK));
		openItem.setActionCommand(OPEN_ACTION_COMMAND);
		this.items.add(openItem);
		this.add(openItem);
		
		saveItem = new JMenuItem("Ulo�it");
		saveItem.setMnemonic(KeyEvent.VK_S);
		saveItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, KeyEvent.CTRL_DOWN_MASK));
		saveItem.setActionCommand(SAVE_ACTION_COMMAND);
		this.items.add(saveItem);
		this.add(saveItem);

		saveAsItem = new JMenuItem("Ulo�it jako ...");
		saveAsItem.setActionCommand(SAVEAS_ACTION_COMMAND);
		this.items.add(saveAsItem);
		this.add(saveAsItem);
		
		this.add(new JSeparator());
		
		printGroupsItem = new JMenuItem("Tisk v�n�ch listin ...");
		printGroupsItem.setActionCommand(PRINT_GROUPS_ACTION_COMMAND);
		this.items.add(printGroupsItem);
		this.add(printGroupsItem);
		
		printTechnicalItem = new JMenuItem("Tisk tech. z�pisu ...");
		printTechnicalItem.setActionCommand(PRINT_TECHNICAL_ACTION_COMMAND);
		this.items.add(printTechnicalItem);
		this.add(printTechnicalItem);
		
		printEmptyResultsItem = new JMenuItem("Tisk pr�zdn�ho z�pisu ...");
		printEmptyResultsItem.setActionCommand(PRINT_EMPTY_RESULTS_ACTION_COMMAND);
		this.items.add(printEmptyResultsItem);
		this.add(printEmptyResultsItem);
		
		printResultsItem = new JMenuItem("Tisk v�sledk� ...");
		printResultsItem.setActionCommand(PRINT_RESULTS_ACTION_COMMAND);
		this.items.add(printResultsItem);
		this.add(printResultsItem);
		
		this.add(new JSeparator());
		
		exitItem = new JMenuItem("Konec");
		exitItem.setMnemonic(KeyEvent.VK_E);
		exitItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F4, KeyEvent.ALT_DOWN_MASK));
		exitItem.setActionCommand(EXIT_ACTION_COMMAND);
		this.items.add(exitItem);
		this.add(exitItem);
	}
	
	public void setSaveEnabled(boolean enabled){
		this.saveItem.setEnabled(enabled);
	}
	public void setSaveAsEnabled(boolean enabled){
		this.saveAsItem.setEnabled(enabled);
	}
	public void setExportEnabled(boolean enabled){
		//this.exportItem.setEnabled(enabled);
		this.printResultsItem.setEnabled(enabled);
		this.printTechnicalItem.setEnabled(enabled);
	}
	
	@Override
	public void addActionListener(ActionListener listener){
		super.addActionListener(listener);
		for (JMenuItem item : items) {
			item.addActionListener(listener);
		}
	}
	
	@Override
	public void removeActionListener(ActionListener listener){
		super.removeActionListener(listener);
		for (JMenuItem item : items) {
			item.removeActionListener(listener);
		}
	}
	
}
