package weightlifting.gui;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import javax.swing.GroupLayout;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.GroupLayout.Alignment;
import javax.swing.GroupLayout.ParallelGroup;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

public abstract class AutoGridPanel extends JPanel implements DocumentListener, PropertyChangeListener {

	private static final long serialVersionUID = 1046128261480055932L;
	protected GroupLayout layout;
	protected List<JLabel> labels;
	protected List<JComponent> components;
	
	public AutoGridPanel(){
		this.layout = new GroupLayout(this);
		this.layout.setAutoCreateGaps(true);
		this.layout.setAutoCreateContainerGaps(true);
		this.setLayout(this.layout);
		
		this.labels = new LinkedList<JLabel>();
		this.components = new LinkedList<JComponent>();
	}
	
	protected void align(){
		align(null);
	}
	
	protected void align(JComponent additionalComponent){
		for (JComponent item : this.components) {
			if(item instanceof JTextField){
				((JTextField)item).getDocument().addDocumentListener(this);
			}
		}
		
		GroupLayout.SequentialGroup hGroup = this.layout.createSequentialGroup();
		ParallelGroup pGroup = layout.createParallelGroup(Alignment.TRAILING);
		for (JLabel label : this.labels) {
			pGroup.addComponent(label);
		}

		hGroup.addGroup(pGroup);
		
		
		pGroup = layout.createParallelGroup();
		for (JComponent component : this.components) {
			pGroup.addComponent(component);
		}
		hGroup.addGroup(pGroup);
		
		if(additionalComponent != null){
			ParallelGroup pGroupAdd = layout.createParallelGroup();
			pGroupAdd.addGroup(hGroup);
			pGroupAdd.addComponent(additionalComponent);
			this.layout.setHorizontalGroup(pGroupAdd);
		}else{
			this.layout.setHorizontalGroup(hGroup);
		}
		//_----------------
		
		GroupLayout.SequentialGroup vGroup = this.layout.createSequentialGroup();
		Iterator<JLabel> labelIt = this.labels.iterator();
		Iterator<JComponent> compIt = this.components.iterator();
		
		while(labelIt.hasNext() && compIt.hasNext()){
			pGroup = layout.createParallelGroup(Alignment.BASELINE);
			pGroup.addComponent(labelIt.next());
			pGroup.addComponent(compIt.next());
			vGroup.addGroup(pGroup);
		}
		if(additionalComponent != null){
			vGroup.addComponent(additionalComponent);
		}
		this.layout.setVerticalGroup(vGroup);
	}
	
	@Override
	public void changedUpdate(DocumentEvent e) {
		harvestData();
	}
	@Override
	public void insertUpdate(DocumentEvent e) {
		harvestData();
	}
	@Override
	public void removeUpdate(DocumentEvent e) {
		harvestData();
	}
	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		harvestData();
	}
	
	protected abstract void harvestData();
}
