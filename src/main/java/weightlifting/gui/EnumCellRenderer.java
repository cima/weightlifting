package weightlifting.gui;

import java.awt.Component;
import java.util.Map;

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

public class EnumCellRenderer<E extends Enum<E>> extends DefaultTableCellRenderer {

	private static final long serialVersionUID = 9049598878142288858L;
	public Map<E, String> translations = null;

	public EnumCellRenderer(Map<E, String> translations) {
		this.translations = translations;
	}
	
	@Override
	public Component getTableCellRendererComponent(JTable table, Object value,
			boolean isSelected, boolean hasFocus, int row, int col) {

		Component renderer = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, col);
		
		if(value instanceof Enum<?>){
			String translated = translations.get(value);
			if(translated != null){
				setText(translated);
			}
		}
	
		return renderer;
	}
}
