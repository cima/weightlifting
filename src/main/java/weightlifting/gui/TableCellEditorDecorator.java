package weightlifting.gui;

import java.awt.Component;
import java.util.EventObject;

import javax.swing.JTable;
import javax.swing.event.CellEditorListener;
import javax.swing.table.TableCellEditor;

public class TableCellEditorDecorator implements TableCellEditor {

	
	private TableCellEditor editor;
	
	public TableCellEditorDecorator(TableCellEditor editor) {
		this.editor = editor;
	}
	
	@Override
	public void addCellEditorListener(CellEditorListener l) {
		this.editor.addCellEditorListener(l);
	}

	@Override
	public void cancelCellEditing() {
		this.editor.cancelCellEditing();
		
	}

	@Override
	public Object getCellEditorValue() {
		return this.editor.getCellEditorValue();
	}

	@Override
	public boolean isCellEditable(EventObject anEvent) {
		return this.editor.isCellEditable(anEvent);
	}

	@Override
	public void removeCellEditorListener(CellEditorListener l) {
		this.editor.removeCellEditorListener(l);
	}

	@Override
	public boolean shouldSelectCell(EventObject anEvent) {
		return this.editor.shouldSelectCell(anEvent);
	}

	@Override
	public boolean stopCellEditing() {
		return this.editor.stopCellEditing();
	}

	@Override
	public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
		return this.editor.getTableCellEditorComponent(table, value, isSelected, row, column);
	}

}
