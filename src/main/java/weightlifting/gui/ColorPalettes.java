package weightlifting.gui;

import java.awt.Color;

import weightlifting.data.structures.ContestantType;
import weightlifting.data.structures.GenderType;

public class ColorPalettes {
	
	/**
	 * public static final Color MALE_BASIC_COLOR = new Color(0xBFE6FF);
	public static final Color BOY_BASIC_COLOR = new Color(0xD6FFD6);
	public static final Color MASTER_BASIC_COLOR = new Color(0x999999);
	
	public static final Color FEMALE_BASIC_COLOR = new Color(0xFFCCCC);
	public static final Color GIRL_BASIC_COLOR = new Color(0xFFC9FF);
	public static final Color MASTRESS_BASIC_COLOR = new Color(0xFFF2D9);
	 * */
	
	public static final Color MALE_BASIC_COLOR = new Color(0xF2FAFF);
	public static final Color SCHOOL_BOY_BASIC_COLOR = new Color(0xFFFFD9);
	public static final Color BOY_BASIC_COLOR = new Color(0xF2FAF5);
	public static final Color MASTER_BASIC_COLOR = new Color(0xF0F0F5);
	
	public static final Color FEMALE_BASIC_COLOR = new Color(0xFFF2F2);
	public static final Color SCHOOL_GIRL_BASIC_COLOR = new Color(0xFFFFF5);
	public static final Color GIRL_BASIC_COLOR = new Color(0xFFF2FF);
	public static final Color MASTRESS_BASIC_COLOR = new Color(0xFFF5EB);
	
	public static final Color BASE_ATTEMPT_COLOR = new Color(0x0088EE);
	
	public static Color getColor(ContestantType contestant){
		if(contestant.getGender() == GenderType.MALE) {
			switch(contestant.getAgeCategory()){
			case SCHOOL:
				return SCHOOL_BOY_BASIC_COLOR;
			case JUNIOR:
				return BOY_BASIC_COLOR;
			case ADULT:
				return MALE_BASIC_COLOR;
			case MASTER:
				return MASTER_BASIC_COLOR;
			}
			
		}else{
			switch(contestant.getAgeCategory()){
			case SCHOOL:
				return SCHOOL_GIRL_BASIC_COLOR;
			case JUNIOR:
				return GIRL_BASIC_COLOR;
			case ADULT:
				return FEMALE_BASIC_COLOR;
			case MASTER:
				return MASTRESS_BASIC_COLOR;
			}
		}
		
		return MALE_BASIC_COLOR;
	}
}
