package weightlifting.gui;

import java.awt.event.ActionListener;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JSeparator;

import weightlifting.data.CompetitionDataHandler;
import weightlifting.data.structures.GroupType;

public class ContestantPopupMenu extends AutoListenPopupMenu implements Observer {

	private static final long serialVersionUID = -5253362223407579095L;
	
	static String DELETE_CONTESTANT_COMMAND = "delete contestant";
	static String MOVE_CONTESTANT_COMMAND = "move contestant";
	static String MOVE_UP_COMMAND = "move up";
	static String MOVE_DOWN_COMMAND = "move down";
	
	private int[] selectedRows;
	private JMenu moveMenu;
	
	public ContestantPopupMenu(){
		
		JMenuItem tmpItem;
		
		tmpItem = new JMenuItem("Posunout nahoru");
		tmpItem.setActionCommand(MOVE_UP_COMMAND);
		this.items.add(tmpItem);
		this.add(tmpItem);
		
		tmpItem = new JMenuItem("Posunout dolu");
		tmpItem.setActionCommand(MOVE_DOWN_COMMAND);
		this.items.add(tmpItem);
		this.add(tmpItem);
		
		this.moveMenu = new JMenu("Přesunout do");
		this.items.add(this.moveMenu);
		this.add(this.moveMenu);
		
		this.add(new JSeparator());
		
		tmpItem = new JMenuItem("Smazat závodníka");
		tmpItem.setActionCommand(DELETE_CONTESTANT_COMMAND);
		this.items.add(tmpItem);
		this.add(tmpItem);
		
	}

	@Override
	public void update(Observable o, Object arg) {
		if(o instanceof CompetitionDataHandler){
			this.updateMoveItems((CompetitionDataHandler)o);
		}
	}

	public void updateMoveItems(CompetitionDataHandler data){
		this.moveMenu.removeAll();
		
		int order = 1;
		List<GroupType> groups = data.getData().getGroups().getGroup();
		for (GroupType group : groups) {
			JMenuItem groupMenuItem = new JMenuItem("" + order + ". skupiny");
			groupMenuItem.setActionCommand(MOVE_CONTESTANT_COMMAND);
			groupMenuItem.putClientProperty("group", group);
			for (ActionListener listener : this.listenersCache) {
				groupMenuItem.addActionListener(listener);
			}
			this.moveMenu.add(groupMenuItem);
			order += 1;
		}
		
	}
	
	public boolean hasItem(Object component){
		return component instanceof JMenuItem && this.items.contains(component);
	}
	
	public int[] getSelectedRows() {
		return selectedRows;
	}

	public void setSelectedRows(int[] selectedRows) {
		this.selectedRows = selectedRows;
	}
	
}
