package weightlifting.gui;

import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.util.LinkedList;
import java.util.List;

import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JSeparator;
import javax.swing.KeyStroke;

public class HelpMenu extends JMenu {
	private static final long serialVersionUID = 2766087815445455263L;
	
	static String SHOW_HELP_ACTION_COMMAND = "show help";
	static String SHOW_IP_ACTION_COMMAND = "show IP addresses";
	static String SHOW_LOG_ACTION_COMMAND = "show debug log addresses";
	static String SHOW_ABOUT_COMMAND = "show about dialog";

	private JMenuItem helpItem;
	private JMenuItem showIpItem;
	private JMenuItem showLogItem;
	private JMenuItem aboutItem;
	
	private List<JMenuItem> items;
	
	public HelpMenu(){
		super("N�pov�da");
		
		this.items = new LinkedList<JMenuItem>();
		
		this.helpItem = new JMenuItem("N�pov�da");
		this.helpItem.setMnemonic(KeyEvent.VK_N);
		this.helpItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F1, 0));
		this.helpItem.setActionCommand(SHOW_HELP_ACTION_COMMAND);
		this.items.add(this.helpItem);
		this.add(this.helpItem);
		
		this.showIpItem = new JMenuItem("Zobrazit IP adresy");
		this.showIpItem.setMnemonic(KeyEvent.VK_P);
		this.showIpItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F12, 0));
		this.showIpItem.setActionCommand(SHOW_IP_ACTION_COMMAND);
		this.items.add(this.showIpItem);
		this.add(this.showIpItem);
		
		this.showLogItem = new JMenuItem("Debug log");
		this.showLogItem.setMnemonic(KeyEvent.VK_D);
		this.showLogItem.setActionCommand(SHOW_LOG_ACTION_COMMAND);
		this.items.add(this.showLogItem);
		this.add(this.showLogItem);
		
		this.add(new JSeparator());
		
		this.aboutItem = new JMenuItem("O programu");
		this.aboutItem.setActionCommand(SHOW_ABOUT_COMMAND);
		this.items.add(this.aboutItem);	
		this.add(this.aboutItem);
	}

	@Override
	public void addActionListener(ActionListener listener){
		super.addActionListener(listener);
		for (JMenuItem item : items) {
			item.addActionListener(listener);
		}
	}
	
	@Override
	public void removeActionListener(ActionListener listener){
		super.removeActionListener(listener);
		for (JMenuItem item : items) {
			item.removeActionListener(listener);
		}
	}
}
