package weightlifting.gui;

import java.awt.Component;

import javax.swing.JLabel;
import javax.swing.JTable;

import weightlifting.data.CompetitionDataHandler;
import weightlifting.data.structures.ContestantType;
import weightlifting.data.structures.TeamMembershipType;

public class SinclairCellRenderer extends AttemptCellRenderer {
	
	private static final long serialVersionUID = 5317227922114663921L;

	private CompetitionDataHandler data;
	
	SinclairCellRenderer(CompetitionDataHandler data){
		this.data = data;
	}
	
	@Override
	public Component getTableCellRendererComponent(JTable table, Object value, 
			boolean isSelected, boolean hasFocus, int row, int col) {
		
		Component renderer = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, col);
		Object contestantObject = table.getValueAt(row, -1);
		
		if(renderer instanceof JLabel && contestantObject instanceof ContestantType) {
			ContestantType contestant = (ContestantType)contestantObject;
			if(contestant.getTeamMembership() == TeamMembershipType.TEAM){
				JLabel label = (JLabel)renderer;
				String text = label.getText();
				text += " (" + formatter.format(data.getTeamCache().getContestantTeamScore(contestant)) + ")";
				label.setText(text);
			}
		}
		
		return renderer;
	}


	
	

}
