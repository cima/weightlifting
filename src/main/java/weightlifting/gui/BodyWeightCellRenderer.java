package weightlifting.gui;

import java.awt.Component;
import java.text.DecimalFormat;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

public class BodyWeightCellRenderer extends DefaultTableCellRenderer {    
	private static final long serialVersionUID = 9035224693353295908L;
	private DecimalFormat weightFormat = new DecimalFormat("##0.0");
	
	public BodyWeightCellRenderer() {
		super();
		this.setHorizontalAlignment(JLabel.RIGHT);
	}
	
	@Override
	public Component getTableCellRendererComponent(JTable table, Object value,
			boolean isSelected, boolean hasFocus, int row, int col) {

		Component renderer = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, col);
		//renderersetFont(table.getFont());
		
		String formated = null;
		if(value instanceof Number){
			try{
				formated = weightFormat.format(value);
			}catch(IllegalArgumentException iae){
				formated = null;
			}
		}
		
		if(formated != null){
			setText(formated);
		}
	
		return renderer;
	}
	
}
