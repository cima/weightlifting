package weightlifting.gui;

import weightlifting.data.ContestantTypeDecorator;
import weightlifting.data.structures.ContestantType;

public class ContestantIndexDecorator extends ContestantTypeDecorator {

	private int index = -1;

	public ContestantIndexDecorator(int index, ContestantType contestant) {
		super(contestant);
		this.index = index;
	}

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}
	
}
