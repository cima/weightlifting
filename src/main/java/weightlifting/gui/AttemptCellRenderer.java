package weightlifting.gui;

import java.awt.Color;
import java.awt.Component;
import java.text.DecimalFormat;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

import weightlifting.data.structures.ContestantType;

public class AttemptCellRenderer extends DefaultTableCellRenderer {

	private static final long serialVersionUID = -6364483508870282135L;
	protected static final DecimalFormat formatter = new DecimalFormat( "#0.0000" );

	private boolean colorize = true;
	
	@Override
	public Component getTableCellRendererComponent(JTable table, Object value,
			boolean isSelected, boolean hasFocus, int row, int col) {
		
		Object newValue = value;
		if (value instanceof Double) {
			newValue = formatter.format((Double)value);
		}
		Component renderer = super.getTableCellRendererComponent(table, newValue, isSelected, hasFocus, row, col);
		
		if( ! isSelected) {
			Object contestantObject = table.getValueAt(row, -1);
			if(colorize && contestantObject instanceof ContestantType) {
				this.setBackground(ColorPalettes.getColor((ContestantType)contestantObject));
			}else{
				this.setBackground(Color.WHITE);
			}
		}
		
		this.setForeground(new Color(0x000000));
		
		if (value instanceof String) {
			if(renderer instanceof JLabel) {
				((JLabel)renderer).setHorizontalTextPosition(JLabel.LEFT);
				((JLabel)renderer).setHorizontalAlignment(JLabel.LEFT);
			}
		}

		if (value instanceof Integer || value instanceof Double) {
			if(renderer instanceof JLabel) {
				((JLabel)renderer).setHorizontalTextPosition(JLabel.RIGHT);
				((JLabel)renderer).setHorizontalAlignment(JLabel.RIGHT);
			}
		}
		
		if (value instanceof Integer) {
			if((Integer)value < 0){
				this.setForeground(new Color(0xFF0000));
			}else{
				this.setForeground(new Color(0x000000));
			}
		}
		return renderer;
	}

	public boolean isColorize() {
		return colorize;
	}

	public void setColorize(boolean colorize) {
		this.colorize = colorize;
	}
}
