package weightlifting.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.math.BigInteger;
import java.util.List;

import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.event.DocumentListener;

import org.apache.log4j.Logger;

import weightlifting.data.CompetitionDataHandler;
import weightlifting.data.GroupIdContestantFactory;
import weightlifting.data.GroupIdContestantFilter;
import weightlifting.data.structures.GroupType;

public class GroupView extends AutoGridPanel implements DocumentListener, ObservableChangeSource, ActionListener {

	private static final long serialVersionUID = -1047676144567267253L;
	private static Logger logger = Logger.getLogger(GroupView.class);
	
	//data
	private CompetitionDataHandler data;
	private BigInteger groupId;
	private GroupType group = null;
	
	//gui
	
	private JTextField bodyWeightReferee;
	private JTextField technicalReferee;
	private JTextField resultsReferee;
	private JTextField timeReferee;
	private JTextField mainReferee;
	private JTextField sideReferee1;
	private JTextField sideReferee2;
	
	private ContestantView contestantView;
	
	public GroupView(CompetitionDataHandler data, BigInteger groupId){
		this.data = data;
		this.groupId = groupId;
		List<GroupType> groups = data.getData().getGroups().getGroup();
		for (GroupType group : groups) {
			if(group.getId().compareTo(this.groupId) == 0){
				this.group = group;
				break;
			}
		}

		if(group == null){
			logger.error("Group with given id " + this.groupId + " wasn't found.");
		}
		
		//<bodyWeightReferee>Jaromír Jílek</bodyWeightReferee>
		this.labels.add(new JLabel("V�il", JLabel.RIGHT));
		this.bodyWeightReferee = new JTextField(this.group.getBodyWeightReferee());
		this.components.add(this.bodyWeightReferee);
		
        //<technicalReferee>Zacharda</technicalReferee>
		this.labels.add(new JLabel("Technick� rozhod��", JLabel.RIGHT));
		this.technicalReferee = new JTextField(this.group.getTechnicalReferee());
		this.components.add(this.technicalReferee);
		
        //<resultsReferee>Martin Šimek</resultsReferee>
		this.labels.add(new JLabel("Z�pis", JLabel.RIGHT));
		this.resultsReferee = new JTextField(this.group.getResultsReferee());
		this.components.add(this.resultsReferee);
		
        //<timeReferee>Jaroslava Vančurová</timeReferee>
		this.labels.add(new JLabel("�asom�ra", JLabel.RIGHT));
		this.timeReferee = new JTextField(this.group.getTimeReferee());
		this.components.add(this.timeReferee);
		
        //<mainReferee>Zázvorka</mainReferee>
		this.labels.add(new JLabel("Hlavn� rozhod��", JLabel.RIGHT));
		this.mainReferee = new JTextField(this.group.getMainReferee());
		this.components.add(this.mainReferee);
		
        //<sideReferee>Jaromír Jílek</sideReferee>
		List<String> sideReferees = group.getSideReferee();
		this.labels.add(new JLabel("Postrann� rozhod��", JLabel.RIGHT));
		this.sideReferee1 = new JTextField(); 
		if(sideReferees.size() >= 1){
			this.sideReferee1.setText(sideReferees.get(0));
		}
		this.components.add(this.sideReferee1);
		
		this.labels.add(new JLabel("Druh� postrann� rozhod��", JLabel.RIGHT));
		this.sideReferee2 = new JTextField();
		if(sideReferees.size() >= 2){
			this.sideReferee1.setText(sideReferees.get(1));
		}
		this.components.add(this.sideReferee2);
		
		this.contestantView = new ContestantView(this.data, new GroupIdContestantFactory(this.groupId));
		this.contestantView.setContestantFilter(new GroupIdContestantFilter(this.groupId));
		
		align(this.contestantView);
		
		
	}

	protected void harvestData(){
		this.group.setBodyWeightReferee(bodyWeightReferee.getText());
		this.group.setTechnicalReferee(technicalReferee.getText());
		this.group.setResultsReferee(resultsReferee.getText());
		this.group.setTimeReferee(timeReferee.getText());
		this.group.setMainReferee(mainReferee.getText());
		
		this.group.getSideReferee().clear();
		if( ! sideReferee1.getText().trim().isEmpty()){
			this.group.getSideReferee().add(sideReferee1.getText());
		}
		if( ! sideReferee2.getText().trim().isEmpty()){
			this.group.getSideReferee().add(sideReferee2.getText());
		}
		
		if( ! this.data.isDirty()){
			this.data.setDirty();
		}
	}

	@Override
	public void addObservableChange(ObservableChange change) {
		this.contestantView.addObservableChange(change);		
	}

	@Override
	public void removeObservableChange(ObservableChange change) {
		this.contestantView.removeObservableChange(change);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		this.contestantView.actionPerformed(e);
	}
}

