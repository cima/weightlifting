package weightlifting.gui;

import java.awt.event.ActionListener;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

public class AutoListenPopupMenu extends JPopupMenu {
	private static final long serialVersionUID = 4911633867377263133L;
	
	protected List<JMenuItem> items;
	protected Set<ActionListener> listenersCache;
	
	AutoListenPopupMenu(){
		this.items = new LinkedList<JMenuItem>();
		this.listenersCache = new HashSet<ActionListener>();
	}
	
	public void addActionListener(ActionListener listener){
		this.listenersCache.add(listener);
		for (JMenuItem item : items) {
			item.addActionListener(listener);
		}
	}
	
	public void removeActionListener(ActionListener listener){
		this.listenersCache.remove(listener);
		for (JMenuItem item : items) {
			item.removeActionListener(listener);
		}
	}
}
