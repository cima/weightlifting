package weightlifting.gui;

import java.awt.Color;
import java.awt.Component;

import javax.swing.DefaultCellEditor;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeConstants;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.log4j.Logger;

/**
 * Year selector for table.
 * */
public class YearCellEditor extends DefaultCellEditor {
	private static final long serialVersionUID = 7949848488522192804L;
	private static Logger logger = Logger.getLogger(YearCellEditor.class);
	
    private static final Border redBorder = new LineBorder(Color.red);
    private static final Border blackBorder = new LineBorder(Color.black);
	
	private JTextField myYearText;
	
	public YearCellEditor(JTextField myYearText){
		super(myYearText);
		this.myYearText = myYearText;
	}
	
	@Override
	public Component getTableCellEditorComponent(JTable table, Object cellValue, boolean isSelected, int row, int col) {
		
		if(cellValue instanceof XMLGregorianCalendar){
			this.myYearText.setBorder(blackBorder);
			XMLGregorianCalendar cellYear = (XMLGregorianCalendar)cellValue;
			return super.getTableCellEditorComponent(table, cellYear.getYear(), isSelected, row, col);
		}
		return null;
	}

	@Override
	public Object getCellEditorValue() {
		try{
			int year = Integer.parseInt(myYearText.getText(), 10);
			XMLGregorianCalendar xmlDate = DatatypeFactory.newInstance().newXMLGregorianCalendarDate(
					year, DatatypeConstants.FIELD_UNDEFINED, DatatypeConstants.FIELD_UNDEFINED, 
					DatatypeConstants.FIELD_UNDEFINED);
			
			return xmlDate;
		}catch(DatatypeConfigurationException dte){
			logger.error("Error during obtaining date from YearCell", dte);
		}catch(NumberFormatException nfe){
			logger.error("Error during obtaining date from YearCell", nfe);
		}
		return super.getCellEditorValue();
	}
	
	@Override
	public boolean stopCellEditing() {
		boolean correct = false;
		try{
			int year = Integer.parseInt(this.myYearText.getText(), 10);
			if(year > 0){
				correct = true;
			}else{
				correct = false;
			}
		}catch(NumberFormatException nfe){
			correct = false;
		}
		
		if( ! correct){
			this.myYearText.setBorder(redBorder);
		}
		
		return correct && super.stopCellEditing();
	}
}