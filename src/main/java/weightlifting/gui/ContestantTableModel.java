package weightlifting.gui;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;
import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import weightlifting.data.CompetitionDataHandler;
import weightlifting.data.ContestantFactory;
import weightlifting.data.ContestantFilter;
import weightlifting.data.structures.AgeCategoryType;
import weightlifting.data.structures.ContestantType;
import weightlifting.data.structures.GenderType;
import weightlifting.data.structures.GroupType;
import weightlifting.data.structures.TeamMembershipType;

public class ContestantTableModel implements TableModel{

	private static Logger logger = Logger.getLogger(ContestantTableModel.class);
	
	public enum ContestantColumn {
		NAME, CLUB, YEAR_BIRTH, BODY_WEIGHT, SNATCH, CLEAN_AND_JERK, 
		GENDER, AGE_CATEGORY, TEAM_MEMBERSHIP, TEAM;
	};
	private static final String[] columnName = {
		"Jm�no", "Odd�l", "Rok narozen�", "Hmotnost", "Trh", "Nadhoz", 
		"Pohlav�", "V�k. kategorie", "Dru�stvo", "N�zev dru�stva" 
	};
	private static final Class<?>[] columnClass = {
		String.class, String.class, XMLGregorianCalendar.class, BigDecimal.class, Integer.class, Integer.class, 
		GenderType.class, AgeCategoryType.class, TeamMembershipType.class, String.class
	};
	
	private Set<TableModelListener> listeners;
	private CompetitionDataHandler data;
	private List<ContestantType> filteredContestants;
	private ContestantFilter filter = null;
	private ContestantFactory contestantFactory;
	
	public ContestantTableModel(CompetitionDataHandler data, ContestantFactory contestantFactory){
		this.listeners = new HashSet<TableModelListener>();
		this.data = data;
		this.filteredContestants = this.data.getData().getContestants().getConstestant();
		this.contestantFactory = contestantFactory;
	}
	
	@Override
	public void addTableModelListener(TableModelListener listener) {
		this.listeners.add(listener);
	}
	@Override
	public void removeTableModelListener(TableModelListener listener) {
		this.listeners.remove(listener);
	}

	@Override
	public Class<?> getColumnClass(int index) {
		return columnClass[index];
	}

	@Override
	public int getColumnCount() {
		return columnName.length;
	}

	@Override
	public String getColumnName(int index) {
		return columnName[index];
	}

	@Override
	public int getRowCount() {
		return this.filteredContestants.size() + 1;
	}

	@Override
	public Object getValueAt(int row, int col) {
		if(row >= this.filteredContestants.size()){
			return null;
		}
		
		ContestantType contestant = this.filteredContestants.get(row);
		
		Object retval = null;
		ContestantColumn column = ContestantColumn.values()[col];
		switch(column){
		case NAME:
			retval = contestant.getName();
			break;
		case CLUB:
			retval = contestant.getClub();
			break;
		case YEAR_BIRTH:
			retval = contestant.getYearBirth();
			break;
		case BODY_WEIGHT:
			retval = contestant.getBodyWeight();
			break;
		case SNATCH:
			retval = new Integer(contestant.getSnatchBase());
			break;
		case CLEAN_AND_JERK:
			retval = new Integer(contestant.getCleanAndJerkBase());
			break;
		case GENDER:
			retval = contestant.getGender();
			break;
		case AGE_CATEGORY:
			retval = contestant.getAgeCategory();
			break;
		case TEAM_MEMBERSHIP:
			retval = contestant.getTeamMembership();
			break;
		case TEAM:
			if(contestant.getTeamMembership() == TeamMembershipType.TEAM){
				retval = contestant.getTeam();
			}else{
				retval = "";
			}
			break;
		}
			
		return retval;
	}

	@Override
	public boolean isCellEditable(int row, int col) {
		return true;
	}

	@Override
	public void setValueAt(Object value, int rowIndex, int col) {
		
		int groupSize = this.filteredContestants.size();
		if(rowIndex >= groupSize){
			if(this.filteredContestants.size() > 0){
				this.contestantFactory.setMasterContestant(this.filteredContestants.get(groupSize - 1));
			}
			ContestantType newContestant = this.data.addContestant(this.contestantFactory);
			this.filteredContestants.add(newContestant);
		}
		
		ContestantColumn column = ContestantColumn.values()[col];
		boolean changed = false;
		
		ContestantType row = this.filteredContestants.get(rowIndex);
		
		if(column == ContestantColumn.AGE_CATEGORY && value instanceof AgeCategoryType){
			if(changed = row.getAgeCategory() != (AgeCategoryType)value){
				row.setAgeCategory((AgeCategoryType)value);
				this.data.getTeamCache().invalidate();
			}
		}else if(column == ContestantColumn.GENDER && value instanceof GenderType){
			if(changed = row.getGender() != (GenderType)value){
				row.setGender((GenderType)value);
				this.data.getTeamCache().invalidate();
			}
		}else if(column == ContestantColumn.TEAM_MEMBERSHIP && value instanceof TeamMembershipType){
			if(changed = row.getTeamMembership() != (TeamMembershipType)value){
				row.setTeamMembership((TeamMembershipType)value);
				this.data.getTeamCache().invalidate();
			}
		}else if(column == ContestantColumn.YEAR_BIRTH && value instanceof XMLGregorianCalendar){
			if((changed = row.getYearBirth() == null) || (changed = row.getYearBirth().getYear() != ((XMLGregorianCalendar)value).getYear())){
				row.setYearBirth((XMLGregorianCalendar)value);
				this.data.getTeamCache().invalidateScore();
			}
		}else if(column == ContestantColumn.BODY_WEIGHT && value instanceof BigDecimal){
			if((row.getBodyWeight() == null) || (changed = row.getBodyWeight().compareTo((BigDecimal)value) != 0)){
				row.setBodyWeight((BigDecimal)value);
				this.data.getTeamCache().invalidateScore();
			}
		}else if(value instanceof Integer){
			int intVal = ((Integer)value).intValue();
			if(column == ContestantColumn.SNATCH){
				if(changed = row.getSnatchBase() != intVal){
					row.setSnatchBase(intVal);
				}
			}else if(column == ContestantColumn.CLEAN_AND_JERK){
				if(changed = row.getCleanAndJerkBase() != intVal){
					row.setCleanAndJerkBase(intVal);
				}
			}
		}else if(value instanceof String){
			String previous = "";
			switch(column){
				case NAME:
					previous = row.getName();
					break;
				case CLUB:
					previous = row.getClub();
					break;
				case TEAM:
					previous = row.getTeam();
					break;
				default:
					break;
			}
			if(previous != null && previous.compareTo((String)value) == 0){
				return;
			}
			changed = true;
			switch(column){
			case NAME:
				row.setName((String)value);
				break;
			case CLUB:
				row.setClub((String)value);
				this.data.getTeamCache().invalidate();
				break;
			case TEAM:
				row.setTeam((String)value);
				this.data.getTeamCache().invalidate();
				break;
			default:
				break;
		}
		}
		
		if(changed){
			this.data.setDirty();
		}
		
	}
	
	public void refilter(){
		setContestantFilter(this.filter);
		notifyListeners();
	}
	
	public void setContestantFilter(ContestantFilter filter){
		this.filter = filter;
		this.filteredContestants = new ArrayList<ContestantType>(20);
		List<ContestantType> allContestants = this.data.getData().getContestants().getConstestant();
		for (ContestantType contestant : allContestants) {
			if(this.filter.fit(contestant)){
				this.filteredContestants.add(contestant);
			}
		}
	}

	public void removeContestants(int[] selectedRows){
		
		Set<ContestantType> toRemove = new HashSet<ContestantType>(selectedRows.length, 1);
		for (int row : selectedRows) {
			if(row >= this.filteredContestants.size()){
				continue;
			}
			ContestantType contestant = this.filteredContestants.get(row); 
			toRemove.add(contestant);
			
			logger.log(Level.INFO, "Contestant " + contestant.getName() 
					+ "(" + contestant.getClub() + ") removed from group " + contestant.getGroupId());
		}
		
		this.filteredContestants.removeAll(toRemove); //from filtered section
		this.data.getData().getContestants().getConstestant().removeAll(toRemove); //from main data
		this.data.setDirty();
		notifyListeners();
	}
	
	/**
	 * Just data operation over contestatnt and logging.
	 * */
	private void moveContestantToGroup(ContestantType contestant, GroupType group){
		logger.log(Level.INFO, "Contestant " + contestant.getName() 
				+ "(" + contestant.getClub() + ") moved from group " + contestant.getGroupId() 
				+ " to group " + group.getId());
		
		contestant.setGroupId(group.getId());
	}
	
	/**
	 * Moves contestants on given lines to given group.
	 * @param selectedRows An array of row numbers with contestants to be moved into given group.
	 * @param group target group where contestants to be moved.
	 * */
	public void moveContestantsToGroup(int[] selectedRows, GroupType group){
		
		Set<ContestantType> toRemove = new HashSet<ContestantType>(selectedRows.length, 1);
		for (int row : selectedRows) {
			if(row >= this.filteredContestants.size()){
				continue;
			}
			ContestantType contestant = this.filteredContestants.get(row); 
			toRemove.add(contestant);
			moveContestantToGroup(contestant, group);
		}
		
		this.filteredContestants.removeAll(toRemove); //from filtered section	
		notifyListeners();
	}
	
	/**
	 * Moves all contestants to given group.
	 * @param group target group where contestants to be moved.
	 * */
	public void moveAllContestantsToGroup(GroupType group){
		logger.log(Level.INFO, "Moving all contestants to group " + group.getId());
		for (ContestantType contestant : this.filteredContestants) {
			moveContestantToGroup(contestant, group);
		}
		
		this.filteredContestants.clear();	
		notifyListeners();
	}
	
	/**
	 * Shifts contestants position about on place up or down
	 * @param selectedRows rows with contestants to be moved up or down
	 * @param up whether to move them up. I false they are moved down
	 * */
	public void moveContestantsUpDown(int[] selectedRows, boolean up){

		List<ContestantType> allContestants = this.data.getData().getContestants().getConstestant();
		int limit = up ? 0 : allContestants.size() - 1;
		int direction = (up ? -1 : 1);
		
		Arrays.sort(selectedRows);
		//i position of selected contestant in current view
		
		for(int pos = up ? 0 : selectedRows.length - 1; 
			(up && pos < selectedRows.length) || ( ! up && pos >= 0); 
			pos -= direction
		){
			int i = selectedRows[pos];
			if (i >= this.filteredContestants.size()) {
				continue;
			}
			ContestantType contestant = this.filteredContestants.get(i);
			
			int j = 0; //Global position of moved contestant
			for (ContestantType contestantIt : allContestants) {
				if(contestant == contestantIt){
					//k is the closest contestant in the same group in direction according to up
					for(int k = j + direction; (up && k >= limit) || ( ! up && k <= limit); k += direction){
						if(allContestants.get(k).getGroupId().equals(contestant.getGroupId())){
							ContestantType swapee = allContestants.get(k);
							
							//swap
							allContestants.set(k, contestant);
							allContestants.set(j, swapee);
							break;
						}
					}
					
					break;
				}
				j++;
			}
		}
	}
	
	private void notifyListeners(){
		for (TableModelListener listener : this.listeners) {
			listener.tableChanged(new TableModelEvent(this));
		}
	}
}
