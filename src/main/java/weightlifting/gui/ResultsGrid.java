package weightlifting.gui;

import java.awt.BorderLayout;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableColumnModel;

import weightlifting.data.AnnouncementItem;
import weightlifting.data.CompetitionDataHandler;
import weightlifting.data.ContestantComparator;
import weightlifting.data.ContestantFilter;
import weightlifting.data.DisciplineKind;

public class ResultsGrid extends JPanel implements Observer {

	private static final long serialVersionUID = 439236341238835049L;
	//private static Logger logger = Logger.getLogger(GroupView.class);
	
	// Data
	private CompetitionDataHandler data;
	private ResultsTableModel model;
	private ResultsRowSorter sorter;
	
	// GUI
	private JScrollPane rollingArea;	
	private JTable table;
	private boolean colorize = true;
	
	private AttemptCellRenderer attemptRendeder;
	private SinclairCellRenderer sinclairRendeder;
	private AttemptCellRenderer[] announcingRenderers;
	
	private TableCellEditor deletableEditor;
	
	public ResultsGrid(CompetitionDataHandler data) {
		super(new BorderLayout());
		this.data = data;
		
		this.model = new ResultsTableModel(this.data);
		this.table = new JTable(model);
		this.rollingArea = new JScrollPane(this.table);
		
		attemptRendeder = new AttemptCellRenderer();
		sinclairRendeder = new SinclairCellRenderer(this.data);
		announcingRenderers = new AttemptCellRenderer[6];
		announcingRenderers[0] = new BaseAttemptCellRenderer(DisciplineKind.SNATCH, 1);
		announcingRenderers[1] = new BaseAttemptCellRenderer(DisciplineKind.SNATCH, 2);
		announcingRenderers[2] = new BaseAttemptCellRenderer(DisciplineKind.SNATCH, 3);
		announcingRenderers[3] = new BaseAttemptCellRenderer(DisciplineKind.CLEAN_AND_JERK, 1);
		announcingRenderers[4] = new BaseAttemptCellRenderer(DisciplineKind.CLEAN_AND_JERK, 2);
		announcingRenderers[5] = new BaseAttemptCellRenderer(DisciplineKind.CLEAN_AND_JERK, 3);
		
		deletableEditor = new SelectAllCellDecorator(new DeletableCellEditor(new JTextField()));
		
		TableColumnModel colModel = this.table.getColumnModel();
		
		colModel.getColumn(ResultsTableModel.ResultColumn.NAME.ordinal()).setCellRenderer(attemptRendeder);
		colModel.getColumn(ResultsTableModel.ResultColumn.NAME.ordinal()).setPreferredWidth(205);
		
		colModel.getColumn(ResultsTableModel.ResultColumn.CLUB.ordinal()).setCellRenderer(attemptRendeder);
		colModel.getColumn(ResultsTableModel.ResultColumn.CLUB.ordinal()).setPreferredWidth(200);
		
		colModel.getColumn(ResultsTableModel.ResultColumn.SNATCH_1.ordinal()).setCellRenderer(announcingRenderers[0]);
		colModel.getColumn(ResultsTableModel.ResultColumn.SNATCH_2.ordinal()).setCellRenderer(announcingRenderers[1]);
		colModel.getColumn(ResultsTableModel.ResultColumn.SNATCH_3.ordinal()).setCellRenderer(announcingRenderers[2]);
		colModel.getColumn(ResultsTableModel.ResultColumn.CLEAN_AND_JERK_1.ordinal()).setCellRenderer(announcingRenderers[3]);
		colModel.getColumn(ResultsTableModel.ResultColumn.CLEAN_AND_JERK_2.ordinal()).setCellRenderer(announcingRenderers[4]);
		colModel.getColumn(ResultsTableModel.ResultColumn.CLEAN_AND_JERK_3.ordinal()).setCellRenderer(announcingRenderers[5]);
		
		colModel.getColumn(ResultsTableModel.ResultColumn.SNATCH_1.ordinal()).setCellEditor(deletableEditor);
		colModel.getColumn(ResultsTableModel.ResultColumn.SNATCH_2.ordinal()).setCellEditor(deletableEditor);
		colModel.getColumn(ResultsTableModel.ResultColumn.SNATCH_3.ordinal()).setCellEditor(deletableEditor);
		colModel.getColumn(ResultsTableModel.ResultColumn.CLEAN_AND_JERK_1.ordinal()).setCellEditor(deletableEditor);
		colModel.getColumn(ResultsTableModel.ResultColumn.CLEAN_AND_JERK_2.ordinal()).setCellEditor(deletableEditor);
		colModel.getColumn(ResultsTableModel.ResultColumn.CLEAN_AND_JERK_3.ordinal()).setCellEditor(deletableEditor);
		
		colModel.getColumn(ResultsTableModel.ResultColumn.TOTAL.ordinal()).setCellRenderer(attemptRendeder);
		
		colModel.getColumn(ResultsTableModel.ResultColumn.SINCLAIR.ordinal()).setCellRenderer(sinclairRendeder);
		colModel.getColumn(ResultsTableModel.ResultColumn.SINCLAIR.ordinal()).setPreferredWidth(205);
		
		colModel.getColumn(ResultsTableModel.ResultColumn.ORDER.ordinal()).setCellRenderer(attemptRendeder);
		
		this.sorter = new ResultsRowSorter(model);
		this.sorter.setComparator(null);
		this.table.setRowSorter(this.sorter);
		this.table.getTableHeader().setReorderingAllowed(false);
				
		this.add(this.rollingArea);
	}
	
	public void setComparator(ContestantComparator comparator){
		this.sorter.setComparator(comparator);
	}
	
	public void setFilter(ContestantFilter filter){
		this.sorter.setFilter(filter);
	}
	
	public void setColorize(boolean colorize) {
		if(this.colorize != colorize){
			this.colorize = colorize;
			this.attemptRendeder.setColorize(colorize);
			for (AttemptCellRenderer item : this.announcingRenderers) {
				item.setColorize(colorize);
			}
			this.table.repaint();
		}
	}

	@Override
	public void update(Observable source, Object value) {
		if(value != null && value instanceof AnnouncementItem){
			AnnouncementItem announcement = (AnnouncementItem)value;
			this.model.announceAttempt(announcement.contestantId, announcement.discipline);
		}
	}
}
