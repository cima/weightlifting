package weightlifting.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.LinkedList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;

import weightlifting.data.Category;
import weightlifting.data.CategoryContestantComparator;
import weightlifting.data.CategoryContestantFilter;
import weightlifting.data.CompetitionDataHandler;
import weightlifting.data.ContestantComparator;
import weightlifting.data.ContestantFilter;
import weightlifting.data.GroupContestantComparator;
import weightlifting.data.GroupIdContestantFilter;
import weightlifting.data.GroupNameContestantComparator;
import weightlifting.data.structures.AgeCategoryType;
import weightlifting.data.structures.GenderType;
import weightlifting.data.structures.GroupType;
import weightlifting.data.structures.TeamMembershipType;
import weightlifting.settings.Settings;

public class ResultsView extends AutoGridPanel implements ActionListener, Observer{

	private static final long serialVersionUID = -4127005173271075347L;

	private CompetitionDataHandler data = null;
	
	private ResultsGrid resultsGrid = null;
	private JComboBox<FilterChoice> filterByCombo = null;
	private JComboBox<OrderBy> orderByCombo = null;
	private FilterChoice showAllFilter = new FilterChoice("V�e", null);
	private List<FilterChoice> categoryChoices = null;
	private JCheckBox colorizeResults;
	private Settings settings;
	
	private enum OrderBy {
		//NOTHING("Ni�eho", null),
		//GROUP("Skupiny", new GroupContestantComparator()),
		GROUP_ONLY("Skupiny", new GroupContestantComparator()),
		GROUP_NAME("Skupiny a jm�na", new GroupNameContestantComparator()),
		CATEGORY("Kategorie a jm�na", new CategoryContestantComparator());
		
		private String text;
		private ContestantComparator comparator;
		
		OrderBy(String text, ContestantComparator comparator){
			this.text = text;
			this.comparator = comparator;
		}
		public ContestantComparator getComparator(){
			return comparator;
		}
		
		public String toString(){
			return text;
		}
	}
	
	private class FilterChoice {
		
		public String text;
		public ContestantFilter filter;
		
		public FilterChoice(String text, ContestantFilter filter){
			this.text = text;
			this.filter = filter;
		}
		
		public String toString() {
			return text;
		}
	}
	
	public ResultsView(CompetitionDataHandler data, Settings settings){
		this.data = data;
		this.settings = settings;
		this.data.addObserver(this);
		
		this.labels.add(new JLabel("zobrazit:", JLabel.RIGHT));
		this.filterByCombo = new JComboBox<FilterChoice>();
		this.filterByCombo.addActionListener(this);
		this.components.add(this.filterByCombo);
		
		this.orderByCombo = new JComboBox<OrderBy>(OrderBy.values());
		this.orderByCombo.addActionListener(this);
		
		//Turn of category colouring
		this.colorizeResults = new JCheckBox("Barvit kategorie");
		this.colorizeResults.setSelected(true);
		this.colorizeResults.addActionListener(this);
			
		if(settings.isShowAdvancedOptions()){
			this.labels.add(new JLabel("�adit podle:", JLabel.RIGHT));
			this.components.add(this.orderByCombo);
			
			this.labels.add(new JLabel(" ", JLabel.RIGHT));
			this.components.add(this.colorizeResults);
		}

		this.resultsGrid = new ResultsGrid(this.data);
		this.data.addObserver(this.resultsGrid); //because of announcements repaint 
		this.resultsGrid.setComparator(getOrderComparator());
		initCategories();
		fillFilterCombo();
		this.resultsGrid.setFilter(getSelectedFilter());
		
		align(this.resultsGrid);
	}
	
	private void initCategories(){
		this.categoryChoices = new LinkedList<FilterChoice>();
		
		LinkedList<Category> categories = new LinkedList<Category>();
		categories.add(new Category(GenderType.MALE, AgeCategoryType.SCHOOL, TeamMembershipType.SINGLE));
		categories.add(new Category(GenderType.MALE, AgeCategoryType.JUNIOR, TeamMembershipType.SINGLE));
		categories.add(new Category(GenderType.MALE, AgeCategoryType.ADULT, TeamMembershipType.SINGLE));
		categories.add(new Category(GenderType.MALE, AgeCategoryType.MASTER, TeamMembershipType.SINGLE));

		categories.add(new Category(GenderType.MALE, AgeCategoryType.SCHOOL, TeamMembershipType.TEAM));
		categories.add(new Category(GenderType.MALE, AgeCategoryType.JUNIOR, TeamMembershipType.TEAM));
		categories.add(new Category(GenderType.MALE, AgeCategoryType.ADULT, TeamMembershipType.TEAM));
		categories.add(new Category(GenderType.MALE, AgeCategoryType.MASTER, TeamMembershipType.TEAM));
				
		categories.add(new Category(GenderType.FEMALE, AgeCategoryType.SCHOOL, TeamMembershipType.SINGLE));
		categories.add(new Category(GenderType.FEMALE, AgeCategoryType.JUNIOR, TeamMembershipType.SINGLE));
		categories.add(new Category(GenderType.FEMALE, AgeCategoryType.ADULT, TeamMembershipType.SINGLE));
		categories.add(new Category(GenderType.FEMALE, AgeCategoryType.MASTER, TeamMembershipType.SINGLE));

		categories.add(new Category(GenderType.FEMALE, AgeCategoryType.SCHOOL, TeamMembershipType.TEAM));
		categories.add(new Category(GenderType.FEMALE, AgeCategoryType.JUNIOR, TeamMembershipType.TEAM));
		categories.add(new Category(GenderType.FEMALE, AgeCategoryType.ADULT, TeamMembershipType.TEAM));
		categories.add(new Category(GenderType.FEMALE, AgeCategoryType.MASTER, TeamMembershipType.TEAM));
		
		categories.add(new Category(GenderType.FEMALE, AgeCategoryType.SCHOOL, TeamMembershipType.OFF_CONTEST));
		categories.add(new Category(GenderType.FEMALE, AgeCategoryType.JUNIOR, TeamMembershipType.OFF_CONTEST));
		categories.add(new Category(GenderType.FEMALE, AgeCategoryType.ADULT, TeamMembershipType.OFF_CONTEST));
		categories.add(new Category(GenderType.FEMALE, AgeCategoryType.MASTER, TeamMembershipType.OFF_CONTEST));
		
		categories.add(new Category(GenderType.MALE, AgeCategoryType.SCHOOL, TeamMembershipType.OFF_CONTEST));
		categories.add(new Category(GenderType.MALE, AgeCategoryType.JUNIOR, TeamMembershipType.OFF_CONTEST));
		categories.add(new Category(GenderType.MALE, AgeCategoryType.ADULT, TeamMembershipType.OFF_CONTEST));
		categories.add(new Category(GenderType.MALE, AgeCategoryType.MASTER, TeamMembershipType.OFF_CONTEST));
		
		for (Category category : categories) {
			this.categoryChoices.add(new FilterChoice(category.toString(), new CategoryContestantFilter(category)));
		}
	}
	
	private ContestantComparator getOrderComparator(){
		Object selectedItem = this.orderByCombo.getSelectedItem();
		if(selectedItem instanceof OrderBy){
			OrderBy selectedComparator = (OrderBy)selectedItem;
			return selectedComparator.getComparator();
		}
		return null;
	}
	
	private ContestantFilter getSelectedFilter() {
		Object selectedItem = this.filterByCombo.getSelectedItem();
		if(selectedItem instanceof FilterChoice){
			FilterChoice selectedFilter = (FilterChoice)selectedItem;
			return selectedFilter.filter;
		}
		return null;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == this.orderByCombo){
			this.resultsGrid.setComparator(getOrderComparator());
		}else if(e.getSource() == this.filterByCombo){
			this.resultsGrid.setFilter(getSelectedFilter());
		}else if(e.getSource() == this.colorizeResults){
			this.resultsGrid.setColorize(this.colorizeResults.isSelected());
		}
	}

	private void fillFilterCombo(){
		Object lastSelected = this.filterByCombo.getSelectedItem();
		FilterChoice lastSelectedChoice = null;
		if(lastSelected instanceof FilterChoice && ((FilterChoice)lastSelected).filter instanceof CategoryContestantFilter){
			lastSelectedChoice = (FilterChoice)lastSelected;
		}
		
		this.filterByCombo.removeAllItems();
		this.filterByCombo.addItem(showAllFilter);
		List<GroupType> groups = this.data.getData().getGroups().getGroup();
		int i = 1;
		for (GroupType group : groups) {
			FilterChoice choice = new FilterChoice("" + i + ". Skupinu", new GroupIdContestantFilter(group.getId()));
			this.filterByCombo.addItem(choice);
			i++;
		}
		
		if(this.settings.isShowAdvancedOptions()){		
			for (FilterChoice choice : this.categoryChoices) {
				this.filterByCombo.addItem(choice);
				if(lastSelectedChoice != null && choice.filter.equals(lastSelectedChoice.filter)){
					this.filterByCombo.setSelectedIndex(i);
				}
				i++;
			}
		}
	}
	
	@Override
	public void update(Observable source, Object value) {
		if(value != null && value instanceof GroupView){
			this.fillFilterCombo();
		}
	}

	@Override
	protected void harvestData() {
		// No Results related static/root data like referee name etc.
	}

}
