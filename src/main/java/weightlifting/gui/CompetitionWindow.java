	package weightlifting.gui;

import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Enumeration;
import java.util.Observable;
import java.util.Observer;

import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.JOptionPane;
import javax.swing.JTabbedPane;
import javax.swing.Timer;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.apache.log4j.Appender;
import org.apache.log4j.FileAppender;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import weightlifting.data.CompetitionDataHandler;
import weightlifting.data.WeightliftingDataException;
import weightlifting.data.structures.GroupType;
import weightlifting.settings.Settings;
import weightlifting.web.WebServer;

public class CompetitionWindow extends JFrame implements ActionListener, WindowListener, Observer, ChangeListener {

	private static final long serialVersionUID = -7392850600713593132L;
	private static Logger logger = Logger.getLogger(CompetitionWindow.class);

	private static String AUTOSAVE_COMMAND = "autoSave";
	
	//------ data items
	private CompetitionDataHandler competitionDataHandler;
	
	//------ gui items
	private JMenuBar menuBar;
	private FileMenu fileMenu;
	private EditMenu editMenu;
	private SettingsMenu settingsMenu;
	private HelpMenu helpMenu;
	private JFileChooser fileChooser;
	private CompetitionPanel competitionPanel = null;
	private FileFilter filter;
	private Settings settings;
	private Timer autoSaveTimer;
	
	//...... Web server
	private WebServer webServer;
	
	public CompetitionWindow(){
		super("WLCompettion");
		
		logger.trace("Programme is starting");
		
		this.setPreferredSize(new Dimension(800, 600));
		this.addWindowListener(this);
		
		//Settings
		this.settings = new Settings(new File(System.getProperty("user.home") + "/weightlifting/config.xml"));
		
		this.menuBar = new JMenuBar();
		this.fileMenu = new FileMenu();
		this.fileMenu.addActionListener(this);
		
		this.editMenu = new EditMenu();
		this.editMenu.addActionListener(this);
		
		this.settingsMenu = new SettingsMenu(this.settings);
		
		this.helpMenu = new HelpMenu();
		this.helpMenu.addActionListener(this);
		
		this.menuBar.add(this.fileMenu);
		this.menuBar.add(this.editMenu);
		this.menuBar.add(this.settingsMenu);
		this.menuBar.add(this.helpMenu);
		this.setJMenuBar(this.menuBar);
		
		setMenuDefaultEnabled();
		
		this.fileChooser = new JFileChooser();
		this.filter = new FileNameExtensionFilter("Weightlifting competition XML", "xml");
		this.fileChooser.setFileFilter(filter);
		this.fileChooser.setCurrentDirectory(new File(settings.getLastFolder()));
		
		this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		
		this.pack();
		this.setVisible(true);
		
		//WEB server
		this.webServer = new WebServer(settings);
		this.webServer.start();

		//Timer
		this.autoSaveTimer = new Timer(5 * 60 * 1000, this);
		this.autoSaveTimer.setActionCommand(AUTOSAVE_COMMAND);
		this.autoSaveTimer.start();
		
		//icon
		ImageIcon icon = new ImageIcon("web/img/weightlifting.png");
		this.setIconImage(icon.getImage());
		
	}

	@Override
	public void windowActivated(WindowEvent arg0) {}
	@Override
	public void windowClosed(WindowEvent arg0) {
		this.settings.save();
		this.webServer.destroy();
		
		try {
			this.autoSaveTimer.stop();
			this.webServer.join();
		} catch (InterruptedException e) {
			logger.error("Problem when stopping webserver: " + e.getMessage(), e);
		}
		System.exit(0);
		
	}
	@Override
	public void windowDeactivated(WindowEvent arg0) {}
	@Override
	public void windowDeiconified(WindowEvent arg0) {}
	@Override
	public void windowIconified(WindowEvent arg0) {}
	@Override
	public void windowOpened(WindowEvent arg0) {}
	
	@Override
	public void windowClosing(WindowEvent arg0) {
		this.close();
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		
		if(e.getActionCommand() == FileMenu.NEW_ACTION_COMMAND){
			this.newFile();
		
		}else if(e.getActionCommand() == FileMenu.OPEN_ACTION_COMMAND){
			this.openFile();
			
		}else if(e.getActionCommand() == FileMenu.SAVE_ACTION_COMMAND){
			this.save();
			
		}else if(e.getActionCommand() == FileMenu.SAVEAS_ACTION_COMMAND){
			this.saveAs();
			
		}else if(e.getActionCommand() == FileMenu.EXIT_ACTION_COMMAND){
			this.close();
			
		}else if(e.getActionCommand() == EditMenu.ADD_GROUP_ACTION_COMMAND){
			GroupType group = this.competitionDataHandler.addGroup();
			this.competitionPanel.addGroup(group);
			
		}else if(e.getActionCommand() == EditMenu.REMOVE_GROUP_ACTION_COMMAND){
			this.removeGroup(e);
			
		}else if(e.getActionCommand() == EditMenu.COPY_BASES_TO_ANNOUNCEMENS){
			this.competitionDataHandler.copyBasesToAnouncements();
			
		}else if(e.getActionCommand() == EditMenu.ENABLE_COEFFICIENT_EDIT){
			this.competitionPanel.setCoefficientEditEnabled(true);
			
		}else if(e.getActionCommand() == FileMenu.PRINT_GROUPS_ACTION_COMMAND){
			openUrl("http://127.0.0.1:" + settings.getWebserverPort() + WebServer.GROUPS_PATH);
			
		}else if(e.getActionCommand() == FileMenu.PRINT_TECHNICAL_ACTION_COMMAND){
			openUrl("http://127.0.0.1:" + settings.getWebserverPort() + WebServer.TECHNICAL_PATH);
			
		}else if(e.getActionCommand() == FileMenu.PRINT_EMPTY_RESULTS_ACTION_COMMAND){
			openUrl("http://127.0.0.1:" + settings.getWebserverPort() + WebServer.EMPTY_RESULTS_PATH);	
			
		}else if(e.getActionCommand() == FileMenu.PRINT_RESULTS_ACTION_COMMAND){
			openUrl("http://127.0.0.1:" + settings.getWebserverPort() + WebServer.RESULTS_PATH);
			
		}else if(e.getActionCommand() == HelpMenu.SHOW_HELP_ACTION_COMMAND){
			openFile("doc/manual-weightlifting.pdf");
		
		}else if(e.getActionCommand() == HelpMenu.SHOW_IP_ACTION_COMMAND){
			showIpAddressesDialog();	
			
		}else if(e.getActionCommand() == HelpMenu.SHOW_LOG_ACTION_COMMAND){
			showLog();	
			
		}else if(e.getActionCommand() == HelpMenu.SHOW_ABOUT_COMMAND){
			showAbout();	
			
		}else if(e.getActionCommand() == AUTOSAVE_COMMAND && settings.isAutosave()){
			autoSave();
		}
	}
	
	@Override
	public void update(Observable originator, Object arg) {
		if(originator == this.competitionDataHandler){
			this.fileMenu.setSaveEnabled(
					this.competitionDataHandler.hasData() 
					&& ! this.competitionDataHandler.isSaved()
			);
			this.fileMenu.setSaveAsEnabled(this.competitionDataHandler.hasData());
			this.fileMenu.setExportEnabled(this.competitionDataHandler.hasData());
		}
	}
	
	private void setMenuDefaultEnabled(){
		this.fileMenu.setSaveEnabled(false);
		this.fileMenu.setSaveAsEnabled(false);
		this.fileMenu.setExportEnabled(false);
		
		this.editMenu.setAddEnabled(false);
		this.editMenu.setRemoveEnabled(false);
	}
	
	private void openUrl(String uriStr){
		if( ! Desktop.isDesktopSupported() ) {
			return;
        }

        Desktop desktop = Desktop.getDesktop();
        if( ! desktop.isSupported( java.awt.Desktop.Action.BROWSE ) ) {
        	return;
        }

        try {
			URI uri = new URI(uriStr);
            desktop.browse( uri );
        }catch (URISyntaxException use) {
        	logger.error("Can't open uri " + uriStr + ". " + use.getMessage());
        }catch(Exception e){
            logger.error("Can't open uri " + uriStr + ". " + e.getMessage());
        }
	}
	
	private void openFile(String path){
		File fileToOpen = new File(path);
		if( ! Desktop.isDesktopSupported() || ! fileToOpen.exists()) {
			return;
        }
		Desktop desktop = Desktop.getDesktop();
		try{
			desktop.open(fileToOpen);
		}catch(IOException ioe){
			logger.error("Openning file in associated application failed", ioe);
		}
	}
	
	private void showIpAddressesDialog(){
		JDialog dialog = new IpAddressesDialog(this);
		dialog.setModal(true);
		dialog.setVisible(true);
		
	}
	
	private void showLog(File logFile) {
		try{
			if (Desktop.isDesktopSupported()) {
				Desktop.getDesktop().open(logFile);
				return;
			}
		}catch (IOException ioe) {
			logger.error("Logfile '" + logFile.getPath() + "' caused an error during opening. Error is '" + ioe.getMessage() + "'.");
		}
		
		JOptionPane.showMessageDialog(this, "Debug log naleznete v souboru " + logFile.getAbsolutePath(), "Debug log nelze otev��t automaticky", JOptionPane.INFORMATION_MESSAGE);
	}
	
	private void showLog(){
		@SuppressWarnings("unchecked")
		Enumeration<Appender> apps = Logger.getRootLogger().getAllAppenders();
		while ( apps.hasMoreElements() ){
			Appender app = apps.nextElement();				
			File logFile = null;
			if (app instanceof FileAppender) {
				logFile = new File(((FileAppender)app).getFile());
				showLog(logFile);
			}
		}
	}
	
	private void showAbout(){
		JOptionPane.showMessageDialog(this, "Copyright\nMArtin �imek\n2013\nCima.m@seznam.cz\n\nVerze 1.2", "O programu Weightlifting", JOptionPane.INFORMATION_MESSAGE);
	}
	
	private boolean saveBeforeDiscard(){
		if(this.competitionDataHandler == null || ! this.competitionDataHandler.isDirty()){
			return true;
		}
		
		int wantSave = JOptionPane.showConfirmDialog(this, 
				"Aktu�ln� data byla zm�n�na, ale zm�ny nejsou ulo�eny.\n" +
				"P�ejete si aktu�ln� data ulo�it?",
				"Ulo�en� projektu",
				JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE);
		
		if(wantSave == JOptionPane.YES_OPTION){
			this.save();
		}else if(wantSave == JOptionPane.CANCEL_OPTION){
			return false;
		}
		
		return true;
	}
	
	private void newFile(){
		if ( ! this.saveBeforeDiscard()){
			return;
		}
		
		logger.trace("Creating new file");
		this.competitionDataHandler = new CompetitionDataHandler();
		this.competitionDataHandler.addObserver(this);
		this.competitionDataHandler.newData();
		
		createCompetitionPanel();
	}
	
	private void openFile(){
		if ( ! this.saveBeforeDiscard()){
			return;
		}
		
		logger.trace("Opening file");
		int retval = fileChooser.showOpenDialog(this);
		if(retval != JFileChooser.APPROVE_OPTION){
			return;
		}
		
		loadFile(fileChooser.getSelectedFile());
		settings.setLastFolder(fileChooser.getSelectedFile().getParent());
	
	}
	
	public void loadFile(File file){
		try{
			this.competitionDataHandler = new CompetitionDataHandler();
			this.competitionDataHandler.addObserver(this);
			this.competitionDataHandler.load(file);
			createCompetitionPanel();
			logger.trace("File " + this.competitionDataHandler.getXmlFile().getAbsolutePath() + " successfully loaded");
			
		}catch(WeightliftingDataException we){
			we.printStackTrace();
			logger.error("Error while loading file. " + we.getMessage());
			JOptionPane.showMessageDialog(this, 
					"Otev�r�n� souboru se nezda�ilo. Soubor je po�kozen� nebo je jin�ho typu.",
					"Chyba p�i otev�r�n� souboru", JOptionPane.ERROR_MESSAGE);
		}
	}
	
	private void createCompetitionPanel(){
		if(competitionPanel != null){
			this.remove(competitionPanel);
			this.competitionPanel = null;
		}
		competitionPanel = new CompetitionPanel(this.competitionDataHandler, this.settings);
		this.editMenu.setAddEnabled(true);
		this.add(competitionPanel);
		this.competitionPanel.addTabsChangeListener(this);
		this.pack();
		
		this.webServer.setData(this.competitionDataHandler);
	}
	
	private File appendXmlExtension(File fileName){
		if( ! fileName.getAbsolutePath().endsWith(".xml")){
			return new File(fileName + ".xml");
		}
		return fileName;
	}
	
	private void autoSave(){
		if(this.competitionDataHandler != null && this.competitionDataHandler.getXmlFile() != null){
			save();
		}
	}
	
	private void save(){
		try{
			if(this.competitionDataHandler.getXmlFile() == null){
				saveAs();
			}else{
				this.competitionDataHandler.save();
			}
		}catch(WeightliftingDataException we){
			logger.error("Problem occured while saving document. " + we.getMessage());
			this.showSaveError();
		}
	}
	
	private void saveAs() {
		try{
			logger.trace("Saving file as...");

			if(this.competitionDataHandler.getXmlFile() != null){
				this.fileChooser.setSelectedFile(this.competitionDataHandler.getXmlFile());
			}
			
			int retval = this.fileChooser.showSaveDialog(this);
			if(retval != JFileChooser.APPROVE_OPTION){
				return;
			}
			
			File dest = appendXmlExtension(fileChooser.getSelectedFile());
			this.competitionDataHandler.save(dest);
			this.competitionDataHandler.setXmlFile(dest);
			settings.setLastFolder(fileChooser.getSelectedFile().getParent());
			
		}catch(WeightliftingDataException we){
			logger.error("Problem occured while saving document. " + we.getMessage());
			this.showSaveError();
		}
	}
	
	private void showSaveError() {
		JOptionPane.showMessageDialog(this,
				"B�hem ukl�d�n� dokuemntu do�lo k chyb�. Soubor nebyl ulo�en.",
				"Chyba p�i ukl�d�n� souboru", JOptionPane.ERROR_MESSAGE);
	}
	
	private void close(){
		if ( ! this.saveBeforeDiscard()){
			return;
		}
		
		this.dispose();
		logger.trace("Programme is closing.");
	}
	
	private void removeGroup(ActionEvent e){
		int result = JOptionPane.showConfirmDialog(this.competitionPanel, 
				"Chyst�te se smazat nepr�zdnou skupinu.\n" +
				"Sout��c� z t�to skupiny budou p�esunuti do jin� skupiny.\n" +
				"Opravdu chcete tuto skupinu smazat?", 
				"Smaz�n� skupiny",
				JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
		
		if(result == JOptionPane.YES_OPTION){
			GroupView gw = this.competitionPanel.getSelectedGroup();
			gw.actionPerformed(e);//notify group to allow it to move it's contestants to last group
			this.competitionPanel.RemoveGroup(gw);
			this.competitionPanel.RenameGroupTabs();
		}
	}

	public static void main(String[] args) {
		PropertyConfigurator.configure(CompetitionWindow.class.getResourceAsStream("/log4j.properties"));
		CompetitionWindow cw = new CompetitionWindow();
		if(args.length >= 1 && args[0] != null && ! args[0].isEmpty()){
			File file = new File(args[0]);
			if(file.exists()){
				cw.loadFile(file);
			}
		}
	}

	@Override
	public void stateChanged(ChangeEvent e) {
		if(e.getSource() instanceof JTabbedPane){
			this.editMenu.setRemoveEnabled(this.competitionPanel.isGroupSelected()
					&& this.competitionDataHandler.getData().getGroups().getGroup().size() > 1);
		}
	}

}
