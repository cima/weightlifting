package weightlifting.gui;

import javax.swing.DefaultCellEditor;
import javax.swing.JTextField;

/**
 * Allows user to delete the content of textfield and cancel editing 
 * which results in return of null as a content of edited cell.
 * 
 * It is up to programmer who uses this editor to use proper model for table.
 * */
public class DeletableCellEditor extends DefaultCellEditor {

	private static final long serialVersionUID = 2491145849753511733L;
	
	private JTextField textBox;
	
	public DeletableCellEditor(JTextField text) {
		super(text);
		this.textBox = text;
		this.textBox.setHorizontalAlignment(JTextField.RIGHT);
	}

	@Override
	public Object getCellEditorValue() {
		Integer value = null;
		
		if(textBox.getText().trim().length() == 0) {
			return value;
		}
		
		try{
			value = Integer.parseInt(textBox.getText());
		}catch(NumberFormatException nfe){
			return value;
		}
		
		return value;
	}
	
	@Override
	public boolean stopCellEditing() {
		if(textBox.getText().trim().length() == 0) {
			fireEditingStopped();
			return true;
		}
		
		try{
			Integer.parseInt(textBox.getText());
		}catch(NumberFormatException nfe){
			return false;
		}
		
		fireEditingStopped();
		return true;
	}
}
