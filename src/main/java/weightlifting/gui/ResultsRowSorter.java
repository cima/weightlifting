package weightlifting.gui;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import javax.swing.RowSorter;

import weightlifting.data.ContestantComparator;
import weightlifting.data.ContestantFilter;
import weightlifting.data.structures.ContestantType;

public class ResultsRowSorter extends RowSorter<ResultsTableModel> {

	private ResultsTableModel model = null;
	private ArrayList<ContestantIndexDecorator> mapping = null;
	private boolean mappingIsFresh = false;
	
	private ContestantComparator comparator = null;
	private ContestantFilter filter = null;
	
	private List<javax.swing.RowSorter.SortKey> sortKeys = null;
	
	public ResultsRowSorter(ResultsTableModel model) {
		this.model = model;
		sortKeys = new LinkedList<RowSorter.SortKey>();
		initMapping();
	}

	private void initMapping(){
		if(this.model != null){
			int count = model.getRowCount();
			this.mapping = new ArrayList<ContestantIndexDecorator>(count);
			for (int i = 0; i < count; i++) {
				ContestantType contestant = (ContestantType)this.model.getValueAt(i, -1);
				if(this.filter == null || this.filter.fit(contestant)){
					mapping.add(new ContestantIndexDecorator(i, contestant));
				}
			}
		}else{
			this.mapping = null;
		}
		
		mappingIsFresh = true;
	}
	
	public void setComparator(ContestantComparator comparator){
		this.comparator = comparator;
		sort();
	}
	
	public void setFilter(ContestantFilter filter) {
		this.filter = filter;
		initMapping();
		sort();
	}

	private void sort() {
		if(this.comparator == null){
			if( ! mappingIsFresh){
				initMapping();
				fireRowSorterChanged(null);
			}
			return;
		}
		
		Collections.sort(this.mapping, this.comparator);
		mappingIsFresh = false;
		fireRowSorterChanged(null);
	}
	
	//------- interface ---------
	@Override
	public int convertRowIndexToModel(int index) {
		return mapping.get(index).getIndex();
	}

	@Override
	public int convertRowIndexToView(int index) {
		int i = 0;
		for (ContestantIndexDecorator contestant : this.mapping) {
			if(contestant.getIndex() == index){
				return i;
			}
			i += 1;
		}
		return -1;
	}

	@Override
	public ResultsTableModel getModel() {
		return this.model;
	}

	@Override
	public int getModelRowCount() {
		return this.model.getRowCount();
	}

	@Override
	public int getViewRowCount() {
		return this.mapping.size();
	}

	//---- changes of underlying model
	
	@Override
	public void allRowsChanged() {
		this.initMapping();
		this.sort();
	}
	
	@Override
	public void modelStructureChanged() {
		this.initMapping();
		this.sort();
	}

	@Override
	public void rowsDeleted(int firstRow, int endRow) {
		for (Iterator<ContestantIndexDecorator> contestantIterator = this.mapping.iterator(); contestantIterator.hasNext();) {
			ContestantIndexDecorator contestant = (ContestantIndexDecorator) contestantIterator.next();
				
			if(contestant.getIndex() >= firstRow && contestant.getIndex() <= endRow){
				contestantIterator.remove();
			}
		}
		this.sort();
	}

	@Override
	public void rowsInserted(int firstRow, int endRow) {
		this.initMapping();
		this.sort();
	}

	@Override
	public void rowsUpdated(int firstRow, int endRow, int column) {
		this.sort();
	}

	@Override
	public void rowsUpdated(int firstRow, int endRow) {
		this.sort();
	}

	//----- sort keys
	
	@Override
	public void setSortKeys(List<? extends javax.swing.RowSorter.SortKey> keys) {
		//not supported
	}
	
	@Override
	public List<? extends javax.swing.RowSorter.SortKey> getSortKeys() {
		return this.sortKeys;
	}

	@Override
	public void toggleSortOrder(int column) {
		// not supported
	}

}
