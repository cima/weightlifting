package weightlifting.gui;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.math.BigInteger;
import java.util.LinkedList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import javax.swing.DefaultCellEditor;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.TableColumnModel;

import org.apache.log4j.Logger;

import weightlifting.data.CompetitionDataHandler;
import weightlifting.data.ContestantFactory;
import weightlifting.data.ContestantFilter;
import weightlifting.data.WeightliftingDataException;
import weightlifting.data.structures.AgeCategoryType;
import weightlifting.data.structures.GenderType;
import weightlifting.data.structures.GroupType;
import weightlifting.data.structures.TeamMembershipType;

public class ContestantView extends JPanel implements MouseListener, ActionListener, Observer, ObservableChangeSource {
	private static final long serialVersionUID = 3646527970403292786L;
	private static Logger logger = Logger.getLogger(ContestantView.class);
	
	private CompetitionDataHandler data;
	private ContestantTableModel model;
	
	private List<ObservableChange> observableChanges;
	
	//GUI
	private JScrollPane rollingArea;
	private JTable table;
	private BorderLayout layout;
	private ContestantPopupMenu popupMenu;
	private ContestantFactory contestantFactory;
	
	public ContestantView(CompetitionDataHandler data, ContestantFactory contestantFactory){
		this.data = data;
		this.model = new ContestantTableModel(this.data, contestantFactory);
		this.table = new JTable(this.model);
		this.table.addMouseListener(this);
		
		this.observableChanges = new  LinkedList<ObservableChange>();
		this.contestantFactory = contestantFactory;
		
		SelectAllCellDecorator easyEditor = new SelectAllCellDecorator(new DefaultCellEditor(new JTextField()));
		
		TableColumnModel colModel = this.table.getColumnModel();
		colModel.getColumn(ContestantTableModel.ContestantColumn.NAME.ordinal()).setPreferredWidth(170);
		colModel.getColumn(ContestantTableModel.ContestantColumn.NAME.ordinal()).setCellEditor(easyEditor);
		
		colModel.getColumn(ContestantTableModel.ContestantColumn.CLUB.ordinal()).setPreferredWidth(170);
		colModel.getColumn(ContestantTableModel.ContestantColumn.CLUB.ordinal()).setCellEditor(easyEditor);
		
		colModel.getColumn(ContestantTableModel.ContestantColumn.AGE_CATEGORY.ordinal()).setCellEditor(
				new EnumCellEditor<AgeCategoryType>(AgeCategoryType.ADULT, Translations.getAgeCategoryTranslations()));
		colModel.getColumn(ContestantTableModel.ContestantColumn.AGE_CATEGORY.ordinal()).setCellRenderer(
				new EnumCellRenderer<AgeCategoryType>(Translations.getAgeCategoryTranslations()));
		
		colModel.getColumn(ContestantTableModel.ContestantColumn.GENDER.ordinal()).setCellEditor(
				new EnumCellEditor<GenderType>(GenderType.MALE, Translations.getGenderTranslations()));
		colModel.getColumn(ContestantTableModel.ContestantColumn.GENDER.ordinal()).setCellRenderer(
				new EnumCellRenderer<GenderType>(Translations.getGenderTranslations()));
		
		colModel.getColumn(ContestantTableModel.ContestantColumn.TEAM_MEMBERSHIP.ordinal()).setPreferredWidth(120);
		colModel.getColumn(ContestantTableModel.ContestantColumn.TEAM_MEMBERSHIP.ordinal()).setCellEditor(
				new EnumCellEditor<TeamMembershipType>(TeamMembershipType.SINGLE, Translations.getTeamMembershipTranslations()));
		colModel.getColumn(ContestantTableModel.ContestantColumn.TEAM_MEMBERSHIP.ordinal()).setCellRenderer(
				new EnumCellRenderer<TeamMembershipType>(Translations.getTeamMembershipTranslations()));
		
		colModel.getColumn(ContestantTableModel.ContestantColumn.YEAR_BIRTH.ordinal()).setCellEditor(
				new SelectAllCellDecorator(new YearCellEditor(new JTextField())));
		
		colModel.getColumn(ContestantTableModel.ContestantColumn.BODY_WEIGHT.ordinal()).setCellEditor(
				new SelectAllCellDecorator(new BodyWeightCellEditor(new JTextField())));
		colModel.getColumn(ContestantTableModel.ContestantColumn.BODY_WEIGHT.ordinal()).setCellRenderer(
				new BodyWeightCellRenderer());
		
		colModel.getColumn(ContestantTableModel.ContestantColumn.SNATCH.ordinal()).setCellEditor(
				new SelectAllCellDecorator(this.table.getDefaultEditor(ContestantTableModel.ContestantColumn.SNATCH.getClass())));
		
		colModel.getColumn(ContestantTableModel.ContestantColumn.CLEAN_AND_JERK.ordinal()).setCellEditor(
				new SelectAllCellDecorator(this.table.getDefaultEditor(ContestantTableModel.ContestantColumn.CLEAN_AND_JERK.getClass())));
		
		colModel.getColumn(ContestantTableModel.ContestantColumn.TEAM.ordinal()).setPreferredWidth(150);
		colModel.getColumn(ContestantTableModel.ContestantColumn.TEAM.ordinal()).setCellEditor(easyEditor);
		
		this.table.getTableHeader().setReorderingAllowed(false);
		
		this.rollingArea = new JScrollPane(this.table);
		this.layout = new BorderLayout();
		this.setLayout(this.layout);
		
		this.add(this.rollingArea);
		
		this.popupMenu = new ContestantPopupMenu();
		this.data.addObserver(this.popupMenu);
		
		this.popupMenu.addMouseListener(this);
		this.popupMenu.addActionListener(this);
		this.popupMenu.updateMoveItems(this.data);
	}
	
	public void setContestantFilter(ContestantFilter filter){
		this.model.setContestantFilter(filter);
	}

	@Override
	public void mouseClicked(MouseEvent e) {}
	@Override
	public void mouseEntered(MouseEvent e) {}
	@Override
	public void mouseExited(MouseEvent e) {}

	@Override
	public void mousePressed(MouseEvent e) {
		showPopupMenu(e);
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		showPopupMenu(e);		
	}
	
	private void showPopupMenu(MouseEvent e){
		if(e.isPopupTrigger()){
			int[] selectedRows = null;
			if(this.table.getSelectedRowCount() == 0){
				selectedRows = new int[1];
				selectedRows[0] = this.table.rowAtPoint(e.getPoint());
			}else{
				selectedRows = this.table.getSelectedRows();
			}
			
			this.popupMenu.setSelectedRows(selectedRows);
			this.popupMenu.show(e.getComponent(), e.getX(), e.getY());
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() instanceof JMenuItem) {
			int[] selectedRows = this.popupMenu.getSelectedRows();
			if(e.getActionCommand().equals(ContestantPopupMenu.DELETE_CONTESTANT_COMMAND)){
				this.model.removeContestants(selectedRows);
				this.changeOccured();
			}else if(e.getActionCommand().equals(ContestantPopupMenu.MOVE_CONTESTANT_COMMAND)){
				GroupType group = (GroupType)((JMenuItem)e.getSource()).getClientProperty("group"); 
				this.model.moveContestantsToGroup(selectedRows, group);
				this.changeOccured();
				
			}else if(e.getActionCommand().equals(EditMenu.REMOVE_GROUP_ACTION_COMMAND)){

				if(this.data.getData().getGroups().getGroup().size() <= 1 || this.table.getRowCount() <= 0){
					return;
				}
				
				BigInteger groupId = this.contestantFactory.newContestant().getGroupId();
				if(groupId == null){
					String errorMsg = "Group ID not generated by GroupContestantFactory.";
					logger.error(errorMsg);
					throw new RuntimeException(new WeightliftingDataException(errorMsg));
				}
				
				GroupType targetGroup = this.data.getLastRemainingGroup(groupId);
				if(targetGroup != null){
					this.model.moveAllContestantsToGroup(targetGroup);
					this.data.removeGroup(groupId);
						
					logger.info("Deleting group " + groupId + " and moving all contestants to group " + targetGroup.getId());
					this.changeOccured();
				}
			}else if(e.getActionCommand().equals(ContestantPopupMenu.MOVE_UP_COMMAND)){ 
				this.model.moveContestantsUpDown(selectedRows, true);
				this.changeOccured();
				
			}else if(e.getActionCommand().equals(ContestantPopupMenu.MOVE_DOWN_COMMAND)){ 
				this.model.moveContestantsUpDown(selectedRows, false);
				this.changeOccured();
				
			}
		}
	}

	@Override
	public void update(Observable o, Object arg) {
		if(o instanceof ObservableChange) {
			this.model.refilter();
		}
	}

	private void changeOccured() {
		for (ObservableChange change : this.observableChanges) {
			change.changeOccured();
		}
	}
	
	@Override
	public void addObservableChange(ObservableChange change) {
		this.observableChanges.add(change);
		change.addObserver(this);
	}

	@Override
	public void removeObservableChange(ObservableChange change) {
		this.observableChanges.remove(change);
		change.deleteObserver(this);
	}
}
