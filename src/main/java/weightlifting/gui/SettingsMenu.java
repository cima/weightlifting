package weightlifting.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.LinkedList;
import java.util.List;

import javax.swing.JCheckBoxMenuItem;
import javax.swing.JMenu;
import javax.swing.JMenuItem;

import weightlifting.settings.Settings;

public class SettingsMenu extends JMenu implements ActionListener{

	private static final long serialVersionUID = -243344956438152498L;
	
	static String ADD_GROUP_ACTION_COMMAND = "add group";
	static String REMOVE_GROUP_ACTION_COMMAND = "remove group";
	
	private JMenuItem autosaveEnabledItem;
	
	private List<JMenuItem> items;
	private Settings settings;
	
	public SettingsMenu(Settings settings) {
		super("Nastavení");
		
		this.settings = settings;
		this.items = new LinkedList<JMenuItem>();
		
		this.autosaveEnabledItem = new JCheckBoxMenuItem("Automatické ukládání", this.settings.isAutosave());
		this.autosaveEnabledItem.addActionListener(this);
		this.items.add(autosaveEnabledItem);
		this.add(autosaveEnabledItem);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == this.autosaveEnabledItem){
			this.settings.setAutosave(this.autosaveEnabledItem.isSelected());
		}
	}

	@Override
	public void addActionListener(ActionListener listener){
		super.addActionListener(listener);
		for (JMenuItem item : items) {
			item.addActionListener(listener);
		}
	}
	
	@Override
	public void removeActionListener(ActionListener listener){
		super.removeActionListener(listener);
		for (JMenuItem item : items) {
			item.removeActionListener(listener);
		}
	}
}
