package weightlifting.gui;

import java.awt.Dimension;
import java.awt.Frame;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;
import java.util.Vector;

import javax.swing.JDialog;
import javax.swing.JScrollPane;
import javax.swing.JTable;

public class IpAddressesDialog extends JDialog {
	private static final long serialVersionUID = -5590211164842433071L;

	private JTable table;
	private JScrollPane rollingArea;
	
	public IpAddressesDialog(Frame owner) {
		super(owner, "IP adresy");
		this.setPreferredSize(new Dimension(120, 240));
		this.setResizable(false);
		
		Vector<String> header = new Vector<String>();
		header.add("IP adresa");
		this.table = new JTable(enumerateAddresses(), header);
		
		this.rollingArea = new JScrollPane(this.table);
		this.add(this.rollingArea);
		
		this.pack();
	}
	
	private Vector<Vector<String>> enumerateAddresses(){
		Vector<Vector<String>> addrs = new Vector<Vector<String>>();
		try{
			Enumeration<NetworkInterface> interfaces = (Enumeration<NetworkInterface>)NetworkInterface.getNetworkInterfaces();
			while(interfaces.hasMoreElements()) {
				NetworkInterface iface = interfaces.nextElement();
				Enumeration<InetAddress> addresses = iface.getInetAddresses();
				while(addresses.hasMoreElements()){
					InetAddress address = addresses.nextElement();
					if(address instanceof Inet4Address && ! address.isLoopbackAddress()){
						Vector<String> tmp = new Vector<String>();
						tmp.add(address.getHostAddress());
						addrs.add(tmp);
					}
				}
			}
		}catch(SocketException se){
			return null;
		}
		return addrs;
	}

}
