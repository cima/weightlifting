package weightlifting.gui;

import java.awt.Color;
import java.awt.Component;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.ParsePosition;

import javax.swing.DefaultCellEditor;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;

public class BodyWeightCellEditor extends DefaultCellEditor {

	private static final long serialVersionUID = 2481530023005486429L;
	private static final Border redBorder = new LineBorder(Color.red);
    private static final Border blackBorder = new LineBorder(Color.black);
    private DecimalFormat weightFormat = new DecimalFormat("##0.0");
	
	private JTextField bodyWeightText;
	
	public BodyWeightCellEditor(JTextField bodyWeightText){
		super(bodyWeightText);
		this.bodyWeightText = bodyWeightText;
		this.bodyWeightText.setHorizontalAlignment(JTextField.RIGHT);
		this.weightFormat.setParseBigDecimal(true);
	}
	
	@Override
	public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
		this.bodyWeightText.setBorder(blackBorder);
		
		if(value instanceof BigDecimal){
			this.bodyWeightText.setText(weightFormat.format(value));
			return this.bodyWeightText; //must be done manually because of decimal sign
		}
		return super.getTableCellEditorComponent(table, value, isSelected, row, column);
	}
	
	@Override
	public Object getCellEditorValue() {
		Number retval = null;

		ParsePosition pos = new ParsePosition(0);
		String text = this.bodyWeightText.getText();
		retval = weightFormat.parse(text, pos);
		if(pos.getIndex() != text.length()){
			retval = null;
		}

		return retval;
	}

	@Override
	public boolean stopCellEditing() {
		boolean correct = false;
		if(getCellEditorValue() != null){
			correct = true;
		}
		
		if( ! correct){
			this.bodyWeightText.setBorder(redBorder);
		}
		
		return correct && super.stopCellEditing();
	}

}
