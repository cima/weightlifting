package weightlifting.gui;

import java.util.Observable;

public class ObservableChange extends Observable {
	public void changeOccured(){
		this.setChanged();
		this.notifyObservers();
	}
	
	public void changeOccured(Object value){
		this.setChanged();
		this.notifyObservers(value);
	}
}
