package weightlifting.gui;

import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.util.LinkedList;
import java.util.List;

import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.KeyStroke;

public class EditMenu extends JMenu {
	private static final long serialVersionUID = -9002477575630956053L;
	
	static String ADD_GROUP_ACTION_COMMAND = "add group";
	static String REMOVE_GROUP_ACTION_COMMAND = "remove group";
	static String COPY_BASES_TO_ANNOUNCEMENS = "copy announcements";
	static String ENABLE_COEFFICIENT_EDIT = "Enable coefficient editing";
	
	private JMenuItem addGroupItem;
	private JMenuItem removeGroupItem;
	private JMenuItem copyAnouncementsItem;
	private JMenuItem enableCoefficientEdit;
	
	private List<JMenuItem> items;
	
	public EditMenu(){
		super("�pravy");
		this.items = new LinkedList<JMenuItem>();
		
		addGroupItem = new JMenuItem("P�idat skupinu");
		addGroupItem.setMnemonic(KeyEvent.VK_N);
		addGroupItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_G, KeyEvent.CTRL_DOWN_MASK));
		addGroupItem.setActionCommand(ADD_GROUP_ACTION_COMMAND);
		this.items.add(addGroupItem);
		this.add(addGroupItem);
		
		removeGroupItem = new JMenuItem("Odebrat skupinu");
		removeGroupItem.setMnemonic(KeyEvent.VK_R);
		removeGroupItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_DELETE, KeyEvent.SHIFT_DOWN_MASK));
		removeGroupItem.setActionCommand(REMOVE_GROUP_ACTION_COMMAND);
		this.items.add(removeGroupItem);
		this.add(removeGroupItem);
		
		copyAnouncementsItem = new JMenuItem("P�idat z�klady do ohl. pokus�");
		copyAnouncementsItem.setMnemonic(KeyEvent.VK_P);
		copyAnouncementsItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_B, KeyEvent.CTRL_DOWN_MASK));
		copyAnouncementsItem.setActionCommand(COPY_BASES_TO_ANNOUNCEMENS);
		this.items.add(copyAnouncementsItem);
		this.add(copyAnouncementsItem);
		
		enableCoefficientEdit = new JMenuItem("Zm�nit Sinclairovy koeficienty");
		enableCoefficientEdit.setMnemonic(KeyEvent.VK_P);
		enableCoefficientEdit.setActionCommand(ENABLE_COEFFICIENT_EDIT);
		this.items.add(enableCoefficientEdit);
		this.add(enableCoefficientEdit);
		
	}
	
	@Override
	public void addActionListener(ActionListener listener){
		super.addActionListener(listener);
		for (JMenuItem item : items) {
			item.addActionListener(listener);
		}
	}
	
	@Override
	public void removeActionListener(ActionListener listener){
		super.removeActionListener(listener);
		for (JMenuItem item : items) {
			item.removeActionListener(listener);
		}
	}
	
	public void setRemoveEnabled(boolean enabled){
		this.removeGroupItem.setEnabled(enabled);
	}
	public void setAddEnabled(boolean enabled){
		this.addGroupItem.setEnabled(enabled);
	}
}
