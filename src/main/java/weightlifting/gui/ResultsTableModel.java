package weightlifting.gui;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.HashSet;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import java.util.Set;

import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import weightlifting.data.AnnouncementItem;
import weightlifting.data.CompetitionDataHandler;
import weightlifting.data.DisciplineKind;
import weightlifting.data.structures.ContestantType;
import weightlifting.data.structures.ResultType;
import weightlifting.data.structures.TeamMembershipType;

public class ResultsTableModel implements TableModel, Observer {

	public enum ResultColumn {
		NAME("Jm�no", 0, String.class, false),
		CLUB("Odd�l", 0, String.class, false), 
		
		SNATCH_1("Trh", 0, Integer.class, true), 
		SNATCH_2("2.", 1, Integer.class, true), 
		SNATCH_3("3.", 2, Integer.class, true), 
		
		CLEAN_AND_JERK_1("Nadhoz", 0, Integer.class, true), 
		CLEAN_AND_JERK_2("2.", 1, Integer.class, true), 
		CLEAN_AND_JERK_3("3.", 2, Integer.class, true), 
		
		TOTAL("Celkem", 0, Integer.class, false), 
		SINCLAIR("Sinclair", 0, Double.class, false),
		ORDER("Po�ad�", 0, Integer.class, false);
		
		private String name;
		private int subIndex;
		private Class<?> colClass;
		private boolean editable;
		
		private ResultColumn(String name, int subIndex, Class<?> colClass, boolean editable) {
			this.name = name;
			this.subIndex = subIndex;
			this.colClass = colClass;
			this.editable = editable;
		}
		
		public String getName() {
			return name;
		}
		
		public int getSubIndex() {
			return subIndex;
		}
		
		public Class<?> getColumnClass() {
			return colClass;
		}
		
		public boolean isEditable() {
			return editable;
		}
	};
	
	private static Logger logger = Logger.getLogger(ResultsTableModel.class);
	
	//data
	private CompetitionDataHandler data;
	
	//other
	private Set<TableModelListener> listeners;
	
	public ResultsTableModel(CompetitionDataHandler data) {
		this.data = data;
		this.listeners = new HashSet<TableModelListener>();
		this.data.addObserver(this);
	}
	
	@Override
	public void addTableModelListener(TableModelListener listener) {
		this.listeners.add(listener);
	}
	
	@Override
	public void removeTableModelListener(TableModelListener listener) {
		this.listeners.remove(listener);
	}
	
	@Override
	public void update(Observable o, Object arg) {
		if (o instanceof CompetitionDataHandler) {
			
			if (arg != null &&  arg instanceof AnnouncementItem) {
				return; //announcement doesn't change model
			}
			
			if (arg != null && arg instanceof String && arg.equals(CompetitionDataHandler.SAVE)) {
				return; //save does not changes table model
			}
			
			this.notifyListeners(new TableModelEvent(this));
		}
	}
	
	private void notifyListeners(TableModelEvent e){
		for (TableModelListener listener : this.listeners) {
			listener.tableChanged(e);
		}
	}

	@Override
	public Class<?> getColumnClass(int col) {
		return ResultColumn.values()[col].getColumnClass();
	}

	@Override
	public int getColumnCount() {
		return ResultColumn.values().length;
	}

	@Override
	public String getColumnName(int col) {
		return ResultColumn.values()[col].getName();
	}

	@Override
	public int getRowCount() {
		return this.data.getData().getContestants().getConstestant().size();
	}
	
	@Override
	public Object getValueAt(int row, int col) {
		ContestantType contestant = this.data.getData().getContestants().getConstestant().get(row);
		
		if(col == -1){
			return contestant;
		}
		
		ResultColumn column = ResultColumn.values()[col];
		switch(column) {
		case NAME:
			return contestant.getName();
			
		case CLUB:
			return contestant.getClub();
			
		case SNATCH_1:
		case SNATCH_2:
		case SNATCH_3:
			if(contestant.getResults() != null && contestant.getResults().getSnatch() != null && 
			contestant.getResults().getSnatch().size() >= column.getSubIndex() + 1) {
				return contestant.getResults().getSnatch().get(column.getSubIndex());
			}
			break;
		case CLEAN_AND_JERK_1:
		case CLEAN_AND_JERK_2:
		case CLEAN_AND_JERK_3:
			if(contestant.getResults() != null && contestant.getResults().getCleanAndJerk() != null && 
			contestant.getResults().getCleanAndJerk().size() >= column.getSubIndex() + 1) {
				return contestant.getResults().getCleanAndJerk().get(column.getSubIndex());
			}
			break;
		case TOTAL:
			if(contestant.getResults() != null) {
				return CompetitionDataHandler.getContestantTotal(contestant);
			}
			break;
		case SINCLAIR:
			double score = this.data.getContestantSinclairScore(contestant);
			if(score >= 0){
				return score;
			}
			break;
		case ORDER:
			
			if(contestant.getTeamMembership() == TeamMembershipType.TEAM){
				return this.data.getTeamCache().getTeamOrder(contestant);
			}else{
				return this.data.getContestantOrder(contestant);
			}
		}
		return null;
	}

	@Override
	public boolean isCellEditable(int row, int col) {
		return ResultColumn.values()[col].isEditable();
	}

	private void insertAttempt(List<Integer> attempts, ResultColumn column, Integer value) {	
		for(int i = attempts.size(); i <= column.getSubIndex(); i++) {
			attempts.add(0);
		}
		attempts.set(column.getSubIndex(), value);
		
		this.data.getTeamCache().invalidateScore();
		this.notifyListeners(new TableModelEvent(this));
	}
	
	private void removeAttempt(List<Integer> attempts, ResultColumn column) {
		int pos = column.getSubIndex();
		if(attempts.size() <= pos){
			return;
		}
		attempts.remove(pos);
		
		this.data.getTeamCache().invalidateScore();
		this.notifyListeners(new TableModelEvent(this));
	}
	
	private void updateSinclairScore(ContestantType contestant){
		if(contestant == null){
			return;
		}
		
		if(contestant.getBodyWeight().doubleValue() == 0.0){
			return;
		}
		
		try{
			double score = this.data.getContestantSinclairScore(contestant);
			BigDecimal scoreBig = new BigDecimal(score);
			contestant.getResults().setSinclair(scoreBig);
		}catch(NumberFormatException nfe){
			logger.error("Invalid sinclair score. " + nfe.getMessage());
		}
	}
	
	@Override
	public void setValueAt(Object value, int row, int col) {
		if ( ! isCellEditable(row, col)) {
			return;
		}

		ContestantType contestant = this.data.getData().getContestants().getConstestant().get(row);
		if(contestant.getResults() == null) {
			contestant.setResults(CompetitionDataHandler.newResults());
		}
		
		ResultColumn column = ResultColumn.values()[col];
		switch(column) {
			case SNATCH_1:
			case SNATCH_2:
			case SNATCH_3:
			{
				List<Integer> attempts = contestant.getResults().getSnatch();
				if (value instanceof Integer) {
					insertAttempt(attempts, column, (Integer)value);
					updateSinclairScore(contestant);
					logger.log(Level.INFO, "Snatch attempt " + (column.getSubIndex() + 1) +
							" for " + contestant.getName() + " set to " + value);
				}else if(value == null){
					removeAttempt(attempts, column);
					updateSinclairScore(contestant);
					logger.log(Level.INFO, "Snatch attempt " + (column.getSubIndex() + 1) +
							" for " + contestant.getName() + " removed.");
				}
			}
				break;
			case CLEAN_AND_JERK_1:
			case CLEAN_AND_JERK_2:
			case CLEAN_AND_JERK_3:
			{
				List<Integer> attempts = contestant.getResults().getCleanAndJerk();
				if (value instanceof Integer) {
					insertAttempt(attempts, column, (Integer)value);
					updateSinclairScore(contestant);
					logger.log(Level.INFO, "Clean and Jerk attempt " + (column.getSubIndex() + 1) + 
							" for " + contestant.getName() + " set to " + value);
				}else if(value == null){
					removeAttempt(attempts, column);
					updateSinclairScore(contestant);
					logger.log(Level.INFO, "Clean and Jerk attempt " + (column.getSubIndex() + 1) +
							" for " + contestant.getName() + " removed.");
				}
			}
				break;
			default:
				logger.log(Level.ERROR, "Attempt of editing wrong collumn. Bad ResultColumn enum value.");
			return;
		}
		this.data.setDirty();
	}
	
	public void announceAttempt(BigInteger contestantId, DisciplineKind discipline) {
		int row = 0;
		List<ContestantType> contestants = this.data.getData().getContestants().getConstestant();
		ContestantType contestant = null;
		for (ContestantType contestantIt : contestants) {
			if(contestantIt.getId().equals(contestantId)) {
				contestant = contestantIt;
				break;
			}
			row++;
		}
		
		if(contestant == null){
			return;
		}
		
		int col = ResultColumn.SNATCH_1.ordinal();
		ResultType results = contestant.getResults();
		if(discipline == DisciplineKind.SNATCH){
			col += Math.min(results.getSnatch().size(), 2);
		}else{
			col += 3;
			col += Math.min(results.getCleanAndJerk().size(), 2);
		}
		
		this.notifyListeners(new TableModelEvent(this, row, row, col));
	}
}
