package weightlifting.gui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.util.Collections;
import java.util.List;

import javax.swing.JLabel;
import javax.swing.JTable;

import weightlifting.data.CompetitionDataHandler;
import weightlifting.data.DisciplineKind;
import weightlifting.data.structures.ContestantType;

public class AnouncedAttemptCellRenderer extends AttemptCellRenderer {

	private static final long serialVersionUID = 863214866550492627L;
	
	protected DisciplineKind discipline;
	private Font baseFont;
	private Color baseColor;
	protected int order;
	
	public AnouncedAttemptCellRenderer(DisciplineKind baseKind, int order){
		this.discipline = baseKind;
		this.baseFont = new Font(Font.SANS_SERIF, Font.ITALIC | Font.BOLD, 12);
		this.baseColor = ColorPalettes.BASE_ATTEMPT_COLOR;
		this.order = order;
	}

	@Override
	public Component getTableCellRendererComponent(JTable table, Object value,
			boolean isSelected, boolean hasFocus, int row, int col) {
		Component component = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, col);

		if(component instanceof JLabel){
			JLabel label = (JLabel)component;
			if(label.getText().isEmpty()){
				ContestantType contestant = (ContestantType)table.getValueAt(row, -1);
				label.setText(getAnouncementText(contestant));
				label.setFont(this.baseFont);
				label.setForeground(this.baseColor);
				label.setHorizontalAlignment(JLabel.LEFT);
			}
		}
		
		return component;
	}
	
	protected String getAnouncementText(ContestantType contestant) {
		List<Integer> results;
		List<Integer> anouncements;
		
		
		if(contestant.getAnouncements() == null){
			return "";
		}
			
		if(contestant.getResults() == null){
			contestant.setResults(CompetitionDataHandler.newResults());
		}
		
		if(this.discipline == DisciplineKind.SNATCH){
			results = contestant.getResults().getSnatch(); 
			anouncements = contestant.getAnouncements().getSnatch();
		}else{
			results = contestant.getResults().getCleanAndJerk();
			anouncements = contestant.getAnouncements().getCleanAndJerk();
		}
		
		if(this.order == results.size() + 1){ //should display announced attempt?
			int lastValid = getSafeMax(results);
			int nextAttempt = getSafeMax(anouncements);
			
			if(nextAttempt > lastValid){
				return "" + nextAttempt;
			}
		}
		
		return "";
	}
	
	private static Integer getSafeMax(List<Integer> list){
		if(list != null && list.size() > 0){
			return Collections.max(list);
		}
		return 0;
	}
}
