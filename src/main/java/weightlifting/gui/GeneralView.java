package weightlifting.gui;

import java.awt.Dimension;
import java.math.BigDecimal;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.log4j.Logger;

import com.toedter.calendar.JDateChooser;

import weightlifting.data.CompetitionDataHandler;
import weightlifting.data.structures.GeneralType;

public class GeneralView extends AutoGridPanel {
	private static final long serialVersionUID = 4919668078607823988L;

	private static Logger logger = Logger.getLogger(GeneralView.class);

	//data
	private CompetitionDataHandler data;
	
	//gui	
	private JTextField titleText;
	private JLabel titleLabel;

	private JDateChooser dateChooser;
	private JLabel dateLabel;
	private GregorianCalendar gc;
	
	private JTextField locationText;
	private JLabel locationLabel;
	
	private JTextField directorText;
	private JLabel directorLabel;
	
	private JTextField maleRecordBodyWeightText;
	private JLabel maleRecordBodyWeightLabel;
	
	private JTextField maleRecordCoeffiecientText;
	private JLabel maleRecordCoeffiecientLabel;
	
	private JTextField femaleRecordBodyWeightText;
	private JLabel femaleRecordBodyWeightLabel;
	
	private JTextField femaleRecordCoeffiecientText;
	private JLabel femaleRecordCoeffiecientLabel;
	
	public GeneralView(CompetitionDataHandler data){
		this.data = data;
		GeneralType general = this.data.getData().getGeneral();
		
		//<title>Velk� cena m�sta Plzn�</title>
		this.titleLabel = new JLabel("N�zev sout�e", JLabel.RIGHT);
		this.labels.add(this.titleLabel);
		this.titleText = new JTextField(general.getTitle());
		this.components.add(this.titleText);
		
		
		//<date>2011-11-17</date>
		this.dateLabel = new JLabel("Datum kon�n�", JLabel.RIGHT);
		this.labels.add(this.dateLabel);
		
		XMLGregorianCalendar xmlDate = general.getDate();
		Date date = xmlDate.toGregorianCalendar().getTime(); 
		this.dateChooser = new JDateChooser(date);
		this.dateChooser.setMaximumSize(new Dimension(200, 24));
		this.dateChooser.addPropertyChangeListener("date", this);
		this.components.add(this.dateChooser);
		this.gc = new GregorianCalendar();
		
		//<location>Start VD Plze�, doudlevce</location>
		this.locationLabel = new JLabel("M�sto kon�n�", JLabel.RIGHT);
		this.labels.add(this.locationLabel);
		this.locationText = new JTextField(general.getLocation());
		this.locationText.getDocument().addDocumentListener(this);
		
		this.components.add(this.locationText);
		
		//<director>Ing. Jarom�r J�lek</director>
		this.directorLabel = new JLabel("�editel sout�e", JLabel.RIGHT);
		this.labels.add(this.directorLabel);
		this.directorText = new JTextField(general.getDirector());
		this.components.add(this.directorText);
		
		//<maleRecord>
		//	<bodyWeight>173.961</bodyWeight>
		this.maleRecordBodyWeightLabel = new JLabel("V�ha rekordmana");
		this.labels.add(this.maleRecordBodyWeightLabel);
		this.maleRecordBodyWeightText = new JTextField(general.getMaleRecord().getBodyWeight().toPlainString());
		this.components.add(this.maleRecordBodyWeightText);
		
		//	<coefficient>0.784780654</coefficient>
		this.maleRecordCoeffiecientLabel = new JLabel("Mu�sk� sou�initel");
		this.labels.add(this.maleRecordCoeffiecientLabel);
		this.maleRecordCoeffiecientText = new JTextField(general.getMaleRecord().getCoefficient().toPlainString());
		this.components.add(this.maleRecordCoeffiecientText);
		
		//</maleRecord>
		//<femaleRecord>
		//	<bodyWeight>125.441</bodyWeight>
		this.femaleRecordBodyWeightLabel = new JLabel("V�ha rekordmanky");
		this.labels.add(this.femaleRecordBodyWeightLabel);
		this.femaleRecordBodyWeightText = new JTextField(general.getFemaleRecord().getBodyWeight().toPlainString());
		this.components.add(this.femaleRecordBodyWeightText);
		
		//	<coefficient>1.056683914</coefficient>
		this.femaleRecordCoeffiecientLabel = new JLabel("�ensk� sou�initel");
		this.labels.add(this.femaleRecordCoeffiecientLabel);
		this.femaleRecordCoeffiecientText = new JTextField(general.getFemaleRecord().getCoefficient().toPlainString());
		this.components.add(this.femaleRecordCoeffiecientText);
		//</femaleRecord>
		
		this.setCoefficientEditEnabled(false);
		
		align();
	}
	
	public void setCoefficientEditEnabled(boolean enabled){
		this.maleRecordBodyWeightLabel.setEnabled(enabled);
		this.maleRecordBodyWeightText.setEditable(enabled);
		this.maleRecordCoeffiecientLabel.setEnabled(enabled);
		this.maleRecordCoeffiecientText.setEditable(enabled);
		
		this.femaleRecordBodyWeightLabel.setEnabled(enabled);
		this.femaleRecordBodyWeightText.setEditable(enabled);
		this.femaleRecordCoeffiecientLabel.setEnabled(enabled);
		this.femaleRecordCoeffiecientText.setEditable(enabled);
	}

	protected void harvestData(){
		
		GeneralType general = this.data.getData().getGeneral();
		general.setTitle(this.titleText.getText());
		XMLGregorianCalendar xmlDate = general.getDate();
		Date date = this.dateChooser.getDate();
		this.gc.setTime(date);
		xmlDate.setDay(this.gc.get(GregorianCalendar.DAY_OF_MONTH));
		xmlDate.setMonth(this.gc.get(GregorianCalendar.MONTH) + 1);
		xmlDate.setYear(this.gc.get(GregorianCalendar.YEAR));
		general.setDate(xmlDate);
		general.setLocation(this.locationText.getText());
		general.setDirector(this.directorText.getText());
		
		try{
			double weight = Double.parseDouble(this.maleRecordBodyWeightText.getText());
			general.getMaleRecord().setBodyWeight(BigDecimal.valueOf(weight));
		}catch(NumberFormatException nfe){
			logger.debug("Recordman weight cannot be parsed as a number: " + nfe.getMessage());
		}
		
		try{
			double coefficient = Double.parseDouble(this.maleRecordCoeffiecientText.getText());
			general.getMaleRecord().setCoefficient(BigDecimal.valueOf(coefficient));
		}catch(NumberFormatException nfe){
			logger.debug("Recordman coefficient cannot be parsed as a number: " + nfe.getMessage());
		}
		
		try{
			double weight = Double.parseDouble(this.femaleRecordBodyWeightText.getText());
			general.getFemaleRecord().setBodyWeight(BigDecimal.valueOf(weight));
		}catch(NumberFormatException nfe){
			logger.debug("Recordwoman weight cannot be parsed as a number: " + nfe.getMessage());
		}
		
		try{
			double coefficient = Double.parseDouble(this.femaleRecordCoeffiecientText.getText());
			general.getFemaleRecord().setCoefficient(BigDecimal.valueOf(coefficient));
		}catch(NumberFormatException nfe){
			logger.debug("Recordwoman coefficient cannot be parsed as a number: " + nfe.getMessage());
		}
		
		if( ! this.data.isDirty()){
			this.data.setDirty();
		}
		
		this.data.getTeamCache().invalidateScore();
	}
}
