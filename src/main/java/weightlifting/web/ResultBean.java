package weightlifting.web;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import weightlifting.data.Category;
import weightlifting.data.CompetitionDataHandler;
import weightlifting.data.Team;
import weightlifting.data.structures.AgeCategoryType;
import weightlifting.data.structures.ContestantType;
import weightlifting.data.structures.GenderType;
import weightlifting.data.structures.ResultType;
import weightlifting.data.structures.TeamMembershipType;

public class ResultBean {
	
	private String name;
	private String club;
	private int yearBirth;
	private double bodyWeight;
	private double sinclairCoefficient;
	private ResultType results;
	private int total;
	private double sinclairScore;
	private int order;
	
	private TeamMembershipType teamMembership;
	private AgeCategoryType ageCategory;
	private GenderType gender;
	
	/** for rowspan in first cell if contestatn is in team */
	private int teamSize = 1;
	private double teamScore;
	private String team;
	private boolean restartHere = false;
	private Category category = null;

	
	private ResultBean(ContestantType contestant, CompetitionDataHandler data) {
		this.name = contestant.getName();
		this.club = contestant.getClub();
		this.yearBirth = contestant.getYearBirth().getYear();
		this.bodyWeight = contestant.getBodyWeight().doubleValue();
		this.sinclairCoefficient = data.getSinclairUtils().getSinclairCoefficient(contestant.getGender(), contestant.getBodyWeight().doubleValue());
		this.results = contestant.getResults();
		this.total = CompetitionDataHandler.getContestantTotal(contestant);
		this.sinclairScore = data.getContestantSinclairScore(contestant);
		this.teamMembership = contestant.getTeamMembership();
		this.ageCategory = contestant.getAgeCategory();
		this.gender = contestant.getGender();
		
		if(contestant.getTeamMembership() == TeamMembershipType.TEAM){
			this.order = data.getTeamCache().getTeamOrder(contestant);
			Team team = data.getTeamCache().getTeam(contestant);
			this.teamSize = team.getMembers().size();
			this.teamScore = data.getTeamCache().getContestantTeamScore(contestant);
			this.team = contestant.getTeam();
		}else{
			this.order = data.getContestantOrder(contestant);
		}
	}
	
	public static List<ResultBean> ResultsFactory(CompetitionDataHandler data) {
		List<ResultBean> results = new ArrayList<ResultBean>(data == null ? 1 : data.getData().getContestants().getConstestant().size());		
		
		if(data == null){
			return results;	
		}

		List<ContestantType> contestants = data.getData().getContestants().getConstestant();
		for (ContestantType contestant : contestants) {
			results.add(new ResultBean(contestant, data));
		}
		
		Collections.sort(results, new ResultBeanComp());
		separateCategories(results);
		
		return results;
	}
	
	private static void separateCategories(List<ResultBean> sortedResults){
		if(sortedResults == null || sortedResults.isEmpty()){
			return;
		}
		
		ResultBean last = sortedResults.get(0);
		last.category = new Category(last.gender, last.ageCategory, last.teamMembership);
		
		last.restartHere = true;
		
		for (ResultBean result : sortedResults) {
			result.category = new Category(result.gender, result.ageCategory, result.teamMembership);
			if( ! last.category.equals(result.category)){
				result.restartHere = true;
			}
			
			
			if(result.restartHere != true && 
				last.category.equals(result.category) && result.getTeamMembership() == TeamMembershipType.TEAM
			){
				
				boolean resultHasTeam = (result.getTeam() != null && ! result.getTeam().isEmpty());
				boolean lastHasTeam = (last.getTeam() != null && ! last.getTeam().isEmpty());
				boolean hasTeam = resultHasTeam && lastHasTeam;
				boolean sameTeam = hasTeam && result.getTeam().equals(last.getTeam());
				boolean sameClub = (
						(result.getClub() != null && ! result.getClub().isEmpty()) && (last.getClub() != null && ! last.getClub().isEmpty()) &&
						result.getClub().equals(last.getClub())
				);
				
				if(sameTeam){
					result.teamSize = 0;
				}else if( (lastHasTeam == resultHasTeam) && sameClub){
					result.teamSize = 0;	
				}
				
			}
			
			last = result;
		}
	}

	//--------- getters -------------
	
	public String getName() {
		return name;
	}

	public String getClub() {
		return club;
	}

	public double getSinclairCoefficient() {
		return sinclairCoefficient;
	}

	public ResultType getResults() {
		return results;
	}

	public int getTotal() {
		return total;
	}

	public double getSinclairScore() {
		return sinclairScore;
	}

	public int getOrder() {
		return order;
	}

	public int getTeamSize() {
		return teamSize;
	}

	public int getYearBirth() {
		return yearBirth;
	}

	public double getTeamScore() {
		return teamScore;
	}

	public TeamMembershipType getTeamMembership() {
		return teamMembership;
	}

	public AgeCategoryType getAgeCategory() {
		return ageCategory;
	}

	public GenderType getGender() {
		return gender;
	}

	public String getTeam() {
		return team;
	}

	public double getBodyWeight() {
		return bodyWeight;
	}

	public boolean isRestartHere() {
		return restartHere;
	}

	public Category getCategory() {
		return category;
	}
}
