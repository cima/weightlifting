package weightlifting.web;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import weightlifting.data.CompetitionDataHandler;
import weightlifting.data.structures.ContestantType;

public class ResultsServlet extends HttpServlet {

	private static final long serialVersionUID = -1844407975722406598L;

	private CompetitionDataHandler data = null;

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		if(data != null){
			request.setAttribute("General", data.getData().getGeneral());
			request.setAttribute("Groups", data.getData().getGroups().getGroup());
		
			List<ContestantType> contestants = data.getData().getContestants().getConstestant();
			List<ContestantCategoryDecorator> sortedContestants = new ArrayList<ContestantCategoryDecorator>(contestants.size());
			
			//decorate with category
			for (ContestantType contestant : contestants) {
				sortedContestants.add(new ContestantCategoryDecorator(contestant)); 
			}
			
			//Collections.sort(sortedContestants, new NameContestantComparator());
			
			request.setAttribute("Contestants", sortedContestants);
			request.setAttribute("Results", ResultBean.ResultsFactory(data));
		}
		
		ServletContext context = getServletContext();
		if(request.getServletPath().equals(WebServer.GROUPS_PATH)) {
			context.getRequestDispatcher("/jsp/GroupsMain.jsp").forward(request, response);
			
		}else if(request.getServletPath().equals(WebServer.EMPTY_RESULTS_PATH)) {
			context.getRequestDispatcher("/jsp/EmptyResultsMain.jsp").forward(request, response);
			
		}else if(request.getServletPath().equals(WebServer.RESULTS_PATH)) {
			context.getRequestDispatcher("/jsp/ResultsMain.jsp").forward(request, response);
			
		}
	}
	
	public CompetitionDataHandler getData() {
		return data;
	}
	
	public void setData(CompetitionDataHandler data) {
		this.data = data;
	}
}
