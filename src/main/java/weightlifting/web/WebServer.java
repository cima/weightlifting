package weightlifting.web;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.tomcat.SimpleInstanceManager;
import org.eclipse.jetty.annotations.ServletContainerInitializersStarter;
import org.eclipse.jetty.apache.jsp.JettyJasperInitializer;
import org.eclipse.jetty.plus.annotation.ContainerInitializer;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletHolder;
import org.eclipse.jetty.webapp.WebAppClassLoader;
import org.eclipse.jetty.webapp.WebAppContext;

import weightlifting.data.CompetitionDataHandler;
import weightlifting.settings.Settings;

public class WebServer extends Thread {
	
	private static final Logger LOG = Logger.getLogger(WebServer.class);
	
	public static final String GROUPS_PATH = "/groups";
	public static final String TECHNICAL_PATH = "/technical";
	public static final String EMPTY_RESULTS_PATH = "/emptyresults";
	public static final String RESULTS_PATH = "/results";
	public static final String ANOUNCEMENTS_PATH = "/anouncements";
	
	private Server jettyServer = null;
	private ResultsServlet resultsServlet = null;
	private TechnicalServlet technicalServlet = null;
	private AnouncementServlet anouncementServlet = null;
	
	private CompetitionDataHandler data = null;
	
	public WebServer(Settings settings){
		this.jettyServer = new Server(settings.getWebserverPort());
		this.resultsServlet = new ResultsServlet();
		this.technicalServlet = new TechnicalServlet();
		this.anouncementServlet = new AnouncementServlet();
		
		System.setProperty("org.apache.jasper.compiler.disablejsr199", "false");
		
        WebAppContext webApp = new WebAppContext();
        webApp.setContextPath("/");
        
        webApp.addServlet(new ServletHolder(this.resultsServlet), GROUPS_PATH);
        webApp.addServlet(new ServletHolder(this.technicalServlet), TECHNICAL_PATH);
        webApp.addServlet(new ServletHolder(this.resultsServlet), EMPTY_RESULTS_PATH);
        webApp.addServlet(new ServletHolder(this.resultsServlet), RESULTS_PATH);
        webApp.addServlet(new ServletHolder(this.anouncementServlet), ANOUNCEMENTS_PATH);
        		
        webApp.setWar("./web");
        
        webApp.addFilter(DisableCacheFilter.class, "/*", null);
        
        webApp.setAttribute(
                "org.eclipse.jetty.server.webapp.ContainerIncludeJarPattern",
                ".*/[^/]*servlet-api-[^/]*\\.jar$|.*/javax.servlet.jsp.jstl-.*\\.jar$|.*/[^/]*taglibs.*\\.jar$" );

        webApp.setAttribute("org.apache.tomcat.InstanceManager", new SimpleInstanceManager());
        webApp.setResourceBase("./web");
        
        try {
        	WebAppClassLoader classloader = new WebAppClassLoader(Thread.currentThread().getContextClassLoader(), webApp);
			webApp.setClassLoader(classloader);
		} catch (IOException e) {
			LOG.error("Problem with classloader overriding. JSP support might not work.", e);
		}
        
        webApp.setAttribute("org.eclipse.jetty.containerInitializers", jspInitializers());
        webApp.addBean(new ServletContainerInitializersStarter(webApp), true);
        
        jettyServer.setHandler(webApp);
	}
		
	private static List<ContainerInitializer> jspInitializers() {
		JettyJasperInitializer sci = new JettyJasperInitializer();
		ContainerInitializer initializer = new ContainerInitializer(sci, null);
		List<ContainerInitializer> initializers = new ArrayList<ContainerInitializer>();
		initializers.add(initializer);
		return initializers;
	}
	
	public synchronized void destroy() {
		try {
			this.jettyServer.stop();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void run() {
		try{
			this.jettyServer.start();
			this.jettyServer.join();
		} catch(InterruptedException ie) {
			ie.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public CompetitionDataHandler getData() {
		return data;
	}

	public void setData(CompetitionDataHandler data) {
		this.data = data;
		this.resultsServlet.setData(data);
		this.technicalServlet.setData(data);
		this.anouncementServlet.setData(data);
	}
}
