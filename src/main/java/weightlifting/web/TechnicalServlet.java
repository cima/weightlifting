package weightlifting.web;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import weightlifting.data.CompetitionDataHandler;
import weightlifting.data.structures.ContestantType;
import weightlifting.data.structures.GroupType;
import weightlifting.web.technical.TechnicalSheet;

public class TechnicalServlet extends HttpServlet {
	private static final long serialVersionUID = -8347422045134541548L;
	
	private CompetitionDataHandler data = null;
	
	
	
	private void prepareBeans(HttpServletRequest request, HttpServletResponse response){
		if(data != null){
			request.setAttribute("General", data.getData().getGeneral());
			
			List<ContestantType> sortedContestants = new ArrayList<ContestantType>(data.getData().getContestants().getConstestant());
			request.setAttribute("Contestants", sortedContestants);
			
			List<TechnicalSheet> sheets = new LinkedList<TechnicalSheet>();
			List<GroupType> groups = data.getData().getGroups().getGroup();
			for (GroupType group : groups) {
				TechnicalSheet sheet = new TechnicalSheet(group, sortedContestants);
				if(sheet.contestantsCount > 0) {
					sheets.add(sheet);
				}
			}
			request.setAttribute("TechnicalSheets", sheets);
		}
	}
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {	
		prepareBeans(request, response);
		
		ServletContext context = getServletContext();
        context.getRequestDispatcher("/jsp/Technical.jsp").forward(request, response);
	}
	
	public CompetitionDataHandler getData() {
		return data;
	}
	
	public void setData(CompetitionDataHandler data) {
		this.data = data;
	}
}
