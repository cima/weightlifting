package weightlifting.web;

import weightlifting.data.Category;
import weightlifting.data.ContestantTypeDecorator;
import weightlifting.data.structures.ContestantType;

public class ContestantCategoryDecorator extends ContestantTypeDecorator {

	public ContestantCategoryDecorator(ContestantType contestant) {
		super(contestant);
	}
	
	public Category getCategory(){
		return new Category(this);
	}
	
}
