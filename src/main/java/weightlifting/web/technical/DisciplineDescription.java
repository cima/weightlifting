package weightlifting.web.technical;

import weightlifting.data.DisciplineKind;

public class DisciplineDescription {
	
	public DisciplineKind kind;
	public int base;
	public int maxBase;
	
	public DisciplineDescription(DisciplineKind kind, int base, int maxBase) {
		this.kind = kind;
		this.base = base;
		this.maxBase = maxBase;
	}
	
	public DisciplineKind getKind() { return kind; }
	public int getBase() { return base; }
	public int getMaxBase() { return maxBase; }
}