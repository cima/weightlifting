package weightlifting.web.technical;

import java.util.List;

import weightlifting.data.DisciplineKind;
import weightlifting.data.structures.ContestantType;
import weightlifting.data.structures.GroupType;

public class TechnicalSheet {
	public GroupType group;
	public DisciplineDescription[] disciplines;
	public int contestantsCount = 0;
	
	public GroupType getGroup() { return group; }
	public DisciplineDescription[] getDisciplines() { return disciplines; }
	
	public TechnicalSheet(GroupType group, List<ContestantType> contestants) {
		this.group = group;
		
		this.disciplines = new DisciplineDescription[2];
		this.disciplines[0] = new DisciplineDescription(DisciplineKind.SNATCH, Integer.MAX_VALUE, 0);
		this.disciplines[1] = new DisciplineDescription(DisciplineKind.CLEAN_AND_JERK, Integer.MAX_VALUE, 0);
		
		//Find minimums & maximums
		int groupId = group.getId().intValue();
		for (ContestantType contestant : contestants) {
			if(contestant.getGroupId().intValue() == groupId){
				this.contestantsCount++;
				int base = contestant.getSnatchBase();
				if(base > 0){
					this.disciplines[0].base = this.disciplines[0].base == 0 ? base : Math.min(this.disciplines[0].base, base);
					this.disciplines[0].maxBase = Math.max(this.disciplines[0].maxBase, base);
				}

				base = contestant.getCleanAndJerkBase();
				if(base > 0){
					this.disciplines[1].base = this.disciplines[1].base == 0 ? base : Math.min(this.disciplines[1].base, base);
					this.disciplines[1].maxBase = Math.max(this.disciplines[1].maxBase, base);
				}
			}
		}
		
		//If no minimum is found, use 1 (lifting nothing makes no sense)
		for(int discId = 0; discId < 2; discId++) {
			if(this.disciplines[discId].base == Integer.MAX_VALUE) {
				this.disciplines[discId].base = 1;
			}
		}
	}
}
