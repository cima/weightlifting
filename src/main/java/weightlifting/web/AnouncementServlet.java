package weightlifting.web;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import weightlifting.data.CompetitionDataHandler;
import weightlifting.data.DisciplineKind;
import weightlifting.data.WeightliftingDataException;
import weightlifting.data.structures.ContestantType;
import weightlifting.data.structures.GroupType;
import weightlifting.web.technical.TechnicalSheet;

public class AnouncementServlet extends HttpServlet {
	private static final long serialVersionUID = 610701067281513480L;
	private static Logger logger = Logger.getLogger(AnouncementServlet.class);
	
	private enum Command{
		ADD, REMOVE, UPDATE
	}

	private CompetitionDataHandler data = null;

	/**
	 * Not gonna add some string library because of on usage of this.
	 */
	private static String listToString(List<?> list, String separator){
		boolean first = true;
		StringBuilder buffer = new StringBuilder();
		for (Object item : list) {
			if( ! first) {
				buffer.append(separator);
			}
			first = false;
			buffer.append(item);
		}
		
		return buffer.toString();
	}
	
	private String getGroupSignature(BigInteger id){
		StringBuilder buffer = new StringBuilder();
		List<ContestantType> contestants = this.data.getData().getContestants().getConstestant();
		for (ContestantType contestant : contestants) {
			if(contestant.getGroupId().equals(id)){
				buffer.append(contestant.getId());
			}
		}
		return buffer.toString();
	}
	
	private void prepareBeans(HttpServletRequest request, HttpServletResponse response) {
		if(data == null){
			return;
		}
		
		List<GroupType> groups = this.data.getData().getGroups().getGroup();
		request.setAttribute("groups", groups);
		List<ContestantType> contestants = this.data.getData().getContestants().getConstestant();
		request.setAttribute("contestants", contestants);
		
		BigInteger groupId = groups.get(0).getId();
		try {
			String groupIdStr = (String)request.getParameter("groupId");
			groupId = BigInteger.valueOf(Long.parseLong(groupIdStr));
		}catch(NumberFormatException nfe){
			return;
		}		
		
		GroupType group = null;
		for (GroupType groupItem : groups) {
			if(groupItem.getId().compareTo(groupId) == 0){
				group = groupItem;
				break;
			}
		}
		
		TechnicalSheet sheet = new TechnicalSheet(group, contestants);
		request.setAttribute("sheet", sheet);
		
		String groupSignature = getGroupSignature(group.getId());
		request.setAttribute("groupSignature", groupSignature);
		
		DisciplineKind kind = DisciplineKind.SNATCH;
		try{	//because this comes from browser
			kind = DisciplineKind.valueOf((String)request.getParameter("discipline").toString());
		}catch (IllegalArgumentException iae){
			logger.error("Invalid discipline kind sent (" + (String)request.getParameter("discipline").toString() + ")", iae);
			kind = DisciplineKind.SNATCH;
		}
		request.setAttribute("disciplineKind", kind);

	}
	
	private boolean processAddRemove(HttpServletRequest request, HttpServletResponse response, Command command) throws IOException {

		PrintWriter out = response.getWriter();	
		response.setContentType("text/plain");
		
		String weightStr = request.getParameter("weight");
		String contestantIdStr = request.getParameter("id");
		String disciplineStr = request.getParameter("discipline");
		
		if(weightStr == null || contestantIdStr == null || disciplineStr == null){
			response.sendError(400, "missing weight, ID or discipline");
			return false;
		}
		
		try{
			BigInteger contestantId = new BigInteger(contestantIdStr, 10);
			DisciplineKind discipline = DisciplineKind.valueOf(disciplineStr);
			Integer weight = Integer.valueOf(weightStr);
			
			if(command == Command.ADD){
				this.data.addAnnouncement(contestantId, discipline, weight);
			}else{
				this.data.removeAnnouncement(contestantId, discipline, weight);
			}
			
			out.println("OK " + command.toString());
			return true;
		
		}catch (WeightliftingDataException wde){
			response.sendError(400, wde.getMessage());
			return false;
		}catch (NumberFormatException nfe){
			response.sendError(400, "wrong number format: " + nfe.getMessage());
			return false;
		}catch (IllegalArgumentException iae){
			response.sendError(400, "wrong enum constant: " + iae.getMessage());
			return false;
		}
	}
	
	private void processUpdate(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();	
		response.setContentType("text/plain");

		BigInteger groupId;
		DisciplineKind discipline;
		
		try {
			String groupIdStr = (String)request.getParameter("groupId");
			groupId = BigInteger.valueOf(Long.parseLong(groupIdStr));
			
			String disciplineStr = request.getParameter("discipline");
			discipline = DisciplineKind.valueOf(disciplineStr);
			
		}catch(NumberFormatException nfe){
			response.sendError(400, "Nuber format problem: " + nfe.getMessage());
			return;
			
		}catch (IllegalArgumentException iae){
			response.sendError(400, "wrong enum constant: " + iae.getMessage());
			return;
		}
		
		String groupSignature = getGroupSignature(groupId);
		out.println(groupSignature);
		
		List<ContestantType> contestants = this.data.getData().getContestants().getConstestant();
		for (ContestantType contestant : contestants) {
			if(contestant.getGroupId().equals(groupId)){
				try{
					List<Integer> attempts = CompetitionDataHandler.getAnnouncedAttempts(contestant, discipline);
					List<Integer> results = CompetitionDataHandler.getResults(contestant, discipline);
					out.println(contestant.getId() + ":" + listToString(attempts, ",") + ":" + listToString(results, ","));
				}catch(WeightliftingDataException wde){
					continue;
				}
			}
		}
		
	}
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		if(this.data == null){
			response.sendError(404, "Competition data not available");
			return;
		}
		
		String commandStr = request.getParameter("command");
		if(commandStr != null){
			try{
				Command command = Command.valueOf(commandStr);
				switch(command){
				case ADD:
				case REMOVE:
					processAddRemove(request, response, command);
					break;
				case UPDATE:
					processUpdate(request, response);
					break;
				}
			}catch (IllegalArgumentException iae){
				response.sendError(400, "unknown command value");
				return;
			}		
		}else{
			
			if( ! this.data.isBasesAnouncementsSynced()){
				this.data.copyBasesToAnouncements();
				this.data.setBasesAnouncementsSynced(true);
			}
			
			prepareBeans(request, response);
			
			ServletContext context = getServletContext();
			context.getRequestDispatcher("/jsp/Anouncements.jsp").forward(request, response);
		}
	}
	
	public CompetitionDataHandler getData() {
		return data;
	}
	
	public void setData(CompetitionDataHandler data) {
		this.data = data;
	}
}
