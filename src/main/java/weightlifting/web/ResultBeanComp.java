package weightlifting.web;

import java.util.Comparator;

import weightlifting.data.structures.GenderType;
import weightlifting.data.structures.TeamMembershipType;

/**
 * Sorts contestants according to this criteria:
 * 
 * criteria #1:
 * 		Single
 * 		team
 * 		off_contest
 * 
 * criteria #2:
 * 		female
 * 		male
 * 
 * criteria #3:
 * 		school
 * 		junior
 * 		adult
 * 		master
 * 
 * criteria #4:
 * 		club | team -- only for teams
 * 
 * criteria #5:
 * 		sinclair
 * 
 * criteria #6:
 *		name
 * */
public class ResultBeanComp implements Comparator<ResultBean> {

	@Override
	public int compare(ResultBean first, ResultBean second) {

		/* SINGLE < TEAM < OFF_CONTEST */
		if(first.getTeamMembership() != second.getTeamMembership()){
			return first.getTeamMembership().ordinal() - second.getTeamMembership().ordinal();
		}

		/* FEMALE < MALE */
		if(first.getGender() != second.getGender()){
			return (first.getGender() == GenderType.FEMALE) ? -1 : 1;
		}
		
		/* SCHOOL < JUNIOR < ADULT < MASTER */
		if(first.getAgeCategory() != second.getAgeCategory()){
			return first.getAgeCategory().ordinal() - second.getAgeCategory().ordinal();
		}
		
		/* Club | Team */
		if(first.getTeamMembership() == TeamMembershipType.TEAM &&
			second.getTeamMembership() == TeamMembershipType.TEAM){
		
			//team order
			if(first.getOrder() != second.getOrder()){
				return first.getOrder() - second.getOrder();
			}
			
			String firstCmp = first.getClub();
			if(first.getTeam() != null){
				firstCmp += first.getTeam(); 
			}
			
			String secondCmp = second.getClub();
			if(second.getTeam() != null){
				secondCmp += second.getTeam(); 
			}
			
			int result = firstCmp.compareTo(secondCmp);
			if(result != 0){
				return result;
			}
		}
		
		/* sinclair */
		if(first.getSinclairScore() != second.getSinclairScore()){
			return (second.getSinclairScore() - first.getSinclairScore()) < 0 ? -1 : 1;
		}
		
		/* name */
		return first.getName().compareTo(second.getName());
	}

}
