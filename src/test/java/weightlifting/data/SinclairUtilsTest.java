package weightlifting.data;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import weightlifting.data.structures.GenderType;

public class SinclairUtilsTest {

	private SinclairUtils sinclairUtils;
	
	/** 
	 * ALBERTA WEIGHTLIFTING ASSOCIATION
	 * AFFILIATED WITH THE C.W.F.H.C. AND I.W.F.
	 * Male Sinclair Coefficients
	 * 
	 * http://www.albertaweightlifting.com/Assets/2009-2012.Male.pdf
	 * */
	private static double[][] refsMen = {
		{32.0, 2.656480}, {38.8, 2.153896}, {42.4, 1.972327}, {46.9, 1.796101},
		{52.4, 1.633504}, {54.0, 1.594300}, {56.0, 1.549426}, {58.8, 1.493316},
		{62.0, 1.437312}, {65.7, 1.381480}, {69.0, 1.338361}, {73.1, 1.292007},
		{77.0, 1.254076}, {80.5, 1.224310}, {85.0, 1.191025}, {88.2, 1.170275},
		{94.0, 1.137842}, {99.1, 1.113955}, {100.3, 1.108879}, {105.0, 1.090760},
		{110.0, 1.074228}, {113.3, 1.064670}, {117.0, 1.055088}, {122.7, 1.042409},
		{127.7, 1.033110}, {131.0, 1.027798}, {136.2, 1.020619}, {138.7, 1.017642},
		{143.3, 1.012895}, {146.7, 1.009950}, {152.8, 1.005750}, {157.6, 1.003331},
		{159.0, 1.002760}, {163.9, 1.001210}, {169.2, 1.000262}, {173.9, 1.000000}
	};
	
	/**
	 * ALBERTA WEIGHTLIFTING ASSOCIATION
	 * AFFILIATED WITH THE C.W.F.H.C. AND I.W.F.
	 * Female Sinclair Coefficients
	 * 
	 * http://www.albertaweightlifting.com/Assets/2009-2012.Female.pdf 
	 * */
	private static double[][] refsWomen = {
		{28.0, 2.806797}, {34.2, 2.170800}, {39.0, 1.870774}, {43.9, 1.658435},
		{45.8, 1.593398}, {48.0, 1.527286}, {49.9, 1.476917}, {53.0, 1.405834},
		{55.4, 1.358676}, {58.0, 1.313997}, {60.9, 1.270770}, {63.0, 1.243172},
		{65.9, 1.209429}, {69.0, 1.178167}, {72.6, 1.147110}, {75.0, 1.129084},
		{80.8, 1.092849}, {85.8, 1.068444}, {88.9, 1.055915}, {91.4, 1.047071},
		{97.0, 1.030805}, {102.2, 1.019455}, {104.8, 1.014943}, {107.8, 1.010597},
		{110.7, 1.007198}, {112.2, 1.005727}, {115.1, 1.003403}, {117.6, 1.001914},
		{121.1, 1.000569}, {125.4, 1.000000}
	};
	
	/** Taken from http://www.mastersweightlifting.org/forms/malone.htm */
	private static double[][] refsMaloneMeltzer = {
		{30, 1}, {31, 1.014}, {32, 1.028}, {33, 1.043},
		{34, 1.058}, {35, 1.072}, {36, 1.087}, {37, 1.1},
		{38, 1.113}, {39, 1.125}, {40, 1.136}, {41, 1.147},
		{42, 1.158}, {43, 1.17}, {44, 1.183}, {45, 1.195},
		{46, 1.207}, {47, 1.217}, {48, 1.226}, {49, 1.234},
		{50, 1.243}, {51, 1.255}, {52, 1.271}, {53, 1.293},
		{54, 1.319}, {55, 1.35}, {56, 1.384}, {57, 1.417},
		{58, 1.449}, {59, 1.48}, {60, 1.509}, {61, 1.536},
		{62, 1.561}, {63, 1.584}, {64, 1.608}, {65, 1.636},
		{66, 1.671}, {67, 1.719}, {68, 1.782}, {69, 1.856},
		{70, 1.933}, {71, 2.002}, {72, 2.053}, {73, 2.087},
		{74, 2.113}, {75, 2.142}, {76, 2.184}, {77, 2.251},
		{78, 2.358}, {79, 2.5}, {80, 2.669}, {81, 2.849},
		{82, 3.018}, {83, 3.166}, {84, 3.288}, {85, 3.386},
		{86, 3.458}, {87, 3.508}, {88, 3.54}, {89, 3.559},
		{90, 3.571}
	};
	
	private static int referenceYear = 2012;
	
	/**
	 * 2. kolo 3. ligy - 26. kv�tna 2012
	 * z�vod dru�stev - VD Start Plze� Doudlevce
	 * Random selcet
	 * */
	private static double[][] refsScoreMen = {
		{67.5, 1988, 162, 219.8753},
		{88.0, 1965, 179, 209.6998},
		{104.0, 1988, 179, 195.8962},
		{91.5, 1992, 193, 222.1564}, //junuior +20
		{104.3, 1963, 178, 194.6057},
		{94.0, 1977, 230, 261.7037},
		{64.7, 1994, 169, 235.8780} //junuior +20
	};
	
	/**
	 * druzstva_zen_2012-03-18.pdf 
	 * z�vod dru�stev �en a juniorek - VD Start Plze� Doudlevce
	 * Random selcet
	 * */
	private static double[][] refsScoreWomen = {
		{67.8, 1984, 201, 239.1357},
		{67.0, 1993, 101, 120.9787},
		{66.9, 1984, 141, 169.0365},
		{47.9, 1990, 121, 185.1421},
		{48.8, 1976, 111, 167.0970},
		{77.4, 1991, 111, 123.5347},
		{69.5, 1993, 113, 132.6098},
		{77.4, 1992, 136, 151.3579},
		{98.4, 1980, 178, 182.8811}
	};
	
	/**
	 * Velk� cena m�sta Plzn� 2011
	 * */
	private static double[][] refsScoreMenMasters = {
		{68.1, 1937, 135, 384.9693628288}, {65.9, 1948, 105, 229.3054401828},
		{61.3, 1950, 112, 249.2569014132}, {39.4, 2000, 47, 99.6646983299},
		{96.8, 1994, 105, 118.045739254}, {79.2, 1960, 118, 182.8816700226},
		{69.3, 1963, 50, 81.8179906752}, {115.5, 1973, 260, 306.405933718},
		{93.7, 1964, 155, 214.9257177841}, {69.2, 1977, 165, 220.4276370341},
		{96.2, 1972, 225, 285.2861856394}, {106.9, 1988, 189, 204.9076823915},
		{66, 1988, 163, 224.5029749044}, {104, 1951, 166, 274.1386831115},
		{81, 1971, 173, 239.8329435736}, {82.3, 1994, 183, 221.4981507775},
		{83, 1957, 175, 278.1863845192}, {100.9, 1984, 205, 226.8145727983},
		{72.7, 1992, 160, 207.3946650386}, {82.8, 1988, 197, 237.7106382961},
		{101.8, 1988, 220, 242.6160344483}, {103.4, 1974, 198, 238.8454578145},
		{89.1, 1964, 170, 240.9911908139}, {87.8, 1957, 190, 293.9024772794},
		{103.9, 1979, 225, 246.3214067384}, {110, 1996, 195, 209.4745213419},
		{74.4, 1994, 210, 268.5379828384}, {125.5, 1965, 253, 316.6717940977},
		{95.4, 1976, 210, 254.5860524715}, {94.5, 1991, 205, 232.7412902856},
		{89.3, 1965, 180, 252.812232122}, {102.2, 1992, 188, 207.0307675329},
		{132.5, 1991, 187, 191.7841218969}, {111.5, 1977, 240, 256.7421579539},
		{74, 1990, 187, 239.8765801089}, {80.5, 1996, 129, 157.935930615},
		{92.9, 1975, 155, 192.6676580412}, {90, 1994, 160, 185.526489794},
		{145, 1972, 191, 217.3172064438}, {87.8, 1988, 286, 335.406043262},
		{87.4, 1964, 255, 364.7228694318}, {104.5, 1963, 185, 247.8039194675},
		{83.5, 1983, 240, 288.3725362993}, {71.2, 1945, 155, 339.9656638354},
		{99.5, 1977, 130, 144.5913797501}, {84.5, 1985, 270, 322.5080927679},
		{72.2, 1963, 200, 319.1447164606}, {105, 1975, 345, 409.0513417264}
	};
	
	/**
	 * 2. kolo 3. ligy - 26. kv�tna 2012
	 * z�vod dru�stev - VD Start Plze� Doudlevce
	 * */
	private static double[][] refsJuniorBonus = {
		{67.5, 1988, 162, 219.8753364548}, {88, 1965, 179, 209.6998283523}, 
		{104, 1988, 179, 195.8962062043}, {91.5, 1992, 193, 242.1564407593}, 
		{104.3, 1963, 178, 194.6056964058}, {78.5, 1989, 200, 248.1727313776}, 
		{69, 1965, 165, 220.8295284516}, {85.5, 1983, 195, 231.5885411791}, 
		{64.3, 1997, 91, 157.5450769995}, {90, 1967, 180, 208.7173010183}, 
		{57.3, 1996, 126, 221.8310443051}, {86.5, 1969, 200, 236.2040406072}, 
		{81.4, 1991, 192, 233.7085027609}, {86, 1976, 180, 213.1738201919}, 
		{94, 1977, 230, 261.7037487154}, {66.5, 1989, 196, 268.6170711178}, 
		{78, 1993, 181, 245.3792385763}, {80.5, 1965, 172, 210.5812408201}, 
		{82.3, 1983, 233, 282.0167712085}, {69, 1977, 95, 127.144273957}, 
		{64.7, 1994, 169, 255.8780279725}, {100, 1941, 0, 0},
		{99.2, 1972, 223, 248.3159829785}, {97.5, 1994, 133, 169.0973670692} 
	};
	
	public static boolean isInTolerance(double ref, double result) {
		return Math.abs(ref - result) < 0.000001;
	}
	
	public static boolean fourDecimals(double ref, double result) {
		return Math.abs(ref - result) < 0.0001;
	}
	
	public static final double MALE_RECORD_WEIGHT_2012 = 173.961;
	public static final double MALE_RECORD_COEFFICIENT_2012 = 0.784780654;
	
	public static final double FEMALE_RECORD_WEIGHT_2012 = 125.441;
	public static final double FEMALE_RECORD_COEFFICIENT_2012 = 1.056683914;
	
	@Before
	public void setUp() throws Exception {
		this.sinclairUtils = new SinclairUtils(
				MALE_RECORD_WEIGHT_2012, MALE_RECORD_COEFFICIENT_2012,
				FEMALE_RECORD_WEIGHT_2012, FEMALE_RECORD_COEFFICIENT_2012);
		
	}

	@Test
	public void testGetSinclairCoefficient() {
		for (double[] ref : refsMen) {
			assertTrue(isInTolerance(ref[1], this.sinclairUtils.getSinclairCoefficient(GenderType.MALE, ref[0])));
		}
		
		for (double[] ref : refsWomen) {
			assertTrue(isInTolerance(ref[1], this.sinclairUtils.getSinclairCoefficient(GenderType.FEMALE, ref[0])));
		}
	}

	@Test
	public void testGetMaloneMeltzerCoefficient() {
		for (double[] ref : refsMaloneMeltzer) {
			if (ref[0] < 35.0) {
				continue;
			}
			assertTrue(ref[1] == this.sinclairUtils.getMaloneMeltzerCoefficient(GenderType.MALE, (int)ref[0]));
		}
		
		for (double[] ref : refsMaloneMeltzer) {
			assertTrue(ref[1] == this.sinclairUtils.getMaloneMeltzerCoefficient(GenderType.FEMALE, (int)ref[0]));
		}
	}

	@Test
	public void testGetSinclairScore() {
		for (double[] ref : refsScoreMen) {
			assertTrue(fourDecimals(ref[3], this.sinclairUtils.getSinclairScore(GenderType.MALE, referenceYear - (int)ref[1], false, ref[0], (int)ref[2])));
		}

		for (double[] ref : refsScoreWomen) {
			assertTrue(fourDecimals(ref[3], this.sinclairUtils.getSinclairScore(GenderType.FEMALE, referenceYear - (int)ref[1], false, ref[0], (int)ref[2])));
		}
	}
	
	@Test
	public void testGetJuniorBonus() {
		for (double[] ref : refsJuniorBonus) {
			int age = referenceYear - (int)ref[1];
			assertTrue(fourDecimals(ref[3], this.sinclairUtils.getSinclairScore(GenderType.MALE, age, false, ref[0], (int)ref[2]) + this.sinclairUtils.getJuniorBonus(age)));
		}
	}
	
	@Test
	public void testMastersSinclairs() {
		int referenceYear = 2011;
		for (double[] ref : refsScoreMenMasters) {
			System.out.println(this.sinclairUtils.getSinclairScore(GenderType.MALE, referenceYear - (int)ref[1], true, ref[0], (int)ref[2]));
			assertTrue(fourDecimals(ref[3], this.sinclairUtils.getSinclairScore(GenderType.MALE, referenceYear - (int)ref[1], true, ref[0], (int)ref[2])));
		}
	}
	
	

}
