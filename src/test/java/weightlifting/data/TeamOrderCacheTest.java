package weightlifting.data;

import java.math.BigInteger;
import java.net.URL;

import org.junit.Before;
import org.junit.Test;

import weightlifting.data.structures.ContestantType;
import weightlifting.data.structures.TeamMembershipType;

import static org.junit.Assert.*;

public class TeamOrderCacheTest {

	private static final int[][] expectedOrders = {
		{ 0, 2}, { 1, 2}, { 2, 1}, { 3, 1}, { 4, 2}, { 5, 5}, { 6, 4}, { 7, 3}, { 8, 3}, { 9, 2}, //Singles
		
		//teams
		{10, 1}, {11, 1}, {12, 1}, {13, 1},
		{14, 2}, {15, 2}, {16, 2}, {17, 2},
		{18, 3}, {19, 3}, {20, 3}, {21, 3},
		{22, 4}, {23, 4}, {24, 4}, {25, 4},
		{26, 5}, {27, 5}, {28, 5}, {29, 5},
		
		//Single ladies
		{30, 1}, {31, 2}, {32, 2}, {33, 3}, {34, 3}, {35, 3}, {36, 4}, {37, 5}, {38, 6}, {39, 7},
		
		//team continuation
		{40, 6}, {41, 6}, {42, 6}, {43, 6},
		
		//single contestant for collisions
		{44, 1},
		
		//team girls
		{45, 1}, {46, 1}, {47, 1}, {48, 1}, {49, 1}, {50, 1}
	};
	
	private static final int[][] expectedOrdersCollisions = { //simulates adding results -> not commutative
		//singles
		{ 3, 25, 1}, { 2, 0, 2}, { 1, 11, 3}, { 0, 0, 4}, { 7, 8, 5}, { 4, 12, 3}, {9, 13, 3}, //simulation
		{ 0, 0, 6}, { 1, 0, 5}, { 2, 0, 2}, { 3, 0, 1}, { 4, 0, 4}, { 5, 0, 10}, {6, 0, 9}, //after check
		{ 7, 0, 7}, { 8, 0, 8}, { 9, 0, 3}, 
		
		//team ladies
		{42, 5, 1}, {11, 0, 1}, 
		{27, 4, 2}, //TODO arithmetic error on lowest bits //{27, 4, 1} is correct expectation
		{27, 5, 1}, {20, 1, 3},
		{13, 0, 2}, {16, 0, 3}, {21, 0, 3}, {23, 0, 4}, {29, 0, 1}, {41, 0, 2}//aftercheck
		
		
	};
	
	
	private CompetitionDataHandler data;
	
	@Before
	public void setUp() throws Exception {
		data = new CompetitionDataHandler();
		URL resource = Thread.currentThread().getContextClassLoader().getResource("Collisions.xml");
		data.load(resource.getPath());
	}
	
	
	private void checkOrder(ContestantType contestant, int expectedOrder){
		int order = -1;
		if(contestant.getTeamMembership() == TeamMembershipType.TEAM){
			order = this.data.getTeamCache().getTeamOrder(contestant);
		}else{
			order = this.data.getContestantOrder(contestant);
		}
		
		(expectedOrder == order ? System.out : System.err).println("expcted order for " + contestant.getName() 
				+ " (" + contestant.getId().intValue() + ") is " + expectedOrder 
				+ ". Actual is " + order + ".");
		
		assertEquals(expectedOrder, order);
	}
	
	@Test
	public void testGetOrders() {
		for (int[] idOrder : expectedOrders) {
			ContestantType contestant = this.data.getContestantById(BigInteger.valueOf(idOrder[0]));
			checkOrder(contestant, idOrder[1]);
		}
	}
	
	@Test
	public void testSimulateCollision() {
		for (int[] idWeightOrder : expectedOrdersCollisions) {
			ContestantType contestant = this.data.getContestantById(BigInteger.valueOf(idWeightOrder[0]));
			contestant.getResults().getSnatch().add(idWeightOrder[1]);
			this.data.setDirty();
			checkOrder(contestant, idWeightOrder[2]);
		}
	}

}
