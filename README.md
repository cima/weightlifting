# Weightlifting #

An opensource lightweight utility for managing of heavyweight lifting contest. From registering and weighing of athletes through announcing next attempt weights to final results print sheets. It is partially distributed so that maste computer is used for filling in actually lifted weights while the guest computers are used for announcing next attempts.

### What it can do? ###

* Register athlete (name, club, team, year of birth, gender, initial attempts)
* Assign athlete to given age category (youth, juniors, adults, masters)
* Describe competinion briefly (referee names, date, place, club)
* Have unlimited starting groups
* Rearrange athletes between groups and each other
* Accept announcements
* Fill in actually lifted attempt (even unsuccessful)
* Calculate sinclair according to athletes' parameters (even for teams)
* Continuously show current results (including order)
* Print out clean result sheets
* Print out clean/filled registrations sheets
* Print out clean technical sheet for announcements 
* Print out nice looking result sheets

### What it can't do? ###

* Measure time
* Accept refferees' verdicts and automatically write actually lifted weight
* Show fancy splashscreens with various result views and next athlete to get prepared
* Secured connectivity (isolated LAN must be used)

### For users ###
* Download and install Java 1.8
* Go to download section of this project and download [install-weightlifting.jar](https://bitbucket.org/cima/weightlifting/downloads/install-weightlifting.jar) which is standalone multiplatform installer. Just run and click next, next, next.
* When installed, you'll find the icon similar to the one in this project on your desktop.
* Optionally install up to date modern browser like Firefox or chrome
* Optionally install PDF creator

### For developers ###
* Summary of set up: Checkout, import as an existing maven project in your favorite java IDE
* Dependencies: Maven takes care of everything. LibreOffice for documentation editing